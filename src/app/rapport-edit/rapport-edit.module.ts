import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RapportEditPageRoutingModule } from './rapport-edit-routing.module';

import { RapportEditPage } from './rapport-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RapportEditPageRoutingModule
  ],
  declarations: [RapportEditPage]
})
export class RapportEditPageModule {}
