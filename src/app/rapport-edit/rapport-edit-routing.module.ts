import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RapportEditPage } from './rapport-edit.page';

const routes: Routes = [
  {
    path: '',
    component: RapportEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RapportEditPageRoutingModule {}
