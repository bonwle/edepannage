import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RapportEditPage } from './rapport-edit.page';

describe('RapportEditPage', () => {
  let component: RapportEditPage;
  let fixture: ComponentFixture<RapportEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RapportEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RapportEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
