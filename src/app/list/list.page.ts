import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Camera, PictureSourceType, CameraOptions} from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ActionSheetController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  // private selectedItem: any;
  // private icons = [
  //   'flask',
  //   'wifi',
  //   'beer',
  //   'football',
  //   'basketball',
  //   'paper-plane',
  //   'american-football',
  //   'boat',
  //   'bluetooth',
  //   'build'
  // ];
  // public items: Array<{ title: string; note: string; icon: string }> = [];
  // constructor() {
  //   for (let i = 1; i < 11; i++) {
  //     this.items.push({
  //       title: 'Item ' + i,
  //       note: 'This is item #' + i,
  //       icon: this.icons[Math.floor(Math.random() * this.icons.length)]
  //     });
  //   }
  // }
  images=[];

    constructor
    (
      private camera: Camera, private file: File, 
      private webView: WebView, private sheetControler: ActionSheetController,
      private toastController: ToastController, private plt: Platform, 
      private loadingController: LoadingController, 
      private ref: ChangeDetectorRef, private filePath: FilePath,
      )
      {

      }

  ngOnInit() 
    {
      // this.plt.ready().then(()=>{
      // this.loadStoreImage();
      // });
    }

    pathForImage(img)
    {
      if(img===null)
      {
        return '';
      }
      else
      {
        let converted= this.webView.convertFileSrc(img);
        return converted;
      }
    }

    async presentToast(text)
    {
      const toast=await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000,
      });
      toast.present();
    }

    async selectImage()
    {
      const actionSheet=await this.sheetControler.create({
        buttons: [
          {
            text: 'Chargement image',
            handler:()=>{
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
              console.log("take picture");
            }
          },
          {
            text:'Utilisé la camera',
            handler:()=>{this.takePicture(this.camera.PictureSourceType.CAMERA);}
          },
          {
            text:'Annuler',
            role:'cancel'
          }
        ],
        header: 'Selectionné une image.'
      });
       await actionSheet.present();
    }

    takePicture(sourceType: PictureSourceType)
    {
      var options: CameraOptions={
        quality:100,
        sourceType:sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true,
      }

      this.camera.getPicture(options).then(imagePath=>{
        if(this.plt.is('android')&& sourceType===this.camera.PictureSourceType.PHOTOLIBRARY)
        {
          this.filePath.resolveNativePath(imagePath)
          .then(filePath=>{
            let correctPath=filePath.substr(0, filePath.lastIndexOf('/')+1);
            let currentName=imagePath.substring(imagePath.lastIndexOf('/')+1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName,this.createFileName());
            this.images.push('data:image/jpeg;base64,'+imagePath);
          });
        }
        else
        {
          var currentName= imagePath.substr(imagePath.lastIndexOf('/')+1);
          var correctPath=imagePath.substr(0, imagePath.lastIndexOf('/')+1);
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          this.images.push('data:image/jpeg;base64,'+imagePath);
        }
      })
    }

    private createFileName()
  {
    var date= new date();
     var filePathName=date.getTime();
     filePathName+=".jpg";
     return filePathName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName)
  {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName)
    .then(success=>{
      // this.updateStoreImage(newFileName);
    }, error=>{this.presentToast('Erreur de chargement de l\'image');})
  }

  // updateStoreImage(name)
  // {
  //   this.
  // }

  StartUpLoad(imgEntry)
  {
    // this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
    // .then(entry=>{ (<FileEntry> imgEntry).file(file=>this.readFile(file))})
    // .catch(err=>{this.presentToast("Erreur de lecture de l\'image");});
  }

  readFile(file:any)
  {
    const reader=new FileReader();
    reader.onload=()=>{
      const formData=new FormData();
      const imgBlob=new Blob([reader.result], {
        type:file.type
      });
      formData.append('/assets/img/', imgBlob, file.name);

      this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }

  async uploadImageData(formData: FormData)
  {
    const loading= await this.loadingController.create({
      message:'Téléchargement...',
    });
    await loading.present();
  }
}
