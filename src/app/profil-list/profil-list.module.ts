import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilListPageRoutingModule } from './profil-list-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProfilListPage } from './profil-list.page';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule, OwlInputModule,
    ProfilListPageRoutingModule, NgxDatatableModule
  ],
  declarations: [ProfilListPage]
})
export class ProfilListPageModule {}
