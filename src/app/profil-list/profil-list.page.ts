import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { ProfilDetailPage } from '../profil-detail/profil-detail.page';
import { ProfilService } from '../services/profil.service';
import { ProfilCreatePage } from '../profil-create/profil-create.page';
import { ProfilEditPage } from '../profil-edit/profil-edit.page';
import { Profil } from '../models/profil';

@Component ({
  selector: 'app-profil-list',
  templateUrl: './profil-list.page.html',
  styleUrls: ['./profil-list.page.scss'],
})
export class ProfilListPage implements OnInit {

  tableStyle = 'material';
  public columns: any;
  public rows: any;
  filteredData = [];
  ProfilData: any;
  detaildata: Profil;
  constructor(
    public apiService: ProfilService,
    private modalCntrl: ModalController,
    private alertCtlr: AlertController
  ) 
  { this.ProfilData = [];
    this.detaildata = new Profil();
  }

  ngOnInit() {
    this.getAllProfil();
  }

  ionViewWillEnter()
  {
    this.getAllProfil();
  }

  getAllProfil()
  {
    this.apiService.getAllProfil().subscribe(response=>{
      this.ProfilData =response;
    });
  }

  delete(item)
  {
    this.confirmDeleted(item);
  }

  async confirmDeleted(item)
   {
     const alert= await this.alertCtlr.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deleteProfil(item._id).subscribe(response=>{this.getAllProfil()})
            }
          }
            ]
       }
     );
     await alert.present()
   }


  async openDetailModal(data) {
    const modal = await this.modalCntrl.create({
     component: ProfilDetailPage,
     animated:true,
     componentProps:{detailData:data}
    });

    return await modal.present();
   }

   async openEditModal(data) {
    const modal = await this.modalCntrl.create({
     component: ProfilEditPage,
     animated:true,
     backdropDismiss:false,
     componentProps:{editData:data}
    });

    return await modal.present();
   }

   async openAddModal() {
    const modal = await this.modalCntrl.create({
     component: ProfilCreatePage,
     animated:true,
     backdropDismiss:false,
    });

    return await modal.present();
   }

   
}

