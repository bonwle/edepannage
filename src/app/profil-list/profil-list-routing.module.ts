import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilListPage } from './profil-list.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilListPageRoutingModule {}
