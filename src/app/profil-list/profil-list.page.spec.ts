import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfilListPage } from './profil-list.page';

describe('ProfilListPage', () => {
  let component: ProfilListPage;
  let fixture: ComponentFixture<ProfilListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
