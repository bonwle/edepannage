import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RapportListPageRoutingModule } from './rapport-list-routing.module';
import { RapportListPage } from './rapport-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxDatatableModule,
    RapportListPageRoutingModule,
    SignaturePadModule
  ],
  declarations: [RapportListPage]
})
export class RapportListPageModule {}
