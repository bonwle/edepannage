import { Component, OnInit } from '@angular/core';
import { RapportService } from '../services/rapport.service';
import { Rapport } from '../models/rapport';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-rapport-list',
  templateUrl: './rapport-list.page.html',
  styleUrls: ['./rapport-list.page.scss'],
})
export class RapportListPage implements OnInit {

  dataRapport: Rapport;
  rapportData:any;
  imageFinIntervention: string;
  
  constructor(
    private apiService: RapportService,
    private alertCtrl:AlertController
  ) 
  {
    this.dataRapport=new Rapport();
    this.rapportData=[];

   }

  ngOnInit() 
  {
    this.getRapports();
  }

  submitForm()
  {
    this.apiService.createRapport(this.dataRapport).subscribe(
      (response)=>{
        this.getRapports();
    });
    
  }

  getRapports()
  {
    this.apiService.getAllRapport().subscribe(response=>{
      this.rapportData=response;
    })
  }

  async confirmDeleted(item)
   {
     const alert= await this.alertCtrl.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deleteRapport(item._id).subscribe(response=>{this.getRapports()});
            }
          }
            ] 
           
       }
     );
     await alert.present()
   }

}
