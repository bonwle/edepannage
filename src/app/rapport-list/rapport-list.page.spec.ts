import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RapportListPage } from './rapport-list.page';

describe('RapportListPage', () => {
  let component: RapportListPage;
  let fixture: ComponentFixture<RapportListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RapportListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RapportListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
