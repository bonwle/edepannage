import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RapportListPage } from './rapport-list.page';

const routes: Routes = [
  {
    path: '',
    component: RapportListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RapportListPageRoutingModule {}
