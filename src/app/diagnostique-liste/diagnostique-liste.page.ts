import { Component, OnInit } from '@angular/core';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { Router } from '@angular/router';
import { Diagnostique } from '../models/diagnostique';
import { ModalController, AlertController } from '@ionic/angular';

import { DiagnostiqueDetailPage } from '../diagnostique-detail/diagnostique-detail.page';
import { DiagnostiqueCreatePage} from '../diagnostique-create/diagnostique-create.page';
import { DiagnostiqueEditPage} from '../diagnostique-edit/diagnostique-edit.page';

import { InterventionService } from '../services/intervention.service';

@Component ({
  selector: 'app-diagnostique-liste',
  templateUrl: './diagnostique-liste.page.html',
  styleUrls: ['./diagnostique-liste.page.scss'],
})
export class DiagnostiqueListePage implements OnInit {

  tableStyle = 'material';
  public columns: any;
  public rows: any;

  filteredData = [];

  dataDiagnostique: any;
  dataIntervention: any;

  constructor(
    public DiagnosticService: DiagnostiqueService,
    public route: Router,
    private modalCntrl: ModalController,
    private alertCtrl: AlertController,
    private apiIntervention: InterventionService
  ) 
  { this.dataDiagnostique = [];

  this.dataIntervention=[]; 
}

  ngOnInit() {
    this.getAllDiagnostique();
    this.getListInterventions();
  }

  ionViewWillEnter()
  {
    this.getAllDiagnostique();
  }

  getAllDiagnostique()
  {
    this.DiagnosticService.getListDiagnostiques().subscribe(response=>{
      console.log(response);
      this.dataDiagnostique =response;
    })
  }

  getListInterventions()
 {
   this.apiIntervention.getInterventions().subscribe(response=>{
    console.log(response);
    this.dataIntervention=response;
});
 }


  delete(item)
  {
    this.confirmDeleted(item);
  }

  public trackByFunction(index, item)
  {
    if(!item)
    return null;
    return item._id;
  }

   async openDetailModal(data) {
    const modal = await this.modalCntrl.create({
     component: DiagnostiqueDetailPage,
     animated: true,
     componentProps: {diagnostiqueDetail: data}
    });
    return await modal.present();
   }

   async openAddModal() {
    const modal = await this.modalCntrl.create({
     component: DiagnostiqueCreatePage,
     animated: true,
     backdropDismiss: false
    });
    return await modal.present();
   }

   async openEditModal(data)
   // tslint:disable-next-line: one-line
   {
     console.log(data);
    // tslint:disable-next-line: align
    const modal = await this.modalCntrl.create({

     // tslint:disable-next-line: no-shadowed-variable
     component: DiagnostiqueEditPage,

     componentProps: { DiagnostiqueEdit: data},

     animated:true,

     backdropDismiss:true

    });

    // tslint:disable-next-line: align
    return await modal.present();

   }

   async confirmDeleted(item)
   {
     const alert = await this.alertCtrl.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.DiagnosticService.deleteDiagnostique(item._id).subscribe(response=>{this.getAllDiagnostique()});
            }
          }
        ]
       }
     );
     await alert.present();
   }

  submitForm()
  {
    
    this.DiagnosticService.createDiagnostique(this.dataDiagnostique).subscribe((response)=>{
      this.route.navigate(['diagnostiqueList']);
    })
  }
}
