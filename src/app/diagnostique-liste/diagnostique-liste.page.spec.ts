import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnostiqueListePage } from './diagnostique-liste.page';

describe('DiagnostiqueListePage', () => {
  let component: DiagnostiqueListePage;
  let fixture: ComponentFixture<DiagnostiqueListePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnostiqueListePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnostiqueListePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
