import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiagnostiqueListePage } from './diagnostique-liste.page';

const routes: Routes = [
  {
    path: '',
    component: DiagnostiqueListePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiagnostqueListePageRoutingModule {}
