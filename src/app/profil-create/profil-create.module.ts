import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilCreatePageRoutingModule } from './profil-create-routing.module';

import { ProfilCreatePage } from './profil-create.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OwlInputModule, OwlFormFieldModule, OwlSelectModule } from 'owl-ng';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilCreatePageRoutingModule, 
    NgxDatatableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
  ],
  declarations: [ProfilCreatePage]
})
export class ProfilCreatePageModule {}
