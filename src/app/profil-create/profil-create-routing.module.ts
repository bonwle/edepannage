import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilCreatePage } from './profil-create.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilCreatePageRoutingModule {}
