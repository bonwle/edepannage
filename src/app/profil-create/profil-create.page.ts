import { Component, OnInit } from '@angular/core';
import { ProfilService } from '../services/profil.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Profil } from '../models/profil';
import { RoleService } from '../services/role.service';
import { Role } from '../models/role';

@Component({
  selector: 'app-profil-create',
  templateUrl: './profil-create.page.html',
  styleUrls: ['./profil-create.page.scss'],
})
export class ProfilCreatePage implements OnInit {

    profilData : any;
     dataProfil: Profil;
     rolesData: any;

  constructor(
    private apiService: ProfilService,
    private router: Router,
    private apiRoleService: RoleService,
  ) {
    this.dataProfil = new Profil();
    this.profilData =[];
    this.rolesData=[];
  }
  ngOnInit() {
   this.getRoles();
   this.dataProfil.DateCreation=new Date();
  }


  submitForm()
  {
    this.apiService.createProfil(this.dataProfil).subscribe((response)=>{
      this.router.navigate(['administrateur']);
    });
  }

  getRoles()
  {
    this.apiRoleService.getListRole().subscribe(response=>{
      this.rolesData=response;
    })
  }
}
