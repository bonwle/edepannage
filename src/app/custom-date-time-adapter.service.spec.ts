import { TestBed } from '@angular/core/testing';

import { CustomDateTimeAdapterService } from './custom-date-time-adapter.service';

describe('CustomDateTimeAdapterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomDateTimeAdapterService = TestBed.get(CustomDateTimeAdapterService);
    expect(service).toBeTruthy();
  });
});
