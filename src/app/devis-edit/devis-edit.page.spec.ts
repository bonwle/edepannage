import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevisEditPage } from './devis-edit.page';

describe('DevisEditPage', () => {
  let component: DevisEditPage;
  let fixture: ComponentFixture<DevisEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevisEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
