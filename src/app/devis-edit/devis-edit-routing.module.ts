import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevisEditPage } from './devis-edit.page';

const routes: Routes = [
  {
    path: '',
    component: DevisEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevisEditPageRoutingModule {}
