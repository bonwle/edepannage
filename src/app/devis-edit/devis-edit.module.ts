import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevisEditPageRoutingModule } from './devis-edit-routing.module';

import { DevisEditPage } from './devis-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DevisEditPageRoutingModule
  ],
  declarations: [DevisEditPage]
})
export class DevisEditPageModule {}
