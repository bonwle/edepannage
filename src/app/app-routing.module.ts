import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
   {
     path: 'home',
     loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
    },
    {
      path: 'list',
      loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
     },
  {
    path: 'devis-list',
    loadChildren: () => import('./devis-list/devis-list.module').then( m => m.DevisListPageModule)
  },
  {
    path: 'devis-create',
    loadChildren: () => import('./devis-create/devis-create.module').then( m => m.DevisCreatePageModule)
  },
  {
    path: 'devis-edit',
    loadChildren: () => import('./devis-edit/devis-edit.module').then( m => m.DevisEditPageModule)
  },
  {
    path: 'devis-detail/:id',
    loadChildren: () => import('./devis-detail/devis-detail.module').then( m => m.DevisDetailPageModule)
  },
  {
    path: 'rapport-list',
    loadChildren: () => import('./rapport-list/rapport-list.module').then( m => m.RapportListPageModule)
  },
  {
    path: 'rapport-create',
    loadChildren: () => import('./rapport-create/rapport-create.module').then( m => m.RapportCreatePageModule)
  },
  {
    path: 'rapport-edit',
    loadChildren: () => import('./rapport-edit/rapport-edit.module').then( m => m.RapportEditPageModule)
  },
  {
    path: 'rapport-detail/:id',
    loadChildren: () => import('./rapport-detail/rapport-detail.module').then( m => m.RapportDetailPageModule)
  },
  {
    path: 'intervention-create',
    loadChildren: () => import('./intervention-create/intervention-create.module').then( m => m.InterventionCreatePageModule)
  },
  {
    path: 'intervention-list',
    loadChildren: () => import('./intervention-list/intervention-list.module').then( m => m.InterventionListPageModule)
  },
  {
    path: 'intervention-list/:status',
    loadChildren: () => import('./intervention-list/intervention-list.module').then( m => m.InterventionListPageModule)
  },
  {
    path: 'intervention-edit-test/:id',
    loadChildren: () => import('./intervention-edit-test/intervention-edit-test.module').then(m=>m.InterventionEditTestPageModule)
  },

  {
    path: 'intervention-detail/:id',
    loadChildren: () => import('./intervention-detail/intervention-detail.module').then( m => m.InterventionDetailPageModule)
  },
  
  {
    path: 'utilisateur-list',
    loadChildren: () => import('./utilisateur-list/utilisateur-list.module').then( m => m.UtilisateurListPageModule)
  },
  {
    path: 'utilisateur-create',
    loadChildren: () => import('./utilisateur-create/utilisateur-create.module').then( m => m.UtilisateurCreatePageModule)
  },
  {
    path: 'utilisateur-edit/:id',
    loadChildren: () => import('./utilisateur-edit/utilisateur-edit.module').then( m => m.UtilisateurEditPageModule)
  
  },
  {
    path: 'utilisateur-detail/:id',
    loadChildren: () => import('./utilisateur-detail/utilisateur-detail.module').then( m => m.UtilisateurDetailPageModule)
  },
  {
    path: 'authentification',
    loadChildren: () => import('./authentification/authentification.module').then( m => m.AuthentificationPageModule)
  },
  {
    path: 'administrateur',
    loadChildren: () => import('./administrateur/administrateur.module').then( m => m.AdministrateurPageModule)
  },
  {
    path: 'piece-list',
    loadChildren: () => import('./piece-list/piece-list.module').then( m => m.PieceListPageModule)
  },
  {
    path: 'piece-create',
    loadChildren: () => import('./piece-create/piece-create.module').then( m => m.PieceCreatePageModule)
  },
  {
    path: 'piece-edit/:id',
    loadChildren: () => import('./piece-edit/piece-edit.module').then( m => m.PieceEditPageModule)
  },
  {
    path: 'piece-detail/:id',
    loadChildren: () => import('./piece-detail/piece-detail.module').then( m => m.PieceDetailPageModule)
  },
  {
     path: 'diagnostiqueCreate',
      loadChildren: () => import('./diagnostique-create/diagnostique-create.module').then(m => m.DiagnostiqueCreatePageModule)
    },
  {  path: 'diagnostiqueList',
     loadChildren: () => import('./diagnostique-liste/diagnostique-liste.module').then(m => m.DiagnostiqueListePageModule)
  },
  { path: 'diagnostique-/:id',
   loadChildren: () => import( './diagnostique-edit/diagnostique-edit.module').then(m => m.DiagnostiqueEditPageModule)
  },
  { path: 'diagnostique-detail/:id',
  loadChildren: () => import( './diagnostique-detail/diagnostique-detail.module').then(m => m.DiagnostiqueDetailPageModule)
 },
 
  {
    path: 'role-create',
    loadChildren: () => import('./role-create/role-create.module').then( m => m.RoleCreatePageModule)
  },
  {
    path: 'role-detail/:id',
    loadChildren: () => import('./role-detail/role-detail.module').then( m => m.RoleDetailPageModule)
  },
  {
    path: 'role-edit/:id',
    loadChildren: () => import('./role-edit/role-edit.module').then( m => m.RoleEditPageModule)
  },
  {
    path: 'role-list',
    loadChildren: () => import('./role-list/role-list.module').then( m => m.RoleListPageModule)
  },
  {
    path: 'profil-create',
    loadChildren: () => import('./profil-create/profil-create.module').then( m => m.ProfilCreatePageModule)
  },
  {
    path: 'profil-edit/:id',
    loadChildren: () => import('./profil-edit/profil-edit.module').then( m => m.ProfilEditPageModule)
  },
  {
    path: 'profil-detail/:id',
    loadChildren: () => import('./profil-detail/profil-detail.module').then( m => m.ProfilDetailPageModule)
  },
  {
    path: 'profil-list',
    loadChildren: () => import('./profil-list/profil-list.module').then( m => m.ProfilListPageModule)
  },
  {
    path: 'zone-create',
    loadChildren: () => import('./zone-create/zone-create.module').then( m => m.ZoneCreatePageModule)
  },
  {
    path: 'zone-edit/:id',
    loadChildren: () => import('./zone-edit/zone-edit.module').then( m => m.ZoneEditPageModule)
  },
  {
    path: 'zone-detail/:id',
    loadChildren: () => import('./zone-detail/zone-detail.module').then( m => m.ZoneDetailPageModule)
  },
  {
    path: 'equipe-create',
    loadChildren: () => import('./equipe-create/equipe-create.module').then( m => m.EquipeCreatePageModule)
  },
  {
    path: 'equipe-detail/:id',
    loadChildren: () => import('./equipe-detail/equipe-detail.module').then( m => m.EquipeDetailPageModule)
  },
  {
    path: 'equipe-edit/:id',
    loadChildren: () => import('./equipe-edit/equipe-edit.module').then( m => m.EquipeEditPageModule)
  },
  // {
  //   path: 'edit-intervention',
  //   loadChildren: () => import('./edit-intervention/edit-intervention.module').then( m => m.EditInterventionPageModule)
  // },
  // {
  //   path: 'intervention-edit-test/:id',
  //   loadChildren: () => import('./intervention-edit-test/intervention-edit-test.module').then( m => m.InterventionEditTestPageModule)
  // },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
