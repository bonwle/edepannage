import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Zones } from '../models/zones';
import { ZoneService } from '../services/zone.service';
import { UtilisateursService } from '../services/utilisateurs.service';
import { EquipeService } from '../services/equipe.service';

@Component({
  selector: 'app-zone-detail',
  templateUrl: './zone-detail.page.html',
  styleUrls: ['./zone-detail.page.scss'],
})
export class ZoneDetailPage implements OnInit {
  detailZone: Zones;
  id: number;
  dataEquipe: any;
  membreEquipe: any;

  constructor
  (
    private route:Router,
    private activatedRoute: ActivatedRoute,
    private apiService: ZoneService,
    private apiUserService: UtilisateursService,
    private apiServiceEquipe: EquipeService,

    ) 
    {
      this.detailZone=new Zones();
      this.membreEquipe=[];   
      this.dataEquipe=[];
      }

  ngOnInit() {
    this.id=this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.getZone(this.id).subscribe(response=>{
      console.log(response);
      this.getEquipesData();
      this.getDataUsers();
      this.detailZone=response;
      
    })

  }

  getDataUsers()
  {
    this.apiUserService.getUtilisateurs().subscribe(response=>{
      console.log(response);
      console.log('Loading User');
      this.membreEquipe=response;
    })
  }

  getEquipesData()
  {
    this.apiServiceEquipe.getEquipes().subscribe(response=>{
      console.log(response);
      console.log('loading equipe');
      this.dataEquipe=response;
    })
  }


}
