import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoneDetailPageRoutingModule } from './zone-detail-routing.module';

import { ZoneDetailPage } from './zone-detail.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    ZoneDetailPageRoutingModule
  ],
  declarations: [ZoneDetailPage]
})
export class ZoneDetailPageModule {}
