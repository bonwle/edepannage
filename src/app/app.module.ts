import { NgModule, LOCALE_ID } from '@angular/core';
import {OwlFormFieldModule, OwlInputModule, OwlSelectModule} from 'owl-ng'
import { HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import {SignaturePadModule} from 'angular2-signaturepad';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {HttpClientModule} from '@angular/common/http'
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Camera } from '@ionic-native/camera/ngx';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AuthentificationPageModule } from './authentification/authentification.module';
import {OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl, OWL_DATE_TIME_LOCALE} from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { File } from '@ionic-native/file/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import {PhotoViewer} from '@ionic-native/photo-viewer/ngx';
import {Base64} from "@ionic-native/base64/ngx";
import { setInterval } from 'timers';

export class LabelAndMessageText extends OwlDateTimeIntl 
  {
    cancelBtnLabel='Annuler';
    setBtnLabel='Confirmer';
  }

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserAnimationsModule,
    SignaturePadModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    NgxDatatableModule,
    AuthentificationPageModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    ReactiveFormsModule
    ],
  providers: [
    StatusBar,
    Camera,
    File,
    FilePath,
    WebView,
    PhotoViewer,
    Base64,
    SplashScreen,
    {
      provide: OWL_DATE_TIME_LOCALE, useValue: 'fr-FR'
    },
    {
      provide: LOCALE_ID, useValue: 'fr-FR'
    },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    {
      provide:HAMMER_GESTURE_CONFIG, useClass: AppModule
    },
    {
      provide:RouteReuseStrategy, useClass: IonicRouteStrategy
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule extends HammerGestureConfig {
  overrides={
    pinch:{enable:false},
    rotate:{enable: false}
  };
}
