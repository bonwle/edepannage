import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, ModalController, PopoverController } from '@ionic/angular';
import { UtilisateursService } from '../services/utilisateurs.service';
import { Utilisateurs } from '../models/utilisateurs';
import { Router } from '@angular/router';
import { ProfilService } from '../services/profil.service';
import { UserContext } from '../models/user-context';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.page.html',
  styleUrls: ['./authentification.page.scss'],
})
export class AuthentificationPage implements OnInit {
  userConnectName:string="";

  constructor
  (
    private router: Router,
    private ctrlView: PopoverController,
  ) 
  {
    
   }

  ngOnInit()
   {
    console.log("Test instance");
    if(UserContext.userConnect!=undefined)
    this.userConnectName=UserContext.userConnect[0].NomUser;
  }

  async signOut()
  {
    await UserContext.logOutUser();
    // UserContext.userConnect=undefined;
    this.router.navigate(['home']);
    this.popoverClose();
  }

  callUserProfilPage()
  {
    this.router.navigate(['utilisateur-edit/'+UserContext.userConnect[0]._id]);
    this.popoverClose();
  }

  async popoverClose()
  {
   return await this.ctrlView.dismiss();
  }


}
