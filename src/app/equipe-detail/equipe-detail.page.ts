import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { EquipeService } from '../services/equipe.service';
import { Equipes } from '../models/equipes';
import { UtilisateursService } from '../services/utilisateurs.service';
import {ZoneService} from '../services/zone.service';
@Component({
  selector: 'app-equipe-detail',
  templateUrl: './equipe-detail.page.html',
  styleUrls: ['./equipe-detail.page.scss'],
})
export class EquipeDetailPage implements OnInit {
  id:any;
  equipeDetail:Equipes;
  dataUsers: any ;
  dataUser : any;
  dataEquipe : any;
  dataZone : any;

  constructor( 
    
    private modalCtrl: ModalController,
    private activatedRoute: ActivatedRoute,
    private apiService: EquipeService,
    private apiServiceUser: UtilisateursService)
     { 
    this.equipeDetail=new Equipes();
    this.dataUsers=[];
  }

  ngOnInit() 
  {
    this.id=this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.getEquipe(this.id).subscribe(response=>{
    console.log(response);
    console.log(this.equipeDetail)
    this.equipeDetail=response;

    this.getUsers();
  });
  }

  getUsers()
  {
    this.apiServiceUser.getUtilisateurs().subscribe(response=>{
      console.log(response);
      this.dataUsers=response;
    })
  }

  

}
