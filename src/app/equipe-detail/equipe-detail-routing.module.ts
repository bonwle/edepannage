import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EquipeDetailPage } from './equipe-detail.page';

const routes: Routes = [
  {
    path: '',
    component: EquipeDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquipeDetailPageRoutingModule {}
