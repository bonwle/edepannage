import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EquipeDetailPage } from './equipe-detail.page';

describe('EquipeDetailPage', () => {
  let component: EquipeDetailPage;
  let fixture: ComponentFixture<EquipeDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipeDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EquipeDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
