import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoneEditPageRoutingModule } from './zone-edit-routing.module';

import { ZoneEditPage } from './zone-edit.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    IonicModule,
    ZoneEditPageRoutingModule
  ],
  declarations: [ZoneEditPage]
})
export class ZoneEditPageModule {}
