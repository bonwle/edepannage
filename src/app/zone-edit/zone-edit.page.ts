import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Zones } from '../models/zones';
import { ActivatedRoute, Router } from '@angular/router';
import { ZoneService } from '../services/zone.service';
import { UtilisateursService } from '../services/utilisateurs.service';
import { EquipeService } from '../services/equipe.service';

@Component({
  selector: 'app-zone-edit',
  templateUrl: './zone-edit.page.html',
  styleUrls: ['./zone-edit.page.scss'],
})
export class ZoneEditPage implements OnInit {

  id: any;
  editZoneData: Zones;
  membresEquipe: any;
  allEquipes: any;
  usersData: any;

  constructor(
    private ActivatedRoute: ActivatedRoute,
    private router: Router,
    private apiZoneService: ZoneService,
    private apiUserService: UtilisateursService,
    private toastCtrl: ToastController,
    private apiServiceEquipe: EquipeService,
  ) 
  { 
    this.editZoneData=new Zones();
    this.allEquipes=[];
    this.membresEquipe=[];
    this.usersData=[];
  }

  ngOnInit() {
    this.id=this.ActivatedRoute.snapshot.params["id"];
    this.apiZoneService.getZone(this.id).subscribe(response=>{
      this.getDataUsers();
      this.getEquipesData();
      this.editZoneData=response;
    });
  }


  update()
  {
    this.apiZoneService.updateZone(this.id, this.editZoneData).subscribe(response=>{
    this.displayToastEdit();
    this.router.navigate(['administrateur']);
    // this.closeModal();

  });
  }

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"middle",
      }
    );
    toast.present();
  }

getDataUsers()
  {
    this.apiUserService.getUtilisateurs().subscribe(response=>{
      console.log(response);
      console.log('Loading User');
      this.membresEquipe=response;
    })
  }

  getEquipesData()
  {
    this.apiServiceEquipe.getEquipes().subscribe(response=>{
      console.log(response);
      console.log('loading equipe');
      this.allEquipes=response;
    })
  }

  searchMembres(items)
  {
    if(items)
    {
      console.log(items);
      console.log("Ok item")
      this.apiServiceEquipe.getAgentNotInEquipe(items).subscribe((response)=>{
      console.log(response);
      this.membresEquipe=response;
    });
  }
}

}
