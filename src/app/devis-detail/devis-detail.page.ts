import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DevisService } from '../services/devis.service';
@Component({
  selector: 'app-devis-detail',
  templateUrl: './devis-detail.page.html',
  styleUrls: ['./devis-detail.page.scss'],
})
export class DevisDetailPage implements OnInit {
  DevisData:any;
  id: number;
  

  constructor(
    public activatedRoute: ActivatedRoute, 
    public devisService:DevisService,
  ) { 
    this.DevisData=[];

  }

  ngOnInit() {
    this.id=this.activatedRoute.snapshot.params["id"];
    this.getDevisDetail();
  }


//   getPieceDetail(item)
//  {
//   this.pieceService.getPiece(item).subscribe(response=>{
//     this.dataPiece=response;
//   });
//  }
 getDevisDetail()
 {
   this.devisService.getDevis(this.id).subscribe(response=>{
     this.DevisData= response;
     console.log(this.DevisData);
   });
 }
}
