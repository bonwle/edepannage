import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevisDetailPageRoutingModule } from './devis-detail-routing.module';

import { DevisDetailPage } from './devis-detail.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlSelectModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,OwlDateTimeModule, OwlNativeDateTimeModule, OwlFormFieldModule,
    OwlInputModule, OwlSelectModule,
    DevisDetailPageRoutingModule
  ],
  declarations: [DevisDetailPage]
})
export class DevisDetailPageModule {}
