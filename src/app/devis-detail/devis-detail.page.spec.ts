import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevisDetailPage } from './devis-detail.page';

describe('DevisDetailPage', () => {
  let component: DevisDetailPage;
  let fixture: ComponentFixture<DevisDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevisDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
