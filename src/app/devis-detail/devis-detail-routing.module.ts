import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevisDetailPage } from './devis-detail.page';

const routes: Routes = [
  {
    path: '',
    component: DevisDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevisDetailPageRoutingModule {}
