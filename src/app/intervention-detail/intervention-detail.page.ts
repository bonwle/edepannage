import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Interventions } from '../models/interventions';
import { InterventionService } from '../services/intervention.service';
import { ActivatedRoute } from '@angular/router';
import { UtilisateursService } from '../services/utilisateurs.service';
import { EquipeService} from '../services/equipe.service';
import {ZoneService} from '../services/zone.service';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { DevisService} from '../services/devis.service';
import { RapportService } from '../services/rapport.service';
@Component({
  selector: 'app-intervention-detail',
  templateUrl: './intervention-detail.page.html',
  styleUrls: ['./intervention-detail.page.scss'],
})
export class InterventionDetailPage implements OnInit {

  id: number;
  data: Interventions;
  interventionSegment: string;
  dataUserIntervention: any;
  dataEquipe: any;
  dataZone: any;
  DiagnosticsIntervention: any;
  dataDiagnosticDisplay: any;
  dataDevis: any;
  dataRapport: any;
  otherUser:any;
  photoPDF :any;

  constructor(
    private alertCtrl: AlertController,
    public apiService: InterventionService,
    public activatedRoute: ActivatedRoute,
    public equipeService: EquipeService,
    public zoneService: ZoneService,
    public userService: UtilisateursService,
    public diagnosticservice: DiagnostiqueService,
    public devisService: DevisService,
    public rapportService: RapportService
  ) {
    
    this.data=new Interventions(); 
    this.dataEquipe=[];
    this.dataZone=[];
    this.dataUserIntervention=[];
    this.DiagnosticsIntervention =[];
    this.dataDiagnosticDisplay = [];
    this.dataDevis = [];
    this.dataRapport = [];
    this.otherUser = [];
    this.photoPDF = [];

  
  }

  ngOnInit() 
  {
    
    this.interventionSegment='details';
    this.id=this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.getIntervention(this.id).subscribe(response=>{
      console.log(response);
      this.data=response;});

    this.getUserAgent();
    this.getUserEquipe();
    this.getZone();
    this.getDiagnostic();
    this.getDevisIntervention();
    this.getRapportIntervention();
   // this.getDiagnosticListe();
    
  }

  getUserAgent()
  {
    this.userService.getUtilisateurs().subscribe(response=>{
      this.dataUserIntervention=response;
      this.otherUser =response;
      console.log(response+"liste user");
    });
  }


  getUserEquipe()
  {
    this.equipeService.getEquipes().subscribe(response=>{
      this.dataEquipe=response;
      console.log(response+"liste equipe");
    });
  }

  getZone()
  {
    this.zoneService.getZones().subscribe(response=>{
      this.dataZone=response;
      console.log(response+"liste zone");
    });
  }

  getDiagnostic()
  {
    this.diagnosticservice.getDiagnosticIntervention(this.id).subscribe(response=>{
      this.DiagnosticsIntervention=response;

      console.log("list diagnostic");
      console.log(this.DiagnosticsIntervention);
      
    });
  }

  getDevisIntervention()
  {
    this.devisService.getAllDevisByIntervention(this.id).subscribe(response=>{
      this.dataDevis=response;
    });
  }

  getRapportIntervention()
  {
    this.rapportService.getAllRapportIntervention(this.id).subscribe(response=>{
      this.dataRapport=response;
      
      this.dataRapport.forEach(photo => {
        //console.log(photo.Photos);
        photo.Photos.forEach(element => {
          this.photoPDF.push('data:image/jpeg;base64,'+element);
        });
      });
      
    });
  }

//------------*****************************************------------//

delete(item)
{
  this.confirmDeleted(item);
}

formatDate(date1){
  var dt = [];
   dt = date1.split('T');
  var date2 = dt[0];
 var dt1 = date2.split('-');
 var year = dt1[0];
 var month = dt1[1];
 var day = dt1[2];
  var dateFormated = day + "/" + month + "/" + year;
  return dateFormated;
}

 async confirmDeleted(item)
 {
   const alert= await this.alertCtrl.create(
     {
       header:"Suppression",
       message:"Confirmez-vous la suppression de cet élément?",
       animated:true,
       backdropDismiss:false,
       translucent:true,
       buttons:[
        {
          text:"Non",
          role:"cancel",
          cssClass: "secondary" 
        },
        {
          text: "Oui",
          handler:()=>{
            this.diagnosticservice.deleteDiagnostique(item._id).subscribe(response=>{this.getDiagnostic()});
            this.devisService.deleteDevis(item._id).subscribe(response=>{this.getDevisIntervention()});
          }
        }
          ] 
         
     }
   );
   await alert.present();
 }




}
