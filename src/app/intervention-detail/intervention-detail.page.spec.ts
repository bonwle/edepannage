import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InterventionDetailPage } from './intervention-detail.page';

describe('InterventionDetailPage', () => {
  let component: InterventionDetailPage;
  let fixture: ComponentFixture<InterventionDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterventionDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InterventionDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
