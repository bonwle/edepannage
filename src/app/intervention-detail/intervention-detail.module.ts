import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InterventionDetailPageRoutingModule } from './intervention-detail-routing.module';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';
import { InterventionDetailPage } from './intervention-detail.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule, OwlDateTimeModule, OwlNativeDateTimeModule, OwlFormFieldModule,
    OwlInputModule, OwlSelectModule,
    NgxDatatableModule,
    SignaturePadModule, ReactiveFormsModule,
    InterventionDetailPageRoutingModule
  ],
  declarations: [InterventionDetailPage]
})
export class InterventionDetailPageModule {}
