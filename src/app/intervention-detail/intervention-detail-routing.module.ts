import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InterventionDetailPage } from './intervention-detail.page';

const routes: Routes = [
  {
    path: '',
    component: InterventionDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterventionDetailPageRoutingModule {}
