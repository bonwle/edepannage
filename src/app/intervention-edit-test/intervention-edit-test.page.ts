import { Component, OnInit, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Interventions } from '../models/interventions';
import { Diagnostique } from '../models/diagnostique';
import { Devis } from '../models/devis';
import { Pieces } from '../models/pieces';
import { Rapport } from '../models/rapport';
import { ActivatedRoute, Router } from '@angular/router';
import { InterventionService } from '../services/intervention.service';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { DevisService } from '../services/devis.service';
import { PieceService } from '../services/piece.service';
import { EquipeService } from '../services/equipe.service';
import { ZoneService } from '../services/zone.service';
import { UtilisateursService } from '../services/utilisateurs.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RapportService } from '../services/rapport.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-intervention-edit-test',
  templateUrl: './intervention-edit-test.page.html',
  styleUrls: ['./intervention-edit-test.page.scss'],
})
export class InterventionEditTestPage implements OnInit {

  photoUrls: string[] = [];
  @ViewChild("clientSignature", {static:false}) clientSignature: SignaturePad;
  @ViewChild("agentSignature", {static:false}) agentSignature: SignaturePad;

 private Options:object=
 {
   'canvasHeight':80,
 }

  id: any;
  dataEditIntervention: Interventions;
  dataIntervention: Interventions;
  EditIntervention: string;
  dataDiagnostique: Diagnostique;
  dataInterventionDisplay: any;
  devisData: Devis;
  dataPiece : Pieces;
  pieceData: any;
  dataUser:any;
  dataEditZone:any;
  dataEditEquipe:any;
  dataDiagnosticDisplay: any;
  editOtherUser :any;
  dataDevis :any;
  rapportImage: string;
  diagnosticImage: string;
  rapportData: Rapport;
  dataRapport: any;
  dataDiagnostic :any;
  raportData: any;
  to :any;
  imageData : any;
  geoData :any;
  photoPDF :any;
  photoPDF1 :any;
  imageFinIntervention : any;
  imagesRapport:any;
  photosDiagnostic:any;
  demarre: boolean;
  hidde:boolean;
  dataInterventionEquipe:any;
  signatureClient:any;
  signatureIntervenant:any;
  total1:number;
  tva:number;
  totalDevis:number;
  totalHorsTax1:number;
  Mtva:number;
  ttc:number;
 
  constructor(
    private router: Router,
    private apiService: InterventionService,
    private toastCtrl: ToastController,
    private DiagnosticService: DiagnostiqueService,
    private deviService: DevisService,
    private alertCtrl: AlertController,
    private pieceService: PieceService,
    private equipeService: EquipeService,
    private zoneService: ZoneService,
    private userService: UtilisateursService,
    private camera: Camera,
    private apiRapportService: RapportService,
    private activeRouter:ActivatedRoute,
    private loadingController :LoadingController

  ) 
  { 
    this.dataEditIntervention=new Interventions();
    this.dataIntervention= new Interventions();
    this.dataDiagnostique = new Diagnostique();
    this.dataInterventionDisplay = []; 
    this.devisData=new Devis();
    this.dataPiece = new Pieces();
    this.rapportData=new Rapport();
    this.dataRapport=[];
    this.dataUser = [];
    this.dataEditZone =[];
    this.dataEditEquipe =[];
    this.dataDiagnosticDisplay =[];
    this.pieceData=[];
    this.editOtherUser=[];
    this.dataDevis = [];
    this.dataDiagnostic = [];
    this.raportData = [];

    this.to = 0;
    this.total1 =0;
    this.tva = 0;
    this.totalDevis = 0;
    this.totalHorsTax1= 0;
    this.Mtva = 0;
    this.ttc = 0;

    this.imageData =[];
    this.geoData = [];
    this.photoPDF =[];
    this.photoPDF1 =[];
    this.photosDiagnostic=[];
    this.imagesRapport=[];
    this.demarre=true;
    this.hidde=false;

    this.imageFinIntervention =[];
    this.dataInterventionEquipe =[];
    this.signatureClient = [];
    this.signatureIntervenant = [];
    
  }
 
  ngOnInit() {

    this.EditIntervention ='EditDetail';

    this.id=this.activeRouter.snapshot.params['id'];
    this.signatureClient=null;
    this.signatureIntervenant=null;
    this.apiService.getIntervention(this.id).subscribe(response=>{
    this.getUserAgent();
    this.getUserEquipe();
    this.getZones();
    this.dataEditIntervention=response;
    this.dataIntervention = response;
    this.dataInterventionDisplay = response;
    this.getinterventionDiagnostics();
    this.getListPieces();
    this.getDevisIntervention();
    this.getDiagnostics();
    this.getRapportIntervention();
    this.photosDiagnostic.push(2);
    this.imagesRapport.push(1);
    });

    this.apiService.getEquipeUser(this.id).subscribe(response=>{
    this.dataInterventionEquipe = response;
    });

  }

  ngAfterContentInit() {
    this.tva = 0;
    this.totalDevis =0;
    this.total1= 0;
    this.totalHorsTax1=0;
    this.Mtva = 0;
    this.totalHorsTax1=0;
    this.tva = 0;
    this.ttc = 0;
    this.totalHorsTax1 = 0;
  }

  
  updateIntervention()
  {
    this.apiService.updateIntervention(this.id, this.dataEditIntervention).subscribe(response=>{
      this.router.navigate(['intervention-list']);
      this.displayToastEdit();
  });

  }

  getRapports()
  {
      this.apiRapportService.getAllRapport().subscribe(response=>{
      this.dataRapport=response;
    });
  }


  updateDevis()
  // tslint:disable-next-line: one-line
  {
      this.devisData.IdIntervention=this.dataEditIntervention._id;
      this.deviService.createDevis(this.devisData).subscribe((response)=>{
           this.apiService.updateMainDoeuvre(this.id, this.dataEditIntervention).subscribe(data=>{
          });
      //this.displayToastEdit();
      this.dataPiece=new  Pieces();
      this.getListPieces();
     // this.updateIntervention();
      this.displayToastEdit();
      this.getDevisIntervention();
      
    });
}

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"top"
      }
    );
    toast.present();
  }

  async displayToastEdit1()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"date et heure de demarrage non signifiée!",
        header:"Succes Modif",
        color:"danger",
        duration:4000,
        position:"bottom"
      }
    );
    toast.present();
  }

 async displayToastEdit2()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"aucun rapport enregistré!",
        header:"echec genrer pdf",
        color:"danger",
        duration:3500,
        position:"bottom"
      }
    );
    toast.present();
  }

  
 async displayToastEdit3()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"aucun devis enregistré!",
        header:"echec genrer pdf",
        color:"danger",
        duration:3500,
        position:"bottom"
      }
    );
    toast.present();
  }
  

//--------------------------------------------------------------

   getDevisIntervention()
  {
    this.deviService.getAllDevisByIntervention(this.id).subscribe(response=>{
      this.dataDevis= response;
    });
  }
  
async presentLoading() {
  const loading = await this.loadingController.create({
    spinner: 'lines',
    message: 'Chargement...',
    duration:5000,
    translucent: true,
    cssClass: 'custom-class custom-loading'
  });
  return await loading.present();
}

async dismiss() {
  return await this.loadingController.dismiss();
}


  getRapportIntervention()
  {
    this.presentLoading();
    this.apiRapportService.getAllRapportIntervention(this.id).subscribe(response=>{
      this.raportData=response;
      if(response['length']>0)
      {this.demarre=false;
      this.hidde=true;}

      // this.raportData.forEach(photo => {
      //   photo.Photos.forEach(element => {
      //     this.photoPDF.push('data:image/jpeg;base64,'+element);
      //   });
      // });
      
      // this.raportData.forEach(signature => {
      // this.signatureClient = signature.SignatureClient;
      // this.signatureIntervenant = signature.SignatureIntervenant;
      // });

      this.dismiss();
    });

  }

  demarrer()
  {
    this.demarre=false;
    this.hidde=true;
    this.dataEditIntervention.statut="En cours";
    var d=new Date();
    //this.dataDiagnostique.dateDemarrage = this.formatDate(d.toISOString().split('T')[0])+' à '+d.toISOString().split('T')[1].split('.')[0];
    this.dataDiagnostique.dateDemarrage = d;
  }
  


delete(item1, item2)
  {
    this.confirmDeleted(item1,item2);
  }

  async confirmDeleted(item, item2)
   {
     const alert= await this.alertCtrl.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              switch (item2) {
                case 1:
                  { 
                       this.deviService.deleteDevis(item._id).subscribe(response=>{this.getDevisIntervention()});
                  break;
                }
                case 2:
                  {

                    this.DiagnosticService.deleteDiagnostique(item._id).subscribe(response=>{this.getinterventionDiagnostics()});

                  break;
                }
                case 3:
                  {
                    this.apiRapportService.deleteRapport(item._id).subscribe(response=>{
                          this.getRapportIntervention()});
                   
                  break;
                }
        
                default:
                  break;
              }
              
          }}
            ] 
           
       }
     );
     await alert.present()
   }



getUserAgent()
{
  this.userService.getUtilisateurs().subscribe(response=>{
    this.editOtherUser = response;
    this.dataUser=response;
    
  });
}

  getOtherUser(item1)
  {
    this.equipeService.getAgentNotInEquipe(item1).subscribe((response)=>{
    this.editOtherUser=response;
    
    });
  }

  getAgentsEquipe(item)
  {
    this.equipeService.getEquipeAgent(item).subscribe(response=>{
      this.dataUser = response;
      this.getOtherUser(item);
    });
  }

  
  getZoneEquipes(item)
  {
    this.zoneService.getZoneEquipe(item).subscribe(response=>{
    this.dataEditEquipe= response;
  });
}


getUserEquipe()
{
  this.equipeService.getEquipes().subscribe(response=>{
    this.dataEditEquipe=response;
  });
}


getZones()
{
  this.zoneService.getZones().subscribe(response=>{
    this.dataEditZone=response;
  });
}


getinterventionDiagnostics()
{
  this.presentLoading();
  this.DiagnosticService.getDiagnosticIntervention(this.id).subscribe(response=>{
    this.dataDiagnosticDisplay=response;
    if(response['length']>0)
    {
      this.demarre=false;
      this.hidde=true;
    }
    //  this.dataDiagnosticDisplay.forEach(photo => {
    //     photo.PhotosDiagnostic.forEach(element => {
    //       this.photoPDF1.push('data:image/jpeg;base64,'+element);
    //     });
    //   });

      this.dismiss();
  });
}

getDiagnostics()
{
  this.DiagnosticService.getListDiagnostiques().subscribe(response=>{
    this.dataDiagnostic=response;
  });
}

// delete(item)
// {
//   this.confirmDeleted(item);
// }


onChangeMultiple(value){

}

 getListPieces()
 {
   this.pieceService.getAllPieces().subscribe(response=> {
    this.pieceData=response;
  });
 }

 getPieceDetail(item)
 {
  this.pieceService.getPiece(item).subscribe(response=>{
    this.dataPiece=response;
  });
 }


 getPicture(val)
 {
   const options: CameraOptions={
     quality:100,
     destinationType:this.camera.DestinationType.FILE_URI,
     encodingType:this.camera.EncodingType.JPEG,
     mediaType:this.camera.MediaType.PICTURE,
     saveToPhotoAlbum:false,
     correctOrientation:true,
     targetHeight:350,
     targetWidth:350
   };
   this.camera.getPicture(options).then((imageData)=>
   {
    if(val==1)
    {
      var indexR=this.imagesRapport.indexOf(1);
      if(indexR!==-1)
      {
        this.imagesRapport.splice(indexR,1)
      }
      
      this.imagesRapport.push(imageData);
      this.imagesRapport.push(1);
    }

    if(val==2)
    {
      
      
      var indexd=this.photosDiagnostic.indexOf(2);
      if(indexd!==-1)
      {
        
        
        this.photosDiagnostic.splice(indexd,1)
      }
      this.photosDiagnostic.push(imageData);
      this.photosDiagnostic.push(2);
    }
    
   });
 }

 onClearSignatureClient()
 {
   this.clientSignature.clear();
 }

 onClearSignatureAgent()
 {
   this.agentSignature.clear();
 }

 showPhoto()
 {
    //  this.photoviewer.show(this.rapportImage, "Image Rapport");
 }

  formatDate(date1){
    var dt = [];
     dt = date1.split('-');
    var year = dt[0];
    var month = dt[1];
    var day = dt[2];
    var dateFormated = day + "/" + month + "/" + year;
    return dateFormated;
  }


  /* generer le rapport final */
  numbreFormat(x){
   var result = new Intl.NumberFormat('de-DE', { style: 'decimal', currency: 'EUR' }).format(x);
  return result;

  }




getPeriodeIntervention(dateDemarage,dateFinal){
    var Date1 = new Date(dateDemarage).getTime();
    var Date2 =  new Date(dateFinal).getTime();
    var DiffTime = Math.abs(Date2-Date1);
    var DiffDayminute = Math.ceil(DiffTime/(1000*60));
    var min = 0;
    var diffMin =0;
if(DiffDayminute>=60){
   DiffDayminute = DiffDayminute/60;
   min = Math.trunc(DiffDayminute);
   diffMin = (DiffDayminute-min);
   var minute = diffMin*60;
   var heure = min;
} else {
  var minute = DiffDayminute;
}
if(heure>=24){
  heure = heure/24;
  var hr = Math.trunc(heure);
  var  diffhr = (DiffDayminute-hr);
   var heur = diffMin*60;
   var jour = hr;
} else {
  var heur = heure;
}

 if(isNaN(heur)){
  var result = minute +" minute(s) de temps ";
 } else if(jour==undefined){
     var result = Math.trunc(heur)+" heure(s) de temps ";
 } else if(isNaN(heur)){
  var result = minute +" minute(s) de temps ";
} else{
     var result = jour+" Jour(s)"; 
     }

    return result;
}


 genereRapport(){

  var doc = new jsPDF('p', 'pt');
  doc.setFont("helvetica");
  doc.setFontType("bold");
  
  var img = new Image();
  img.src = ('assets/logo.jpg');
  doc.addImage(img, 'jpg',30,17,80, 76);

  var columns = [
      { title: "", dataKey: "DescriptionProbleme" }
  ];

 // tslint:disable-next-line: align
  var dataDiagnostiques = [];
  var devisDatas = [];
 // tslint:disable-next-line: align
 if(this.dataDiagnosticDisplay==""||this.dataDiagnosticDisplay==="undefined"){
  let temp="";
 } else{
 this.dataDiagnosticDisplay.forEach(element => { 
  let temp = [element.DescriptionProbleme, element.Photo];
  dataDiagnostiques.push(temp);

});

}


if(this.dataDevis==""||this.dataDevis==="undefined"){
var temp = "";
}else {

this.to =0;
this.dataDevis.forEach(element => { 
  var temp = [element.IdPieces.designationPiece,element.Quantite,element.IdPieces.prixUnitairePiece,element.IdPieces.prixUnitairePiece*element.Quantite];
  devisDatas.push(temp);
  this.to += element.IdPieces.prixUnitairePiece*element.Quantite;
});

}

 if(this.to>0){
   this.total1 = this.to;
   }else{
  this.total1 =0;
  }

if(this.dataInterventionDisplay.typeIntervention==""||this.dataInterventionDisplay.typeIntervention===undefined &&
  this.raportData==""||this.raportData===undefined &&
  this.dataInterventionEquipe===""||this.dataInterventionEquipe===undefined
){ 
  
} else{
doc.setFontType("bold");
doc.text(200,60, 'RAPPORT INTERVENTION');
doc.line(200, 63,400, 63);

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(30,120,'Type d\'Intervention : ');
doc.setFontType("normal");
doc.text(130,120,this.dataInterventionDisplay.typeIntervention);
doc.setFontType("bold");
doc.text(30,140,'Date de Demarrage le :');
if(this.dataDiagnosticDisplay[0].dateDemarrage!==undefined){

var d3 = this.formatDate(this.dataDiagnosticDisplay[0].dateDemarrage.split('T')[0]);
doc.setFontType("normal");
doc.text(130,140,d3);
doc.setFontType("bold");
doc.text(180,140,' à');
var d4 = this.dataDiagnosticDisplay[0].dateDemarrage.split('T')[1].split('.')[0];
doc.setFontType("normal");
doc.text(195,140,d4);

} else{
doc.setFontType("normal");
doc.text(130,140,"");
doc.setFontType("bold");
doc.text(180,140,' à');
doc.setFontType("normal");
doc.text(195,140,"");

}
doc.setFontType("bold");
doc.text(30,160,'Date Fin intervention le:');
if(this.raportData[0]==""||this.raportData[0]=="undefined"){
  this.displayToastEdit2();
  return;
}
//------------------- ok ok  
var d6 = this.formatDate(this.raportData[0].DateFin.toString().slice(0,10));
doc.setFontType("normal");
doc.text(140,160,d6);
doc.setFontType("bold");
doc.text(185,160,' à');
var d7 = this.raportData[0].DateFin.toString().slice(11,16);
doc.setFontType("normal");
doc.text(195,160,d7);
doc.setFontType("bold");
doc.text(30,180,'Durée d\'Intervention:');
if(this.raportData[0].DateFin!=undefined&&this.dataDiagnosticDisplay[0].dateDemarrage!=undefined){
doc.setFontType("normal");
//------------------okok 
doc.text(130,180,this.getPeriodeIntervention(this.raportData[0].DateFin,this.dataDiagnosticDisplay[0].dateDemarrage));
} else{
doc.setFontType("normal");
doc.text(130,180,"");
}
doc.setFontType("bold");
doc.text(30,200,'Rdv le ');
doc.setFontType("normal");

if(this.dataInterventionDisplay.DateDebutIntervention!=undefined){

doc.text(130,200,this.formatDate(this.dataInterventionDisplay.DateDebutIntervention.split('T')[0]));
doc.setFontType("bold");
doc.text(185,200,'à');
doc.setFontType("normal");
doc.text(195,200,this.dataInterventionDisplay.DateDebutIntervention.split('T')[1].split('.')[0]);
doc.setFontType("bold");
doc.text(30,220,'Au');
doc.setFontType("normal");
doc.text(130,220,this.formatDate(this.dataInterventionDisplay.DateFinIntervention.split('T')[0]));
doc.setFontType("bold");
doc.text(185,220,'à');
doc.setFontType("normal");
doc.text(195,220,this.dataInterventionDisplay.DateFinIntervention.split('T')[1].split('.')[0]);

}else{
doc.text(130,200,"");
doc.setFontType("bold");
doc.text(180,200,'à');
doc.setFontType("normal");
doc.text(190,200,"");
doc.text(130,200,"");
doc.setFontType("bold");
doc.text(180,200,'à');
doc.setFontType("normal");
doc.text(190,200,"");
}
// doc.setFontType("bold");
// doc.text(30,220,'Fin d\'Intervention le:');
// doc.setFontType("normal");
// if(this.dataInterventionDisplay[0]==""||this.dataInterventionDisplay[0]===undefined){
// // pas de rapport enregistré
// this.displayToastEdit2();
// return;
// } else {
  //------------date fin planif 
// doc.text(130,220,this.formatDate(this.dataInterventionDisplay[0].DateFinIntervention));
// doc.setFontType("bold");
// doc.text(180,220,'à');
// doc.setFontType("normal");
// doc.text(190,220,this.dataInterventionDisplay[0].DateFinIntervention);
// }

doc.setFontType("bold");
doc.text(350,120, 'Ordre d\'intervention:');
doc.setFontType("normal");
doc.text(445,120,this.dataInterventionDisplay.OrdreIntervention);
doc.setFontType("bold");

doc.text(350,140, 'Adresse Chantier:');
doc.setFontType("normal");
doc.text(445,140,this.dataInterventionDisplay.LieuIntervention);
doc.setFontType("bold");
doc.text(350,160,'Zone:');
doc.setFontType("normal");
doc.text(445,160,this.dataInterventionEquipe.ZoneAffectee.NomZone);
doc.setFontType("bold");
doc.text(350,180,'Contact Client:');
doc.setFontType("normal");
doc.text(445,180,this.dataInterventionDisplay.Adresses);
doc.setFontSize(16);
}

var y=270;
var col = [
  { title: "Nom Equipe", dataKey: "NomEquipe" }
];
var  d1 = [];

if(this.dataInterventionEquipe==""||this.dataInterventionEquipe=="undefined"){
  var temp ="";
}else{
this.dataInterventionEquipe.EquipeAffectee.forEach(element => { 
var temp = [element.NomEquipe];
d1.push(temp); 
});
}

doc.autoTable(col,d1, {
  startY:y+10,
   pageBreak: 'avoid',

  headStyles: {
      textColor: [255, 255, 255],
      halign: 'left'
  },

  bodyStyles: {
      halign: 'left'
  },
  margin: {top:50},
  theme: 'striped'
  });

doc.setFontSize(16);
var col = [
  { title: "Autre(s) Agent(s)", dataKey: "NomUser" }

];
var  d2 = [];


if(this.dataInterventionEquipe==""||this.dataInterventionEquipe=="undefined"){
  var temp ="";
}else{
this.dataInterventionEquipe.AutreAgents.forEach(element => { 
var temp = [element.NomUser];
d2.push(temp); 
});
}

doc.autoTable(col,d2, {
  startY:y+10,
  pageBreak: 'avoid',

  headStyles: {
      textColor: [255, 255, 255],
      halign: 'left'
  },
  bodyStyles: {
    halign: 'left'
  },
  margin: {left:300,top:30},
  theme: 'striped',
  
  });

let start1 = doc.previousAutoTable.finalY +60;
doc.text(30,start1," 1- Diagnostic");
doc.autoTable(columns, dataDiagnostiques, {
      startY:start1 +5,
       headStyles: {
      fillColor: [255, 255, 255],
      textColor: [100, 100, 111],
      halign: 'left'
  } 
});

let start2= doc.previousAutoTable.finalY +30;
doc.text(30,start2, '2 - Photos avant Intervention');
var position = 50;


this.dataDiagnosticDisplay.forEach(photo => {
  photo.PhotosDiagnostic.forEach(element => {
    this.photoPDF1.push('data:image/jpeg;base64,'+element);
  });
});

this.raportData.forEach(photo => {
  photo.Photos.forEach(element => {
    this.photoPDF.push('data:image/jpeg;base64,'+element);
  });
});

this.raportData.forEach(signature => {
this.signatureClient = signature.SignatureClient;
this.signatureIntervenant = signature.SignatureIntervenant;
});


if(this.photoPDF1==""||this.photoPDF1=="undefined")
{
 
  doc.setFontType("normal");
  doc.setFontSize(9);
   doc.text(position,start2+30,"Aucune photo prise")
} else {

this.photoPDF1.slice(0,3).forEach(img => {
  doc.addImage(img, 'png',position, start2+30,80,80);
  position+=180;

});
}

doc.setFontSize(16);
doc.text(30,start2+190,' 3 - Photos après Intervention ');
if(this.photoPDF1==""||this.photoPDF1=="undefined")
{
 
   doc.setFontType("normal");
   doc.setFontSize(9);
   doc.text(position, start2+220,"Aucune photo prise");
} else {
var position1 = 50;
this.photoPDF.slice(0,3).forEach(img => {
  doc.addImage(img, 'png',position1, start2+220,80,80);
  position1+=180;
});
}

doc.addPage();
doc.setFontSize(16);
doc.text(30,40, '4 - Dévis');
// tslint:disable-next-line: align
 var columns4 = [
    { title: "Désignation pièce", dataKey: "designationPiece" },
    { title: "Qté", dataKey: "Quantite" },
    { title: "P.U (Fcfa)", dataKey: "prixUnitairePiece" },
    { title: "Montant (Fcfa)", dataKey: "" }
];

 doc.autoTable(columns4, devisDatas, {
         startY: 60
});

let start3 = doc.previousAutoTable.finalY +160;
if(this.dataInterventionDisplay.mainDoeuvre==""||this.dataInterventionDisplay.mainDoeuvre==="undefined"){
   var mainDoeuvres=0;
}else{
 mainDoeuvres = this.dataInterventionDisplay.mainDoeuvre;
 if(mainDoeuvres>0){

   mainDoeuvres =  this.dataInterventionDisplay.mainDoeuvre;
 
}else {
  mainDoeuvres = 0;
}
}

  this.tva = 0.18;
  this.totalHorsTax1=(this.total1+mainDoeuvres);
  this.Mtva = this.totalHorsTax1*this.tva;
  this.ttc = this.totalHorsTax1*(1+this.tva);

 doc.autoTable({
    columnStyles: {0: {halign: 'right'}}, 
    margin: {top:10,right:40},
    body: [['Main d\'oeuvre', '',this.numbreFormat(mainDoeuvres)],
           ['Montant HT', '', this.numbreFormat(this.totalHorsTax1)], 
           ['TVA 18%', '', this.numbreFormat(this.Mtva)],
           ['Montant TTC', '',this.numbreFormat(this.ttc)]]
});

doc.setFontSize(16);
doc.text(30,start3,"5 - Rapport");
let start4 =  start3+ 20

var columns5 = [
  { title: "Date de fin", dataKey: "DateFin" },
  { title: "Description travail effectué ", dataKey: "DescriptionTravail" }

];

  var raporData = [];
if(this.raportData==""||this.raportData=="undefined"){
  var temp ="";
}else{
this.raportData.forEach(element => { 
  if(element.DateFin!==undefined){
  var temp = [this.formatDate(element.DateFin.toString().slice(0,10)),element.DescriptionTravail];
  raporData.push(temp);
  }
});
}

doc.autoTable(columns5, raporData,{
  startY : start4
});

let start5 = doc.previousAutoTable.finalY +100;
doc.setFontSize(11);
doc.text(30,start5, 'Signature de l\'Intervenant :');
doc.text(400,start5, 'Signature du Client :');

if(this.signatureIntervenant!=undefined){
  doc.addImage(this.signatureIntervenant,'PNG',40,start5+30,60,40);
}else{

doc.text(40,start5+30,"");
}
if(this.signatureClient!=undefined){
  doc.addImage(this.signatureClient, 'PNG',430,start5+10,60,40);
  
}else{
  doc.text(430,start5+10,"");
}

doc.save('RAPPORT-FINAL-'+this.dataInterventionDisplay.OrdreIntervention+'.pdf'); 


}

/* voir  devis  avant de genrer */
// voirPDFdevis(){
 
//   var doc = new jsPDF('p', 'pt');
//   doc.setFont("helvetica");
//   doc.setFontType("bold");

//   var img = new Image();
//   img.src = ('assets/logo.jpg');
//  doc.addImage(img, 'JPG',30,17,80, 76);

//   var columns = [
//       { title: "", dataKey: "DescriptionProbleme" }
//   ];

//  // tslint:disable-next-line: align
//   var dataDiagnostiques = [];
//   var devisDatas = [];
//  // tslint:disable-next-line: align
//  if(this.dataDiagnosticDisplay==""||this.dataDiagnosticDisplay==="undefined"){
//   let temp="";
//  } else{
//  this.dataDiagnosticDisplay.forEach(element => { 
//   let temp = [element.DescriptionProbleme, element.Photo];
//   dataDiagnostiques.push(temp);
// });
// }

// if(this.dataDevis==""||this.dataDevis==="undefined"){
// this.displayToastEdit3();
// return;
// var temp = "";
// }else {
// this.to = 0;
// this.dataDevis.forEach(element => { 
//   var temp = [element.IdPieces.designationPiece,element.Quantite,element.IdPieces.prixUnitairePiece,element.IdPieces.prixUnitairePiece*element.Quantite];
//   devisDatas.push(temp);
//   this.to += element.IdPieces.prixUnitairePiece*element.Quantite;
//   if(this.to>0){
//     this.total1 = this.to;
//     }else{
//       this.total1 =0;
//     }
  
// });

// }
// if(this.dataInterventionDisplay.typeIntervention==""||this.dataInterventionDisplay.typeIntervention===undefined &&
//   this.raportData==""||this.raportData===undefined &&
//   this.dataInterventionEquipe===""||this.dataInterventionEquipe===undefined
// ){ 
  
// } else{
// doc.setFontType("bold");
// doc.text(200,60, 'RAPPORT INTERVENTION');
// doc.line(200, 63,400, 63);

// doc.setFontType("normal");
// doc.setFontSize(9);
// doc.setFontType("bold");
// doc.text(30,120,'Type d\'Intervention : ');
// doc.setFontType("normal");
// doc.text(130,120,this.dataInterventionDisplay.typeIntervention);
// doc.setFontType("bold");
// doc.text(30,140,'Date de Demarrage le :');

// if(this.dataDiagnosticDisplay[0].dateDemarrage!==undefined){
// var d3 = this.formatDate(this.dataDiagnosticDisplay[0].dateDemarrage.split('T')[0]);
// doc.setFontType("normal");
// doc.text(130,140,d3);
// doc.setFontType("bold");
// doc.text(180,140,' à');
// var d4 = this.dataDiagnosticDisplay[0].dateDemarrage.split('T')[1].split('.')[0];
// doc.setFontType("normal");
// doc.text(195,140,d4);
// } else{
// doc.setFontType("normal");
// doc.text(130,140,"");
// doc.setFontType("bold");
// doc.text(180,140,' à');
// doc.setFontType("normal");
// doc.text(195,140,"");
// }
// doc.setFontType("bold");
// doc.text(30,160,'Durée d\'Intervention:');
// if(this.dataInterventionDisplay.DateFinIntervention!=undefined&&this.dataDiagnosticDisplay[0].dateDemarrage!=undefined){
// doc.setFontType("normal");
// doc.text(130,160,this.getPeriodeIntervention(this.dataInterventionDisplay.DateFinIntervention,this.dataDiagnosticDisplay[0].dateDemarrage));
// } else{
// doc.setFontType("normal");
// doc.text(130,160,"");
// }
// doc.setFontType("bold");
// doc.text(30,180,'Rdv le ');
// doc.setFontType("normal");
// if(this.dataInterventionDisplay.DateDebutIntervention!=undefined){
// doc.text(130,180,this.formatDate(this.dataInterventionDisplay.DateDebutIntervention.toString().slice(0,10)));
// doc.setFontType("bold");
// doc.text(180,180,'à');
// doc.setFontType("normal");
// doc.text(190,180,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(12,16));
// }else{
// doc.text(130,180,"");
// doc.setFontType("bold");
// doc.text(180,180,'à');
// doc.setFontType("normal");
// doc.text(190,180,"");
// }

// doc.setFontType("bold");
// doc.text(30,200,'Fin d\'Intervention le:');
// doc.setFontType("normal");
// doc.text(130,200,"");
// doc.setFontType("bold");
// doc.text(180,200,'à');
// doc.setFontType("normal");
// doc.text(190,200,"");

// doc.setFontType("bold");
// doc.text(350,120, 'Ordre d\'intervention:');
// doc.setFontType("normal");
// doc.text(445,120,this.dataInterventionDisplay.OrdreIntervention);
// doc.setFontType("bold");

// doc.text(350,140, 'Adresse Chantier:');
// doc.setFontType("normal");
// doc.text(445,140,this.dataInterventionDisplay.LieuIntervention);
// doc.setFontType("bold");
// doc.text(350,160,'Zone:');
// doc.setFontType("normal");
// doc.text(445,160,this.dataInterventionEquipe.ZoneAffectee.NomZone);
// doc.setFontType("bold");
// doc.text(350,180,'Contact Client:');
// doc.setFontType("normal");
// doc.text(445,180,this.dataInterventionDisplay.Adresses);
// doc.setFontSize(16);
// }



// var y=270;
// var col = [
//   { title: "Nom Equipe", dataKey: "NomEquipe" }

// ];

// // tslint:disable-next-line: align
// var  d1 = [];
// // tslint:disable-next-line: align

// if(this.dataInterventionEquipe==""||this.dataInterventionEquipe=="undefined"){
//   var temp ="";
// }else{
// this.dataInterventionEquipe.EquipeAffectee.forEach(element => { 
// var temp = [element.NomEquipe];
// d1.push(temp); 
// });
// }

// doc.autoTable(col,d1, {
//   startY:y+10,
//    pageBreak: 'avoid',
//   headStyles: {
//       textColor: [255, 255, 255],
//       halign: 'left'
//   },
//   bodyStyles: {
//       halign: 'left'
//   },
//   margin: {top:50},
//   theme: 'striped'
//   });
// doc.setFontSize(16);
// var col = [
//   { title: "Nom Agent", dataKey: "NomUser" }

// ];
// var  d2 = [];


// if(this.dataInterventionEquipe==""||this.dataInterventionEquipe=="undefined"){
//   var temp ="";
// }else{
// this.dataInterventionEquipe.AutreAgents.forEach(element => { 
// var temp = [element.NomUser];
// d2.push(temp); 
// });
// }

// doc.autoTable(col,d2, {
//   startY:y+10,
//   pageBreak: 'avoid',

//   headStyles: {
//       textColor: [255, 255, 255],
//       halign: 'left'
//   },
//   bodyStyles: {
//     halign: 'left'
//   },
//   margin: {left:300,top:30},
//   theme: 'striped',
  
//   });

// let start1 = doc.previousAutoTable.finalY +60;
// doc.text(30,start1," 1- Diagnostic");

// doc.autoTable(columns, dataDiagnostiques, {
//       startY:start1 +5,
//        headStyles: {
//       fillColor: [255, 255, 255],
//       textColor: [100, 100, 111],
//       halign: 'left'
//   } 
// });

// let start2= doc.previousAutoTable.finalY +30;
// doc.text(30,start2, '2 - Photos avant Intervention');
// var position = 50;
// if(this.photoPDF1==""||this.photoPDF1=="undefined")
// {
//   doc.setFontType("normal");
//   doc.setFontSize(9);
//   doc.text(position,start2+30,"Aucune photo prise")
// } else {
// this.photoPDF1.slice(0,3).forEach(img => {
//   doc.addImage(img, 'JPEG',position, start2+30, 140, 140);
//   position+=180;
// });
// }

// doc.addPage();
// doc.setFontSize(16);
// doc.text(30,40, '3 - Dévis');
// // tslint:disable-next-line: align
//  var columns4 = [
//     { title: "Désignation pièce", dataKey: "designationPiece" },
//     { title: "Qté", dataKey: "Quantite" },
//     { title: "P.U (Fcfa)", dataKey: "prixUnitairePiece" },
//     { title: "Montant (Fcfa)", dataKey: "" }
// ];

//  doc.autoTable(columns4, devisDatas, {
//          startY: 60
// });

// let start3 = doc.previousAutoTable.finalY +160;
// var columntotal = [
//   { title: "Main d\'oeuvre", dataKey: "" },
//   { title: "Total Htaxe", dataKey: "" },
//   { title: "tva", dataKey: "" },
//   { title: "Tttc", dataKey: "" }
// ];

// if(this.dataInterventionDisplay.mainDoeuvre==""||this.dataInterventionDisplay.mainDoeuvre==="undefined"){
//    var mainDoeuvres=0;
// }else{
//  mainDoeuvres = this.dataInterventionDisplay.mainDoeuvre;
//  if(mainDoeuvres>0){
//    mainDoeuvres =  this.dataInterventionDisplay.mainDoeuvre;
// }else {
//   mainDoeuvres = 0;
// }
// }

//  let tva = 0.18;
//  let totalDevis = this.total1;
//  let totalHorsTax1=totalDevis+mainDoeuvres;
//  let Mtva = totalHorsTax1*tva;
//  let ttc = totalHorsTax1*(1+tva);

//  doc.autoTable({
//     columnStyles: {0: {halign: 'right'}}, // Cells in first column centered and green
//     margin: {top:10,right:40},
//     body: [['Main d\'oeuvre', '',this.numbreFormat(mainDoeuvres)],
//            ['Montant HT', '', this.numbreFormat(totalHorsTax1)], 
//            ['TVA 18%', '', this.numbreFormat(Mtva)],
//            ['Montant TTC', '',this.numbreFormat(ttc)]]
// });

// let start5 = doc.previousAutoTable.finalY +100;
// doc.setFontSize(11);
// doc.text(30,start5, 'Signature de l\'Intervenant :');
// doc.text(400,start5, 'Signature du Client :');
// var string = doc.output('datauristring');
// var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
// var x = window.open();
// x.document.open();
// x.document.write(embed);
// x.document.close();

// }

/* generer le rapport de devis   */
generateDevis(){
  var doc = new jsPDF('p', 'pt');
  doc.setFont("helvetica");
  doc.setFontType("bold");

  var img = new Image();
  img.src = ('assets/logo.jpg');
 doc.addImage(img, 'jpg',30,17,80, 76);

  var columns = [
      { title: "", dataKey: "DescriptionProbleme" }
  ];

 // tslint:disable-next-line: align
  var dataDiagnostiques = [];
  var devisDatas = [];
 // tslint:disable-next-line: align
 if(this.dataDiagnosticDisplay==""||this.dataDiagnosticDisplay==="undefined"){
  let temp="";
 } else{
 this.dataDiagnosticDisplay.forEach(element => { 
  let temp = [element.DescriptionProbleme, element.Photo];
  dataDiagnostiques.push(temp);

});

}

if(this.dataDevis==""||this.dataDevis==="undefined"){
this.displayToastEdit3();
return;
var temp = "";
}else {
  
this.to =0;
this.dataDevis.forEach(element => { 
  var temp = [element.IdPieces.designationPiece,element.Quantite,element.IdPieces.prixUnitairePiece,element.IdPieces.prixUnitairePiece*element.Quantite];
  devisDatas.push(temp);
  this.to += element.IdPieces.prixUnitairePiece*element.Quantite;
  if(this.to>0){
    this.total1 = this.to;
    }else{
      this.total1 =0;
    }
});

}

if(this.dataInterventionDisplay.typeIntervention==""||this.dataInterventionDisplay.typeIntervention===undefined &&
  this.raportData==""||this.raportData===undefined &&
  this.dataInterventionEquipe===""||this.dataInterventionEquipe===undefined
){ 
  
} else{
doc.setFontType("bold");
doc.text(200,60, 'DEVIS INTERVENTION');
doc.line(200, 63,400, 63);

doc.setFontType("normal");
doc.setFontSize(9);
doc.setFontType("bold");
doc.text(30,120,'Type d\'Intervention : ');
doc.setFontType("normal");
doc.text(130,120,this.dataInterventionDisplay.typeIntervention);
doc.setFontType("bold");
doc.text(30,140,'Date de Demarrage le :');

if(this.dataDiagnosticDisplay[0].dateDemarrage!==undefined){
var d3 = this.formatDate(this.dataDiagnosticDisplay[0].dateDemarrage.split('T')[0]);
doc.setFontType("normal");
doc.text(130,140,d3);
doc.setFontType("bold");
doc.text(180,140,' à');
var d4 = this.dataDiagnosticDisplay[0].dateDemarrage.split('T')[1].split('.')[0];
doc.setFontType("normal");
doc.text(195,140,d4);
} else{
doc.setFontType("normal");
doc.text(130,140,"");
doc.setFontType("bold");
doc.text(180,140,' à');
doc.setFontType("normal");
doc.text(195,140,"");
}

// doc.setFontType("bold");
// doc.text(30,160,'Durée d\'Intervention:');
// if(this.dataInterventionDisplay.DateFinIntervention!=undefined&&this.dataDiagnosticDisplay[0].dateDemarrage!=undefined){
// doc.setFontType("normal");
// doc.text(130,160,this.getPeriodeIntervention(this.dataInterventionDisplay.DateFinIntervention,this.dataDiagnosticDisplay[0].dateDemarrage));
// } else{
// doc.setFontType("normal");
// doc.text(130,160,"");
// }
doc.setFontType("bold");
doc.text(30,160,'Rdv le ');
doc.setFontType("normal");
if(this.dataInterventionDisplay.DateDebutIntervention!=undefined&&this.dataInterventionDisplay.DateFinIntervention){

doc.text(130,160,this.formatDate(this.dataInterventionDisplay.DateDebutIntervention.toString().slice(0,10)));
doc.setFontType("bold");
doc.text(185,160,'à');
doc.setFontType("normal");
doc.text(195,160,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(11,16));
doc.setFontType("bold");
doc.text(30,180,'Au ');
doc.setFontType("normal");
doc.text(130,180,this.formatDate(this.dataInterventionDisplay.DateFinIntervention.toString().slice(0,10)));
doc.setFontType("bold");
doc.text(185,180,'à');
doc.setFontType("normal");
doc.text(195,180,this.dataInterventionDisplay.DateFinIntervention.toString().slice(11,16));

}else{
  doc.text(130,160,"");
  doc.setFontType("bold");
  doc.text(185,160,'à');
  doc.setFontType("normal");
  doc.text(190,160,"");

doc.text(130,180,"");
doc.setFontType("bold");
doc.text(180,180,'à');
doc.setFontType("normal");
doc.text(190,180,"");
}
// doc.setFontType("bold");
// doc.text(30,200,'Fin d\'Intervention le:');
// doc.setFontType("normal");
// doc.text(130,200,"");
// doc.setFontType("bold");
// doc.text(180,200,'à');
// doc.setFontType("normal");
// doc.text(190,200,"");

doc.setFontType("bold");
doc.text(350,120, 'Ordre d\'intervention:');
doc.setFontType("normal");
doc.text(445,120,this.dataInterventionDisplay.OrdreIntervention);
doc.setFontType("bold");

doc.text(350,140, 'Adresse Chantier:');
doc.setFontType("normal");
doc.text(445,140,this.dataInterventionDisplay.LieuIntervention);
doc.setFontType("bold");
doc.text(350,160,'Zone:');
doc.setFontType("normal");
doc.text(445,160,this.dataInterventionEquipe.ZoneAffectee.NomZone);
doc.setFontType("bold");
doc.text(350,180,'Contact Client:');
doc.setFontType("normal");
doc.text(445,180,this.dataInterventionDisplay.Adresses);
doc.setFontSize(16);
}

var y=270;
var col = [
  { title: "Nom Equipe", dataKey: "NomEquipe" }

];

var  d1 = [];
if(this.dataInterventionEquipe==""||this.dataInterventionEquipe=="undefined"){
  var temp ="";
}else{
this.dataInterventionEquipe.EquipeAffectee.forEach(element => { 
var temp = [element.NomEquipe];
d1.push(temp); 
});
}

doc.autoTable(col,d1, {
  startY:y+10,
   pageBreak: 'avoid',

  headStyles: {
      textColor: [255, 255, 255],
      halign: 'left'
  },

  bodyStyles: {
      halign: 'left'
  },
  margin: {top:50},
  theme: 'striped'
  });
doc.setFontSize(16);
var col = [
  { title: "Autre(s) Agent(s)", dataKey: "NomUser" }
];
var  d2 = [];
if(this.dataInterventionEquipe==""||this.dataInterventionEquipe=="undefined"){
  var temp ="";
}else{
this.dataInterventionEquipe.AutreAgents.forEach(element => { 
var temp = [element.NomUser];
d2.push(temp); 
});
}

doc.autoTable(col,d2, {
  startY:y+10,
  pageBreak: 'avoid',
  headStyles: {
      textColor: [255, 255, 255],
      halign: 'left'
  },
  bodyStyles: {
    halign: 'left'
  },
  margin: {left:300,top:30},
  theme: 'striped',
  
  });


let start1 = doc.previousAutoTable.finalY +60;
doc.text(30,start1," 1- Diagnostic");

doc.autoTable(columns, dataDiagnostiques, {
      startY:start1 +5,
       headStyles: {
      fillColor: [255, 255, 255],
      textColor: [100, 100, 111],
      halign: 'left'
  } 
});

let start2= doc.previousAutoTable.finalY +30;
doc.text(30,start2, '2 - Photos avant Intervention');
var position = 50;


this.dataDiagnosticDisplay.forEach(photo => {
  photo.PhotosDiagnostic.forEach(element => {
    this.photoPDF1.push('data:image/jpeg;base64,'+element);
  });
});

// this.raportData.forEach(photo => {
//   photo.Photos.forEach(element => {
//     this.photoPDF.push('data:image/jpeg;base64,'+element);
//   });
// });

// this.raportData.forEach(signature => {
// this.signatureClient = signature.SignatureClient;
// this.signatureIntervenant = signature.SignatureIntervenant;
// });

if(this.photoPDF1==""||this.photoPDF1=="undefined")
{
  doc.setFontType("normal");
  doc.setFontSize(9);
  doc.text(position,start2+30,"Aucune photo prise")
} else {
this.photoPDF1.slice(0,3).forEach(img => {
  doc.addImage(img, 'JPEG',position, start2+30, 100, 100);
  position+=180;
});
}
doc.addPage();
doc.setFontSize(16);
doc.text(30,40, '3 - Dévis');
// tslint:disable-next-line: align
 var columns4 = [
    { title: "Désignation pièce", dataKey: "designationPiece" },
    { title: "Qté", dataKey: "Quantite" },
    { title: "P.U (Fcfa)", dataKey: "prixUnitairePiece" },
    { title: "Montant (Fcfa)", dataKey: "" }
];

 doc.autoTable(columns4, devisDatas, {
         startY: 60
});

let start3 = doc.previousAutoTable.finalY +160;

if(this.dataInterventionDisplay.mainDoeuvre==""||this.dataInterventionDisplay.mainDoeuvre==="undefined"){
   var mainDoeuvres=0;
}else{
 mainDoeuvres = this.dataInterventionDisplay.mainDoeuvre;
 if(mainDoeuvres>0){
   mainDoeuvres =  this.dataInterventionDisplay.mainDoeuvre;
}else {
  mainDoeuvres = 0;
}
}

 let tva = 0.18;
 let totalDevis = this.total1;
 let totalHorsTax1=totalDevis+mainDoeuvres;
 let Mtva = totalHorsTax1*tva;
 let ttc = totalHorsTax1*(1+tva);

 doc.autoTable({
    columnStyles: {0: {halign: 'right'}}, // Cells in first column centered and green
    margin: {top:10,right:40},
    body: [['Main d\'oeuvre', '',this.numbreFormat(mainDoeuvres)],
           ['Montant HT', '', this.numbreFormat(totalHorsTax1)], 
           ['TVA 18%', '', this.numbreFormat(Mtva)],
           ['Montant TTC', '',this.numbreFormat(ttc)]]
});

let start5 = doc.previousAutoTable.finalY +100;
doc.setFontSize(11);
//doc.text(30,start5, 'Signature de l\'Intervenant :');
doc.text(400,start5, 'Signature du Client :');

doc.save('RAPPORT-DIAGNOSTIC-'+this.dataInterventionDisplay.OrdreIntervention+'.pdf'); 
 
}

}
