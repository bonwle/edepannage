import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InterventionEditTestPage } from './intervention-edit-test.page';

describe('InterventionEditTestPage', () => {
  let component: InterventionEditTestPage;
  let fixture: ComponentFixture<InterventionEditTestPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterventionEditTestPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InterventionEditTestPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
