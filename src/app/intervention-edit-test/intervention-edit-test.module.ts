import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InterventionEditTestPageRoutingModule } from './intervention-edit-test-routing.module';

import { InterventionEditTestPage } from './intervention-edit-test.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SignaturePadModule } from 'angular2-signaturepad';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InterventionEditTestPageRoutingModule,
    NgxDatatableModule,
    SignaturePadModule, OwlDateTimeModule, OwlNativeDateTimeModule,
    OwlFormFieldModule, OwlInputModule, OwlSelectModule
  ],
  declarations: [InterventionEditTestPage]
})
export class InterventionEditTestPageModule {}
