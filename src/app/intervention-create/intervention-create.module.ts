import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { InterventionCreatePageRoutingModule } from './intervention-create-routing.module';

import { InterventionCreatePage } from './intervention-create.page';
import { SignaturePadModule } from 'angular2-signaturepad';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';
/*
import { DynamicFormsCoreModule } from '@ng-dynamic-forms/core';
import { DynamicFormsMaterialUIModule } from '@ng-dynamic-forms/ui-material';
import { DynamicFormsFoundationUIModule } from '@wf-dynamic-forms/ui-foundation';

*/
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignaturePadModule,
    InterventionCreatePageRoutingModule,
    OwlDateTimeModule,
   ReactiveFormsModule,
    OwlNativeDateTimeModule, OwlFormFieldModule, OwlInputModule, OwlSelectModule
  ],
  declarations: [InterventionCreatePage]
})
export class InterventionCreatePageModule {}
