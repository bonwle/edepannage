import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InterventionCreatePage } from './intervention-create.page';

describe('InterventionCreatePage', () => {
  let component: InterventionCreatePage;
  let fixture: ComponentFixture<InterventionCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterventionCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InterventionCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
