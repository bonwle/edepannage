import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { Interventions } from '../models/interventions';
import { InterventionService } from '../services/intervention.service';
import { Router } from '@angular/router';
import { Diagnostique } from '../models/diagnostique';
import { ToastController } from '@ionic/angular';
import { UtilisateursService } from '../services/utilisateurs.service';
import { EquipeService} from '../services/equipe.service';
import {Zones} from '../models/zones';
import {ZoneService} from '../services/zone.service';
import { ThrowStmt } from '@angular/compiler';


// --------------class ou model 
//agentaffectes ----equipe -----zone -----


@Component({
  selector: 'app-intervention-create',
  templateUrl: './intervention-create.page.html',
  styleUrls: ['./intervention-create.page.scss'],
})
export class InterventionCreatePage implements OnInit {

  dataIntervention: Interventions;
  AddIntervention: string;
  dataDiagnostique: Diagnostique;
  dataInterventionDisplay: any;
  /*-----add-------*/
  tableStyle = 'material';
  public columns: any;
  public rows: any;
  dataUser: any;
  dataEquipe: any;
  dataZone: any;
  zoneequipesData: any;
  zones: Zones;
  equipes: any;
  user: any;
  otherUser: any;
  form: FormGroup;
  zoneControl: FormControl;
  equipeControl: FormControl;
  agentControl: FormControl;
  equipeVal: [any];
  number1:number;
  dateCode:string;
  code:any;
  nbre: number;
  ordre:string;

    constructor(
      private equipeService: EquipeService,
      private zoneService: ZoneService,
      private userService: UtilisateursService,
      private serviceIntervention: InterventionService,
      private router: Router,
      private toastController: ToastController,
      )
      { 
        this.dataIntervention= new Interventions();
        this.dataDiagnostique = new Diagnostique();
        this.dataInterventionDisplay = []; 
        this.dataUser =[];
        this.dataEquipe=[];
        this.dataZone=[];
        this.zoneequipesData=[];
        this.zones = new Zones();
        this.equipes = [];
        this.user = [];
        this.otherUser = [];
        this.number1 = 1;
        this.dateCode ="";
        this.code =[];
        this.nbre=0;
        this.ordre="";
      }

      ngOnInit() 
      {
        this.AddIntervention='AddDetail';
        this.getListInterventions();
        this.getUserAgent();
        this.getEquipeData();
        this.getZone();
        // this.generatUnicNumber();
        this.getOrdreIntervention();
      }
  

      getOrdreIntervention()
      {
        // this.getListInterventions();
        this.serviceIntervention.getInterventions().subscribe(response=>{
        this.nbre=response['length'];
        var date= new Date();
        this.ordre=date.getFullYear()+'-';
        var val=(this.nbre+1).toString();
        this.nbre=this.nbre.toString().length
        switch (this.nbre) {
          case 1:
            this.nbre=9;
            break;
          case 2:
            this.nbre=8;
            break;
          case 3:
            this.nbre=7;
            break;
          case 4:
            this.nbre=6;
            break;
          case 5:
            this.nbre=5;
            break;
          default:
            break;
        }

        this.dataIntervention.OrdreIntervention=this.ordre.padEnd(this.nbre,'0')+val;
      });
      }

formatDate(date) {

  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;
  return [ day, month, year].join('-');
}


generatUnicNumber(){
  this.number1= 1 + Math.floor(Math.random() * 999999999);
   this.dateCode = new Date(Date.now()).toString();
  
   this.code = this.formatDate(this.dateCode)+"-"+this.number1;
   console.log(this.code);
  console.log(this.formatDate(this.dateCode)+"-"+this.number1);
}

 getListInterventions()
  {
    this.serviceIntervention.getInterventions().subscribe(response=>{
      console.log(response+"liste ok ");
      this.nbre=response['length'];
      console.log(this.nbre);
    });
  }

  
  getUserAgent()
  {
    this.userService.getUtilisateurs().subscribe(response=>{
      console.log(response+"liste user ok ");
      this.dataUser=response;
      this.otherUser =response;
      console.log(Object.keys(response).length+"size ici");
    });
  }

  getOtherUser(item1)
  {
    this.equipeService.getAgentNotInEquipe(item1).subscribe((response)=>{
    this.otherUser=response;
    console.log(this.otherUser+"autre agent");
    });
  }

  getEquipeData()
  {
    this.equipeService.getEquipes().subscribe(response=>{
      console.log(response+"liste equipe ok ");
      this.dataEquipe = response;
    });
  }


  getZoneEquipes(item)
  {
    console.log(item);
    this.zoneService.getZoneEquipe(item).subscribe(response=>{
    this.dataEquipe= response;
  });
}


  getAgentsEquipe(item)
  {
    this.equipeService.getEquipeAgent(item).subscribe(response=>{
      this.dataUser = response;
      this.getOtherUser(item);
      console.log(this.dataUser);
    });
  }

  

  getZone()
  {
    this.zoneService.getZones().subscribe(response=>{
      this.dataZone = response;
    });
  }



      submitForm()
      {
       // console.log(this.dataIntervention.OrdreIntervention)
        this.serviceIntervention.createIntervention(this.dataIntervention).subscribe((response)=>{
          console.log(response+"liste interventon");
          this.router.navigate(['intervention-list']);
        });
      }
   
      showToast() {
        this.toastController.create({
          message: 'data added',
          duration: 4000,
          animated: true,
          showCloseButton: true,
          closeButtonText: "OK",
          position: "top"
        }).then((obj) => {
          obj.present();
        });
      }

}
