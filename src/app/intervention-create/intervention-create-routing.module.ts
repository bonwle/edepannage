import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InterventionCreatePage } from './intervention-create.page';

const routes: Routes = [
  {
    path: '',
    component: InterventionCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterventionCreatePageRoutingModule {}
