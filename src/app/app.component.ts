import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UtilisateursService } from './services/utilisateurs.service';
import { HomePage } from './home/home.page';
import { UserContext } from './models/user-context';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit{
  // public isRoleMenuPiece: boolean=false;


  public appPages = [
    
    {
      title: 'DashBoard',
      url: '/home',
      icon: 'home',
      // access:true
    },
    {
      title: 'Administrateur',
      url: '/administrateur',
      icon: 'settings',
      // access:true
    },
    {
      title: 'Pièces',
      url: '/piece-list',
      icon: 'hammer',
      // access:this.isRoleMenuPiece
    },
    {
      title: 'Interventions',
      url: '/intervention-list',
      icon: 'document',
      // access:true
    },

    // {
    //   title: 'Mon compte',
    //   url: '/utilisateur-edit/'+UserContext.userConnectId,
    //   icon: 'person',
    //   // access:true
    // },

    // {
    //   title: 'Rapport',
    //   url: '/rapport-create',
    //   icon: 'person',
    //   // access:true
    // }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    // private userAuth: HomePage,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    // console.log(this.userAuth.userConnectData);
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

    });
  }

  ngOnInit()
  {
    console.log(UserContext.userConnect);
    console.log("User connect");
  }

  // ionViewDidEnter()
  // {
  //   this.isRoleMenuPiece=UserContext.userRolesList.indexOf("VLPI01")==-1;
  // }
}
