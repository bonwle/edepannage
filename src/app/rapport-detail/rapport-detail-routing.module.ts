import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RapportDetailPage } from './rapport-detail.page';

const routes: Routes = [
  {
    path: '',
    component: RapportDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RapportDetailPageRoutingModule {}
