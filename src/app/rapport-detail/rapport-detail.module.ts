import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RapportDetailPageRoutingModule } from './rapport-detail-routing.module';

import { RapportDetailPage } from './rapport-detail.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SignaturePadModule,
    IonicModule,OwlDateTimeModule, OwlNativeDateTimeModule, OwlFormFieldModule,
    OwlInputModule, OwlSelectModule,
    RapportDetailPageRoutingModule
  ],
  declarations: [RapportDetailPage]
})
export class RapportDetailPageModule {}
