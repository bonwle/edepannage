import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RapportService } from '../services/rapport.service';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
@Component({
  selector: 'app-rapport-detail',
  templateUrl: './rapport-detail.page.html',
  styleUrls: ['./rapport-detail.page.scss'],
})
export class RapportDetailPage implements OnInit {

  dataRaport :any;
  id:number;
  dateFormatdebut :any;
  dateFormatFin :any;

  @ViewChild("clientSignature", {static:false}) clientSignature: SignaturePad;
  @ViewChild("agentSignature", {static:false}) agentSignature: SignaturePad;

  private Options:object=
 {
   'canvasHeight':80,
 }
  
 
  constructor(
    public activatedRoute: ActivatedRoute, 
    public rapportService : RapportService,
  ) {
    this.dataRaport=[];
    this.dateFormatFin =[];
    this.dateFormatdebut= [];
   }

  ngOnInit() {
    this.id=this.activatedRoute.snapshot.params["id"];
    this.getRapportDetail();
  }


  formatDate(date1){
    var dt = [];
     dt = date1.split('-');
    var year = dt[0];
    var month = dt[1];
    var day = dt[2];
    var dateFormated = day + "-" + month + "-" + year;
    return dateFormated;
  }

  getRapportDetail()
  {
      this.rapportService.getRapport(this.id).subscribe(response=>{
      this.dataRaport=response;
      this.dateFormatdebut = this.formatDate(this.dataRaport.DateDebut.split('T')[0]);
      this.dateFormatFin = this.formatDate(this.dataRaport.DateFin.split('T')[0]);
      this.clientSignature.fromDataURL(response[0].SignatureClient);
      this.agentSignature.fromDataURL(response[0].agentSignature)
    });
  }

  showPhoto()
 {
    //  this.photoviewer.show(this.rapportImage, "Image Rapport");
 }




}
