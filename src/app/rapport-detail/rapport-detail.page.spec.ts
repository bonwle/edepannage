import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RapportDetailPage } from './rapport-detail.page';

describe('RapportDetailPage', () => {
  let component: RapportDetailPage;
  let fixture: ComponentFixture<RapportDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RapportDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RapportDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
