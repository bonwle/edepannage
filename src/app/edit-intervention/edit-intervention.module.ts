import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditInterventionPageRoutingModule } from './edit-intervention-routing.module';

import { EditInterventionPage } from './edit-intervention.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SignaturePadModule } from 'angular2-signaturepad';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditInterventionPageRoutingModule,
    NgxDatatableModule,
    SignaturePadModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    OwlFormFieldModule, 
    OwlInputModule, 
    OwlSelectModule
  ],
  declarations: [EditInterventionPage]
})
export class EditInterventionPageModule {}
