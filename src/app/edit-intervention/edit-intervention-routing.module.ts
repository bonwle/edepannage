import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditInterventionPage } from './edit-intervention.page';

const routes: Routes = [
  {
    path: '',
    component: EditInterventionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditInterventionPageRoutingModule {}
