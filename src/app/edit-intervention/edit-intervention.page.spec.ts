import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditInterventionPage } from './edit-intervention.page';

describe('EditInterventionPage', () => {
  let component: EditInterventionPage;
  let fixture: ComponentFixture<EditInterventionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInterventionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditInterventionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
