import { Component, OnInit, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { Interventions } from '../models/interventions';
import { Diagnostique } from '../models/diagnostique';
import { Devis } from '../models/devis';
import { Pieces } from '../models/pieces';
import { Rapport } from '../models/rapport';
import { ActivatedRoute, Router } from '@angular/router';
import { InterventionService } from '../services/intervention.service';
import { ToastController, AlertController } from '@ionic/angular';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { DevisService } from '../services/devis.service';
import { PieceService } from '../services/piece.service';
import { EquipeService } from '../services/equipe.service';
import { ZoneService } from '../services/zone.service';
import { UtilisateursService } from '../services/utilisateurs.service';
import { RapportService } from '../services/rapport.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');

@Component({
  selector: 'app-edit-intervention',
  templateUrl: './edit-intervention.page.html',
  styleUrls: ['./edit-intervention.page.scss'],
})
export class EditInterventionPage implements OnInit {

  photoUrls: string[] = [];
  @ViewChild("clientSignature", {static:false}) clientSignature: SignaturePad;
  @ViewChild("agentSignature", {static:false}) agentSignature: SignaturePad;

 private Options:object=
 {
   'canvasHeight':80,
 }

  id: number;
  dataEditIntervention: Interventions;
  dataIntervention: Interventions;
  EditIntervention: string;
  dataDiagnostique: Diagnostique;
  dataInterventionDisplay: any;
  devisData: Devis;
  dataPiece : Pieces;
  pieceData: any;
  dataUser:any;
  dataEditZone:any;
  dataEditEquipe:any;
  dataDiagnosticDisplay: any;
  editOtherUser :any;
  dataDevis :any;
  rapportImage: string;
  rapportData: Rapport;
  dataRapport: any;
  dataDiagnostic :any;
  raportData: any;
  to :any;
  imageData : any;
  geoData :any;
  photoPDF :any;
  
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiService: InterventionService,
    private toastCtrl: ToastController,
    private DiagnosticService: DiagnostiqueService,
    private deviService: DevisService,
    private alertCtrl: AlertController,
    private pieceService: PieceService,
    private equipeService: EquipeService,
    private zoneService: ZoneService,
    private userService: UtilisateursService,
    private camera: Camera,
    private apiRapportService: RapportService,
    private photoviewer: PhotoViewer,
    private transfer: Transfer,
    private filePath: FilePath,
    private file: File,

  ) 
  { 
    this.dataEditIntervention=new Interventions();
    this.dataIntervention= new Interventions();
    this.dataDiagnostique = new Diagnostique();
    this.dataInterventionDisplay = []; 
    this.devisData=new Devis();
    this.dataPiece = new Pieces();
    this.rapportData=new Rapport();
    this.dataRapport=[];
    this.dataUser = [];
    this.dataEditZone =[];
    this.dataEditEquipe =[];
    this.dataDiagnosticDisplay =[];
    this.pieceData=[];
    this.editOtherUser=[];
    this.dataDevis = [];
    this.dataDiagnostic = [];
    this.raportData = [];
    this.to = 0;
    this.imageData =[];
    this.geoData = [];
    this.photoPDF =[];
    
  }
 
  ngOnInit() {

    this.EditIntervention ='EditDetail';
    this.id=this.activatedRoute.snapshot.params["id"];
    this.apiService.getIntervention(this.id).subscribe(response=>{
    console.log(response);
    this.dataEditIntervention=response;
    this.dataIntervention = response;
    this.dataInterventionDisplay = response;
    });

    this.getUserAgent();
    this.getUserEquipe();
    this.getZones();
    this.getinterventionDiagnostics();
    this.getListPieces();
    this.getDevisIntervention();
    this.getDiagnostics();
    this.getRapportIntervention();
    this.rapportImage='http://localhost:5000/app/images/'
    console.log(this.rapportImage);
  
  }

  updateIntervention()
  {
    console.log("Updated Intervention");
    console.log(this.dataEditIntervention);
    this.apiService.updateIntervention(this.id, this.dataEditIntervention).subscribe(response=>{
    this.router.navigate(['intervention-list']);
    this.displayToastEdit();
  });
  }

  getRapports()
  {
      this.apiRapportService.getAllRapport().subscribe(response=>{
      console.log(response);
      this.dataRapport=response;
    });
  }

  submitFormRapport()
  {
    this.rapportData.SignatureClient=this.clientSignature.toData();
    this.rapportData.SignatureIntervenant=this.agentSignature.toData();
    this.rapportData.Photo=this.rapportImage;
    this.rapportData.IdIntervention=this.dataEditIntervention._id;
    this.apiRapportService.createRapport(this.rapportData).subscribe((response)=>{
    console.log(response);
    this.displayToastEdit();
    this.updateIntervention();
    this.getRapportIntervention();
    });
  }

  updateDiagnostic()
  {
    this.dataDiagnostique.IdIntervention=this.dataEditIntervention._id;
    this.DiagnosticService.createDiagnostique(this.dataDiagnostique).subscribe((response)=>{
      console.log(response);
      this.displayToastEdit();
      this.getinterventionDiagnostics();
     
    });
  }

  updateDevis()
  // tslint:disable-next-line: one-line
  {
      this.devisData.IdIntervention=this.dataEditIntervention._id;
      this.deviService.createDevis(this.devisData).subscribe((response)=>{
      //this.displayToastEdit();
      console.log(response);
      this.dataPiece=new  Pieces();
      this.getListPieces();
      this.updateIntervention();
      this.displayToastEdit();
      this.getDevisIntervention();
      
    });
}

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"top"
      }
    );
    toast.present();
  }

//--------------------------------------------------------------

   getDevisIntervention()
  {
    this.deviService.getAllDevisByIntervention(this.id).subscribe(response=>{
      this.dataDevis= response;
    });
  }

  getRapportIntervention()
  {
    this.apiRapportService.getAllRapportIntervention(this.id).subscribe(response=>{
      this.raportData=response;
    });
  }


  async confirmDeletedDevis(item)
  {
    console.log(item);
    const alert= await this.alertCtrl.create(
      {
        header:"Suppression",
        message:"Confirmez-vous la suppression de cet élément?",
        animated:true,
        backdropDismiss:false,
        translucent:true,
        buttons:[
         {
           text:"Non",
           role:"cancel",
           cssClass: "secondary" 
         },
         {
           text: "Oui",
           handler:()=>{
             this.deviService.deleteDevis(item._id).subscribe(response=>{this.getDevisIntervention()});
           }
         }
           ] 
      }
    );
    await alert.present()
  }

getUserAgent()
{
  this.userService.getUtilisateurs().subscribe(response=>{
    console.log(response+"liste user ok ");
    this.dataUser=response;
    this.editOtherUser = response;
  });
}

getOtherUser(item1)
  {
    this.equipeService.getAgentNotInEquipe(item1).subscribe((response)=>{
    this.editOtherUser=response;
    console.log(this.editOtherUser+"autre agent");
    });
  }


  getAgentsEquipe(item)
  {
    
    this.equipeService.getEquipeAgent(item).subscribe(response=>{
      this.dataUser = response;
      this.getOtherUser(item);
      console.log(this.dataUser);
    });
  }

  
  getZoneEquipes(item)
  {
    this.zoneService.getZoneEquipe(item).subscribe(response=>{
    this.dataEditEquipe= response;
    
  });
}


getUserEquipe()
{
  this.equipeService.getEquipes().subscribe(response=>{
    console.log(response+"liste equipe ok ");
    this.dataEditEquipe=response;
  });
}


getZones()
{
  this.zoneService.getZones().subscribe(response=>{
    this.dataEditZone=response;
  })
}

getinterventionDiagnostics()
{
  this.DiagnosticService.getDiagnosticIntervention(this.id).subscribe(response=>{
    this.dataDiagnosticDisplay=response;
  });
}

getDiagnostics()
{
  this.DiagnosticService.getListDiagnostiques().subscribe(response=>{
    this.dataDiagnostic=response;
  });
}

delete(item)
{
  this.confirmDeleted(item);
}


onChangeMultiple(value){

}

 async confirmDeleted(item)
 {
   const alert= await this.alertCtrl.create(
     {
       header:"Suppression",
       message:"Confirmez-vous la suppression de cet élément?",
       animated:true,
       backdropDismiss:false,
       translucent:true,
       buttons:[
        {
          text:"Non",
          role:"cancel",
          cssClass: "secondary" 
        },
        {
          text: "Oui",
          handler:()=>{
            this.DiagnosticService.deleteDiagnostique(item._id).subscribe(response=>{this.getinterventionDiagnostics()});
          }

        }
          ] 
     }
   );
   await alert.present() 
 }

 getListPieces()
 {
   this.pieceService.getAllPieces().subscribe(response=> {
    console.log(response);
    this.pieceData=response;
    console.log(this.pieceData);

  });
 }

 getPieceDetail(item)
 {
  this.pieceService.getPiece(item).subscribe(response=>{
    this.dataPiece=response;
  });
 }

 getRapportPicture()
 {
   const options:CameraOptions ={
     quality:100,
     destinationType:this.camera.DestinationType.FILE_URI,
     encodingType:this.camera.EncodingType.JPEG,
     mediaType:this.camera.MediaType.PICTURE,
     saveToPhotoAlbum:false,
     correctOrientation:true,
     targetHeight:800,
     targetWidth:800
   };


   this.camera.getPicture(options).then((imageData)=>
   {
     var currentName=imageData.
     
     this.rapportImage='data:image/jpeg;base64,'+imageData;
     console.log("Picture");
     console.log(this.rapportImage); 
   })
 }

 onClearSignatureClient()
 {
   this.clientSignature.clear();
 }

 onClearSignatureAgent()
 {
   console.log("Signature agent clear...");
   this.agentSignature.clear();
 }

 showPhoto()
 {
  this.photoviewer.show(this.rapportImage, "Image Rapport");
  
 }


  formatDate(date1){
    var dt = [];
     dt = date1.split('-');
    var year = dt[0];
    var month = dt[1];
    var day = dt[2];
    var dateFormated = day + "/" + month + "/" + year;
    return dateFormated;
  }

 genereRapport(){

  var doc = new jsPDF('p', 'pt');
  doc.setFont("helvetica");
  doc.setFontType("bold");

  var img = new Image();
  img.src = ('../assets/logo.png');
 doc.addImage(img, 'PNG',30,17,80, 76);

  var columns = [
      { title: "Constat de l'agent ", dataKey: "DescriptionProbleme" }
  ];

 
 // tslint:disable-next-line: align
  var dataDiagnostiques = [];
  var devisDatas = [];
 // tslint:disable-next-line: align
 this.dataDiagnosticDisplay.forEach(element => { 
  var temp = [element.DescriptionProbleme, element.Photo];
  dataDiagnostiques.push(temp);
});

this.dataDevis.forEach(element => { 
  var temp = [element.IdPieces.designationPiece,element.Quantite,element.IdPieces.prixUnitairePiece,element.IdPieces.prixUnitairePiece*element.Quantite];
  devisDatas.push(temp);
  this.to += element.IdPieces.prixUnitairePiece*element.Quantite;
});

var total1 = this.to;
console.log(total1);

doc.text(30,28, 'E-DEPANNAGE');
doc.text(200,60, 'Rapport Diagnostic-Dévis');
doc.setFontSize(9);
doc.text(30,120,'type d\'intervention:');
doc.text(115,120,this.dataInterventionDisplay.typeIntervention);
doc.text(30,140, 'Date d\'intervention:');

var d3 = this.dataInterventionDisplay.DateDebutIntervention.toString().slice(0,10);

doc.text(115,140,this.formatDate(d3));
doc.text(30,160, 'Heure d\'arrivée:');
doc.text(115,160,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(12,16));
doc.text(30,180, 'Estimation durée d\'intervention:');
doc.text(30,200, ' Date rdv:');
doc.text(115,200,this.formatDate(d3));
doc.text(30,220, ' Heure rdv:');
doc.text(115,220,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(12,16));
doc.text(350,120, 'Ordre d\'intervention:');
doc.text(445,120,this.dataInterventionDisplay.OrdreIntervention);
doc.text(350,160, 'Adresse Chantier:');
doc.text(445,160,this.dataInterventionDisplay.LieuIntervention);
doc.text(350,180,'Zone:');
doc.text(445,180,this.dataInterventionDisplay.ZoneAffectee.NomZone);
doc.text(350,200,'Contact Client:');
doc.text(445,200,this.dataInterventionDisplay.Adresses);

// liste autres agents
doc.setFontSize(16);

var y=270;
var col = [
  { title: "Nom Equipe", dataKey: "NomEquipe" },
  { title: "Descrption Equipe", dataKey: "descriptionEquipe" }

];


// tslint:disable-next-line: align
var  d1 = [];
// tslint:disable-next-line: align
this.dataInterventionDisplay.EquipeAffectee.forEach(element => { 
var temp = [element.NomEquipe, element.descriptionEquipe];
d1.push(temp); 
});
doc.autoTable(col,d1, {
  
  startY:y+10,
   pageBreak: 'avoid',

  headStyles: {
      fillColor: [111, 111, 111],
      textColor: [0, 0, 0],
      halign: 'center'
  },

  bodyStyles: {
      halign: 'center'
  },
  margin: {top:50},
  theme: 'striped'
  });


doc.setFontSize(16);
var col = [
  { title: "Nom Agent", dataKey: "NomUser" }

];
var  d2 = [];
this.dataInterventionDisplay.AutreAgents.forEach(element => { 
var temp = [element.NomUser];
d2.push(temp); 
});

doc.autoTable(col,d2, {
  startY:y+10,
  pageBreak: 'avoid',

  headStyles: {
      fillColor: [111, 111, 111],
      textColor: [0, 0, 0],
      halign: 'center'
  },
  bodyStyles: {
      halign: 'center'
  },
  margin: {left:300,top:30},
  theme: 'striped',
  
  });

let start1 = doc.previousAutoTable.finalY +30;
doc.text(30,start1," 1- Diagnostic");

doc.autoTable(columns, dataDiagnostiques, {
      startY:start1 +20
      
});

let start2= doc.previousAutoTable.finalY +30;
doc.text(30,start2, '2 - Photo avant intervention');

doc.addPage();
console.log(this.dataDevis);
doc.text(30,40, '3 - Dévis');
// tslint:disable-next-line: align
 var columns4 = [
    { title: "Désignation pièce", dataKey: "designationPiece" },
    { title: "Quantité pieces", dataKey: "Quantite" },
    { title: "Prix Unitaire", dataKey: "prixUnitairePiece" },
    { title: "Montant", dataKey: "" }
];
 doc.autoTable(columns4, devisDatas, {
         startY: 60
});
let start3 = doc.previousAutoTable.finalY +160;
 //var mainDoeuvres = 2000;
 if(mainDoeuvres>0){
 var mainDoeuvres =  this.dataInterventionDisplay.mainDoeuvre ;
}else {
  mainDoeuvres = 0;

}
 var tva = 0.18;
 var totalDevis = total1;
 console.log(totalDevis+"devis");
 var totalHorsTax1=totalDevis+mainDoeuvres;
 var Mtva = totalHorsTax1*tva;
 var ttc = totalHorsTax1*(1+tva);
 //var totalHors = 

 doc.autoTable({
    columnStyles: {0: {halign: 'right', fillColor: [111,111,111]}}, // Cells in first column centered and green
    margin: {top:10,right:40},
    body: [['Main d\'oeuvre', '',mainDoeuvres],
           ['Montant HT', '', totalHorsTax1], 
           ['TVA 18%', '', Mtva],
           ['Montant TTC', '',ttc]]
});

doc.text(30,start3,"4 - Rapport");

let start4 =  start3+ 20
// tslint:disable-next-line: align


var columns5 = [
  { title: "Date de debut", dataKey: "DateDebut" },
  { title: "Date de fin", dataKey: "DateFin" },
  { title: "Description travail effectué ", dataKey: "DescriptionTravail" }

];

  var raporData = [];
 // tslint:disable-next-line: align
this.raportData.forEach(element => { 
  var temp = [this.formatDate(element.DateDebut.toString().slice(0,10)),this.formatDate(element.DateFin.toString().slice(0,10)),element.DescriptionTravail];
  //var temp1 = [element.id,element.name];
  raporData.push(temp);
  //rows1.push(temp1);
});
// console.log("1", data);
// console.log("2", columns);
// var d5 = this.dataInterventionDisplay.DateDebutIntervention.toString().slice(0,10);

// doc.text(115,140,this.formatDate(d3));

doc.autoTable(columns5, raporData,{
  startY : start4
});

doc.save('rappoert-final.pdf'); 
}




voirPDFdevis(){

  var doc = new jsPDF('p', 'pt');
  doc.setFont("helvetica");
  doc.setFontType("bold");

  var img = new Image();
  img.src = ('../assets/logo.png');
 doc.addImage(img, 'PNG',30,17,80, 76);

  var columns = [
      { title: "Constat de l'agent ", dataKey: "DescriptionProbleme" }
  ];

 // tslint:disable-next-line: align
  var dataDiagnostiques = [];
  var devisDatas = [];
 // tslint:disable-next-line: align
 this.dataDiagnosticDisplay.forEach(element => { 
  var temp = [element.DescriptionProbleme, element.Photo];
  dataDiagnostiques.push(temp);
});

this.dataDevis.forEach(element => { 
  var temp = [element.IdPieces.designationPiece,element.Quantite,element.IdPieces.prixUnitairePiece,element.IdPieces.prixUnitairePiece*element.Quantite];
  devisDatas.push(temp);
  this.to += element.IdPieces.prixUnitairePiece*element.Quantite;
});

var total1 = this.to;
console.log(total1);

doc.text(30,28, 'E-DEPANNAGE');
doc.text(200,60, 'Rapport Diagnostic-Dévis');
doc.setFontSize(9);
doc.text(30,120,'type d\'intervention:');
doc.text(115,120,this.dataInterventionDisplay.typeIntervention);
doc.text(30,140, 'Date d\'intervention:');

var d3 = this.dataInterventionDisplay.DateDebutIntervention.toString().slice(0,10);

doc.text(115,140,this.formatDate(d3));
doc.text(30,160, 'Heure d\'arrivée:');
doc.text(115,160,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(12,16));
doc.text(30,180, 'Estimation durée d\'intervention:');
doc.text(30,200, ' Date rdv:');
doc.text(115,200,this.formatDate(d3));
doc.text(30,220, ' Heure rdv:');
doc.text(115,220,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(12,16));
doc.text(350,120, 'Ordre d\'intervention:');
doc.text(445,120,this.dataInterventionDisplay.OrdreIntervention);
doc.text(350,160, 'Adresse Chantier:');
doc.text(445,160,this.dataInterventionDisplay.LieuIntervention);
doc.text(350,180,'Zone:');
doc.text(445,180,this.dataInterventionDisplay.ZoneAffectee.NomZone);
doc.text(350,200,'Contact Client:');
doc.text(445,200,this.dataInterventionDisplay.Adresses);

// liste autres agents
doc.setFontSize(16);

var y=270;
var col = [
  { title: "Nom Equipe", dataKey: "NomEquipe" },
  { title: "Descrption Equipe", dataKey: "descriptionEquipe" }

];


// tslint:disable-next-line: align
var  d1 = [];
// tslint:disable-next-line: align
this.dataInterventionDisplay.EquipeAffectee.forEach(element => { 
var temp = [element.NomEquipe, element.descriptionEquipe];
d1.push(temp); 
});
doc.autoTable(col,d1, {
  
  startY:y+10,
   pageBreak: 'avoid',

  headStyles: {
      fillColor: [111, 111, 111],
      textColor: [0, 0, 0],
      halign: 'center'
  },

  bodyStyles: {
      halign: 'center'
  },
  margin: {top:50},
  theme: 'striped'
  });


doc.setFontSize(16);
var col = [
  { title: "Nom Agent", dataKey: "NomUser" }
];
var  d2 = [];
this.dataInterventionDisplay.AutreAgents.forEach(element => { 
var temp = [element.NomUser];
d2.push(temp); 
});

doc.autoTable(col,d2, {
  startY:y+10,
  pageBreak: 'avoid',

  headStyles: {
      fillColor: [111, 111, 111],
      textColor: [0, 0, 0],
      halign: 'center'
  },
  bodyStyles: {
      halign: 'center'
  },
  margin: {left:300,top:30},
  theme: 'striped',
  
  });

let start1 = doc.previousAutoTable.finalY +30;
doc.text(30,start1," 1- Diagnostic");

doc.autoTable(columns, dataDiagnostiques, {
      startY:start1 +20
      
});

let start2= doc.previousAutoTable.finalY +30;
doc.text(30,start2, '2 - Photo avant intervention');

doc.addPage();
doc.text(30,40, '3 - Dévis');
// tslint:disable-next-line: align
 var columns4 = [
    { title: "Désignation pièce", dataKey: "designationPiece" },
    { title: "Quantité pieces", dataKey: "Quantite" },
    { title: "Prix Unitaire", dataKey: "prixUnitairePiece" },
    { title: "Montant", dataKey: "" }
];

 doc.autoTable(columns4, devisDatas, {
         startY:60
 
});

 if(mainDoeuvres>0){
 var mainDoeuvres =  this.dataInterventionDisplay.mainDoeuvre ;
}else {
  mainDoeuvres = 0;

}
 var tva = 0.18;
 var totalDevis = total1;
 console.log(totalDevis+"devis");
 var totalHorsTax1=totalDevis+mainDoeuvres;
 var Mtva = totalHorsTax1*tva;
 var ttc = totalHorsTax1*(1+tva);
 //var totalHors = 

 doc.autoTable({
    columnStyles: {0: {halign: 'right', fillColor: [111,111,111]}}, // Cells in first column centered and green
    margin: {top:10,right:40},
    body: [['Main d\'oeuvre', '',mainDoeuvres],
           ['Montant HT', '', totalHorsTax1], 
           ['TVA 18%', '', Mtva],
           ['Montant TTC', '',ttc]]
});



//test ici 
//doc.addPage();
// doc.autoTable(columns2, devisDatas, {

//   didParseCell: function (data1) {
    
//       if (data1.row.index === devisDatas.length - 1) {
//           console.log('last row');
//           var taille = data1.row.index;
//          // console.log(taille);
//           //TODO
//       } 
//   },

//   pageBreak: 'avoid',
//   headStyles: {
//       fillColor: [239, 154, 154],
//       textColor: [0, 0, 0],
//       halign: 'center'
//   },
//   bodyStyles: {
//       halign: 'center'
//   },
//   margin: {left:300,top:50},
//   theme: 'striped',
  
// });

var string = doc.output('datauristring');
var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
var x = window.open();
x.document.open();
x.document.write(embed);
x.document.close();


}


generateDevis(){

  var doc = new jsPDF('p', 'pt');
  doc.setFont("helvetica");
  doc.setFontType("bold");

  var img = new Image();
  img.src = ('../assets/logo.png');
 doc.addImage(img, 'PNG',30,17,80, 76);

  var columns = [
      { title: "Constat de l'agent ", dataKey: "DescriptionProbleme" }
  ];


 // tslint:disable-next-line: align
  var dataDiagnostiques = [];
  var devisDatas = [];
 // tslint:disable-next-line: align
 this.dataDiagnosticDisplay.forEach(element => { 
  var temp = [element.DescriptionProbleme, element.Photo];
  dataDiagnostiques.push(temp);
});

this.dataDevis.forEach(element => { 
  var temp = [element.IdPieces.designationPiece,element.Quantite,element.IdPieces.prixUnitairePiece,element.IdPieces.prixUnitairePiece*element.Quantite];
  this.to += element.IdPieces.prixUnitairePiece*element.Quantite;
});

var total1 = this.to;
console.log(total1);

doc.text(30,28, 'E-DEPANNAGE');
doc.text(200,60, 'Rapport Diagnostic-Dévis');
doc.setFontSize(9);
doc.text(30,120,'type d\'intervention:');
doc.text(115,120,this.dataInterventionDisplay.typeIntervention);
doc.text(30,140, 'Date d\'intervention:');

var d3 = this.dataInterventionDisplay.DateDebutIntervention.toString().slice(0,10);

doc.text(115,140,this.formatDate(d3));
doc.text(30,160, 'Heure d\'arrivée:');
doc.text(115,160,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(12,16));
doc.text(30,180, 'Estimation durée d\'intervention:');
doc.text(30,200, ' Date rdv:');
doc.text(115,200,this.formatDate(d3));
doc.text(30,220, ' Heure rdv:');
doc.text(115,220,this.dataInterventionDisplay.DateDebutIntervention.toString().slice(12,16));
doc.text(350,120, 'Ordre d\'intervention:');
doc.text(445,120,this.dataInterventionDisplay.OrdreIntervention);

// var x = this.dataInterventionDisplay.DateDebutIntervention.toString().slice(0,10);
// console.log(this.formatDate(x));

//ici la liste des equipes 
// doc.text(350,140, 'liste de/des équipes:');
// doc.text(445,140,this.dataInterventionDisplay.EquipeAffectee[0].NomEquipe);
//doc.text(350,160, 'Liste Agents de l\'équipe:');
doc.text(350,160, 'Adresse Chantier:');
doc.text(445,160,this.dataInterventionDisplay.LieuIntervention);
doc.text(350,180,'Zone:');
doc.text(445,180,this.dataInterventionDisplay.ZoneAffectee.NomZone);
doc.text(350,200,'Contact Client:');
doc.text(445,200,this.dataInterventionDisplay.Adresses);

// liste autres agents
doc.setFontSize(16);

var y=270;
//doc.text(200,y,"Liste Equipes");
var col = [
  { title: "Nom Equipe", dataKey: "NomEquipe" },
  { title: "Descrption Equipe", dataKey: "descriptionEquipe" }

];


// tslint:disable-next-line: align
var  d1 = [];
// tslint:disable-next-line: align
this.dataInterventionDisplay.EquipeAffectee.forEach(element => { 
var temp = [element.NomEquipe, element.descriptionEquipe];
//var temp1 = [element.id,element.name];
d1.push(temp); 
});
doc.autoTable(col,d1, {
  
  startY:y+10,
   pageBreak: 'avoid',

  headStyles: {
      fillColor: [111, 111, 111],
      textColor: [0, 0, 0],
      halign: 'center'
  },

  bodyStyles: {
      halign: 'center'
  },
  margin: {top:50},
  theme: 'striped'
  });


doc.setFontSize(16);
//doc.text(200,500,"Liste des Agents");
var col = [
  { title: "Nom Agent", dataKey: "NomUser" }


];
var  d2 = [];
this.dataInterventionDisplay.AutreAgents.forEach(element => { 
var temp = [element.NomUser];
//var temp1 = [element.id,element.name];
d2.push(temp); 
});

doc.autoTable(col,d2, {
  startY:y+10,
  pageBreak: 'avoid',

  headStyles: {
      fillColor: [111, 111, 111],
      textColor: [0, 0, 0],
      halign: 'center'
  },
  bodyStyles: {
      halign: 'center'
  },
  margin: {left:300,top:30},
  theme: 'striped',
  
  });

let start1 = doc.previousAutoTable.finalY +30;
doc.text(30,start1," 1- Diagnostic");

doc.autoTable(columns, dataDiagnostiques, {
      startY:start1 +20
      
});

let start2= doc.previousAutoTable.finalY +30;
doc.text(30,start2, '2 - Photo avant intervention');

doc.addPage();
console.log(this.dataDevis);
//doc.text(20, 20, 'Do you like that?');
doc.text(30,40, '3 - Dévis');
// tslint:disable-next-line: align

 var columns4 = [
    { title: "Désignation pièce", dataKey: "designationPiece" },
    { title: "Quantité pieces", dataKey: "Quantite" },
    { title: "Prix Unitaire", dataKey: "prixUnitairePiece" },
    { title: "Montant", dataKey: "" }
];

 doc.autoTable(columns4, devisDatas, {
         startY:60
 
});

var columntotal = [
  { title: "Main d\'oeuvre", dataKey: "" },
  { title: "Total Htaxe", dataKey: "" },
  { title: "tva", dataKey: "" },
  { title: "Tttc", dataKey: "" }
];

 //var mainDoeuvres = 2000;
 if(mainDoeuvres>0){
 var mainDoeuvres =  this.dataIntervention.mainDoeuvre ;
}else {
  mainDoeuvres = 0;

}
 var tva = 0.18;
 var totalDevis = total1;
 console.log(totalDevis+"devis");
 var totalHorsTax1=totalDevis+mainDoeuvres;
 var Mtva = totalHorsTax1*tva;
 var ttc = totalHorsTax1*(1+tva);
 //var totalHors = 

 doc.autoTable({
    columnStyles: {0: {halign: 'right', fillColor: [111,111,111]}}, // Cells in first column centered and green
    margin: {top:10,right:40},
    body: [['Main d\'oeuvre', '',mainDoeuvres],
           ['Montant HT', '', totalHorsTax1], 
           ['TVA 18%', '', Mtva],
           ['Montant TTC', '',ttc]]
});
doc.save('rapport-diagnostic.pdf');

}

}

