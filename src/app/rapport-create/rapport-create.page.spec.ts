import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RapportCreatePage } from './rapport-create.page';

describe('RapportCreatePage', () => {
  let component: RapportCreatePage;
  let fixture: ComponentFixture<RapportCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RapportCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RapportCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
