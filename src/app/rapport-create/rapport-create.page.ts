import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RapportService } from '../services/rapport.service';
import { Rapport } from '../models/rapport';
import { Router } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import{Camera, CameraOptions} from '@ionic-native/camera/ngx'
import {PhotoViewer} from '@ionic-native/photo-viewer/ngx'

@Component({
  selector: 'app-rapport-create',
  templateUrl: './rapport-create.page.html',
  styleUrls: ['./rapport-create.page.scss'],
})
export class RapportCreatePage implements OnInit {

  @ViewChild("signatureClient", {static:false}) signatureClient: SignaturePad;
  @ViewChild("signatureAgent", {static:false}) signatureAgent: SignaturePad;

  dataRapport: Rapport;
  imageFinIntervention: string;
  imageRapport:string;
  nbrePhotos: number=1;
  newPhotos:any;

 private signaturepadOptions:object=
 {
   'canvasWidth':150,
   'canvasHeight':100,
   'canvasBackgroundColor':"rgba(0,0,0,1)"

 }

  constructor(
    public apiService: RapportService,
    public router: Router,
    private camera: Camera,
    private photoviewer: PhotoViewer,

  ) {
    this.dataRapport=new Rapport();
    this.imageRapport="";
    this.newPhotos=[];
   }

  ngOnInit() 
  {
    if(this.newPhotos['length']==0) this.newPhotos.push(1);
    // this.getRapportIntervention();
  }

  submitForm()
  {
    this.dataRapport.Photo=this.imageRapport;
    this.dataRapport.IdIntervention="5e097a9c74ff731ce4d1dad1";
    this.apiService.createRapport(this.dataRapport).subscribe((response)=>{
    });
  }

  getPicture()
  {
    const options: CameraOptions={
      quality:100,
      destinationType:this.camera.DestinationType.DATA_URL,
      encodingType:this.camera.EncodingType.JPEG,
      mediaType:this.camera.MediaType.ALLMEDIA,
      correctOrientation:true,
      saveToPhotoAlbum:false,
      targetHeight:300,
      targetWidth:300
    };

    
    this.camera.getPicture(options).then((imageData)=>
    {
     
       this.imageRapport=imageData;
      this.imageFinIntervention='data:image/jpeg;base64,'+imageData;
      this.nbrePhotos++;
      this.newPhotos.push(this.nbrePhotos);
      // this.imageRapport.push(imageData);
    })
    .catch((err)=>{
      console.log("Img loading error: "+err);
    });
  }

  showPhoto()
 {
  this.photoviewer.show(this.imageFinIntervention, "Image Rapport");
 }

  onClearSignatureClient()
  {
  }

  onClearSignatureAgent()
  {
    this.imageFinIntervention=this.signatureAgent.toDataURL();
    this.signatureClient.fromDataURL(this.signatureAgent.toDataURL());
    // this.signatureAgent.clear();
  }

  getRapportIntervention()
  {
    this.apiService.getAllRapportIntervention('5dc43645030b11301c3ad40c').subscribe(response=>{
      this.imageRapport=response[0].Photo;
      this.imageFinIntervention='data:image/jpeg;base64,'+this.imageRapport;
    });
  }

}
