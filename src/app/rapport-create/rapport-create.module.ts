import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RapportCreatePageRoutingModule } from './rapport-create-routing.module';

import { RapportCreatePage } from './rapport-create.page';
import { SignaturePadModule } from 'angular2-signaturepad';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    SignaturePadModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule, OwlInputModule,
    RapportCreatePageRoutingModule
  ],
  declarations: [RapportCreatePage]
})
export class RapportCreatePageModule {}
