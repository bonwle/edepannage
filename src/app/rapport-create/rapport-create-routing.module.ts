import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RapportCreatePage } from './rapport-create.page';

const routes: Routes = [
  {
    path: '',
    component: RapportCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RapportCreatePageRoutingModule {}
