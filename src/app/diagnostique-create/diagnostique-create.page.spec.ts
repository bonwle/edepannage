import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiagnostiqueCreatePage } from './diagnostique-create.page';

describe('DiagnostiqueCreatePage', () => {
  let component: DiagnostiqueCreatePage;
  let fixture: ComponentFixture<DiagnostiqueCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnostiqueCreatePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnostiqueCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
