import { Component, OnInit} from '@angular/core';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { Router } from '@angular/router';
import { Diagnostique } from '../models/diagnostique';
import { ModalController } from '@ionic/angular';
import { InterventionService } from '../services/intervention.service';
@Component({
  selector: 'app-diagnostique-create',
  templateUrl: './diagnostique-create.page.html',
  styleUrls: ['./diagnostique-create.page.scss'],

})

export class DiagnostiqueCreatePage implements OnInit {

  dataDiagnostique: Diagnostique;
  interventionData: any;

  constructor(
    public serviceDiagnostic: DiagnostiqueService,
    public route: Router,
    public closeModel: ModalController,
    private apiIntervention: InterventionService
  ) { 
    
    this.dataDiagnostique = new Diagnostique();
    this.interventionData=[];
  }

  ngOnInit()
  {
    this.getListInterventions();
  }

  submitForm()
  {
    this.serviceDiagnostic.createDiagnostique(this.dataDiagnostique).subscribe((response)=>{
    this.closeModal();
      
     });
  }

getListInterventions()
  {
    this.apiIntervention.getInterventions().subscribe(response=>{
      console.log(response);
      this.interventionData=response;
    });
  }

 closeModal()
 {
    this.closeModel.dismiss();
    this.route.navigate(['diagnostiqueList']);
 }

}
