import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiagnostiqueCreatePage } from './diagnostique-create.page';

const routes: Routes = [
  {
    path: '',
    component: DiagnostiqueCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiagnostqueCreatePageRoutingModule {}
