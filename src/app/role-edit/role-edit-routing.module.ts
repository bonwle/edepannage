import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleEditPage } from './role-edit.page';

const routes: Routes = [
  {
    path: '',
    component: RoleEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleEditPageRoutingModule {}
