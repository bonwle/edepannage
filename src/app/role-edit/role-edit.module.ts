import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleEditPageRoutingModule } from './role-edit-routing.module';

import { RoleEditPage } from './role-edit.page';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule, OwlInputModule,
    RoleEditPageRoutingModule
  ],
  declarations: [RoleEditPage]
})
export class RoleEditPageModule {}
