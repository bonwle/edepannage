import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoleEditPage } from './role-edit.page';

describe('RoleEditPage', () => {
  let component: RoleEditPage;
  let fixture: ComponentFixture<RoleEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoleEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
