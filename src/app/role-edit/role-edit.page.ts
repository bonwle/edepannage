import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, ToastController } from '@ionic/angular';
import { Role } from '../models/role';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.page.html',
  styleUrls: ['./role-edit.page.scss'],
})
export class RoleEditPage implements OnInit {

  id: number;
  dataRole: Role;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiRoleService: RoleService,
    private toastCtrl: ToastController,
  ) { 
    this.dataRole=new Role();
  }

  ngOnInit() {

    this.id=this.activatedRoute.snapshot.params["id"];
    this.apiRoleService.getRole(this.id).subscribe(response=>{
    this.dataRole=response;
    });
  }

  update()
  {
    this.apiRoleService.updateRole(this.id, this.dataRole).subscribe(response=>{
    this.displayToastEdit();
    this.router.navigate(['administrateur']);

  })
  }

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"middle",
      }
    );
    toast.present();
  }
}
