import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoneCreatePageRoutingModule } from './zone-create-routing.module';

import { ZoneCreatePage } from './zone-create.page';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule, 
    OwlInputModule,
    OwlSelectModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ZoneCreatePageRoutingModule
  ],
  declarations: [ZoneCreatePage]
})
export class ZoneCreatePageModule {}
