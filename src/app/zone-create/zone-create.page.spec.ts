import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ZoneCreatePage } from './zone-create.page';

describe('ZoneCreatePage', () => {
  let component: ZoneCreatePage;
  let fixture: ComponentFixture<ZoneCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZoneCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ZoneCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
