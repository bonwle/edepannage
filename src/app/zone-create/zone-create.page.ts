import { Component, OnInit } from '@angular/core';
import { Zones } from '../models/zones';
import { ZoneService } from '../services/zone.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { EquipeService } from '../services/equipe.service';
import { UtilisateursService } from '../services/utilisateurs.service';
import { Utilisateurs } from '../models/utilisateurs';

@Component({
  selector: 'app-zone-create',
  templateUrl: './zone-create.page.html',
  styleUrls: ['./zone-create.page.scss'],
})
export class ZoneCreatePage implements OnInit {

  dataZones: Zones;
  equipeData: any;
  usersData: any;
  // membresEquipe: Utilisateurs;
  
  constructor(
    private apiService: ZoneService,
    private router: Router,
    private apiServiceEquipe: EquipeService,
    private apiUserService: UtilisateursService
  ) {
    this.dataZones=new Zones();
    this.usersData=[];
    this.equipeData=[];
    // this.membresEquipe=new Utilisateurs();

   }

  ngOnInit() {
    this.getAllEquipes();
    this.getUsers();
  }

  submitForm()
  {
    this.apiService.createZone(this.dataZones).subscribe((response)=>{
      console.log(response);
      this.closeModal();
    })
  }

 closeModal()
  {
    this.router.navigate(['administrateur']);
    console.log("Redirect list ok");
  }

  getAllEquipes()
  {
    this.apiServiceEquipe.getEquipes().subscribe(response=>{
      console.log(response);
      this.equipeData=response;
    })
  }

  getUsers()
  {
    this.apiUserService.getUtilisateurs().subscribe(response=>{
      console.log(response);
      this.usersData=response;
      // this.membresEquipe=response;
    })
  }

  searchMembres(items)
  {
    if(items)
    {
      console.log(items);
      console.log("Ok item")
      this.apiServiceEquipe.getAgentNotInEquipe(items).subscribe((response)=>{
      console.log(response);
      this.usersData=response;
    });
  }
  }
}
