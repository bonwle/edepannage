import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZoneCreatePage } from './zone-create.page';

const routes: Routes = [
  {
    path: '',
    component: ZoneCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoneCreatePageRoutingModule {}
