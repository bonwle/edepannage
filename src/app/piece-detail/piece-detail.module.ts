import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PieceDetailPageRoutingModule } from './piece-detail-routing.module';

import { PieceDetailPage } from './piece-detail.page';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PieceDetailPageRoutingModule,
    OwlFormFieldModule ,
    OwlInputModule,
  ],
  declarations: [PieceDetailPage]
})
export class PieceDetailPageModule {}
