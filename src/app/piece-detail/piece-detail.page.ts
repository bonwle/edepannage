import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { PieceService } from '../services/piece.service';

@Component({
  selector: 'app-piece-detail',
  templateUrl: './piece-detail.page.html',
  styleUrls: ['./piece-detail.page.scss'],
})
export class PieceDetailPage implements OnInit {
  id: number;
  dataDetailPiece: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiPieceService: PieceService
  ) 
  {
    this.dataDetailPiece=[];
   }

  ngOnInit() 
  {
    this.id=this.activatedRoute.snapshot.params["id"];
    this.apiPieceService.getPiece(this.id).subscribe(response=>{
    this.dataDetailPiece=response;
  })
}

}
