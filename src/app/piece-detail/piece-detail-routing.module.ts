import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PieceDetailPage } from './piece-detail.page';

const routes: Routes = [
  {
    path: '',
    component: PieceDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PieceDetailPageRoutingModule {}
