import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PieceCreatePageRoutingModule } from './piece-create-routing.module';

import { PieceCreatePage } from './piece-create.page';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PieceCreatePageRoutingModule,
    OwlFormFieldModule, OwlInputModule
  ],
  declarations: [PieceCreatePage]
})
export class PieceCreatePageModule {}
