import { Component, OnInit } from '@angular/core';
import { Pieces } from '../models/pieces';
import { PieceService } from '../services/piece.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-piece-create',
  templateUrl: './piece-create.page.html',
  styleUrls: ['./piece-create.page.scss'],
})
export class PieceCreatePage implements OnInit {

  dataPieces: Pieces
  
  constructor(
    public apiService: PieceService,
    public router: Router,
    public ctlrModel: ModalController
  ) {this.dataPieces=new Pieces(); }


  ngOnInit() {
  }

  submitForm()
  {
    if(!this.dataPieces.referencePiece)
    return  console.log("Invalid");
    else
    {
      this.apiService.createPiece(this.dataPieces).subscribe((response)=>{
      this.dataPieces=new Pieces;
      this.router.navigate(['piece-list']);
      });
    }
  }
  

 closeModal()
 {
    this.router.navigate(['piece-list']);
    this.ctlrModel.dismiss();
 }

}
