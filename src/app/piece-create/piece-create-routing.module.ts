import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PieceCreatePage } from './piece-create.page';

const routes: Routes = [
  {
    path: '',
    component: PieceCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PieceCreatePageRoutingModule {}
