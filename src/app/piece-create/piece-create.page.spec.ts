import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PieceCreatePage } from './piece-create.page';

describe('PieceCreatePage', () => {
  let component: PieceCreatePage;
  let fixture: ComponentFixture<PieceCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PieceCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PieceCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
