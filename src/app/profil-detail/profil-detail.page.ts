import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RoleService } from '../services/role.service';
import { ProfilService } from '../services/profil.service';
import { Role } from '../models/role';

@Component({
  selector: 'app-profil-detail',
  templateUrl: './profil-detail.page.html',
  styleUrls: ['./profil-detail.page.scss'],
})
export class ProfilDetailPage implements OnInit {

  id: number;
  dataDetailProfil: any;
  dataRole: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private roleApi: RoleService,
    private profilApi: ProfilService
  ) 
  { 
    this.dataDetailProfil=[];
    this.dataRole=[];
  }

  ngOnInit() 
  {
    this.id=this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.profilApi.getProfil(this.id).subscribe(response=>{
      this.dataDetailProfil=response;
      this.getRoles();
  })
}

getRoles()
  {
    this.roleApi.getListRole().subscribe(response=>{
      this.dataRole=response;
    })
  }

}
