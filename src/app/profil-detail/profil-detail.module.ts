import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilDetailPageRoutingModule } from './profil-detail-routing.module';

import { ProfilDetailPage } from './profil-detail.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilDetailPageRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
  ],
  declarations: [ProfilDetailPage]
})
export class ProfilDetailPageModule {}
