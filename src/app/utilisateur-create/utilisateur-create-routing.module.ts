import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UtilisateurCreatePage } from './utilisateur-create.page';

const routes: Routes = [
  {
    path: '',
    component: UtilisateurCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UtilisateurCreatePageRoutingModule {}
