import { Component, OnInit } from '@angular/core';
import { Utilisateurs } from '../models/utilisateurs';
import { UtilisateursService } from '../services/utilisateurs.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ProfilService } from '../services/profil.service';

@Component({
  selector: 'app-utilisateur-create',
  templateUrl: './utilisateur-create.page.html',
  styleUrls: ['./utilisateur-create.page.scss'],
})
export class UtilisateurCreatePage implements OnInit {

  dataUtilisateurs: Utilisateurs;
  profilData: any;

    constructor(
      public apiService: UtilisateursService,
      public router: Router,
      public closeModel: ModalController,
      private apiServiceProfil: ProfilService,
      ) 
      { 
        this.dataUtilisateurs= new Utilisateurs();
        this.profilData=[];
      }
      
      ngOnInit() {
        this.getProfils();
      }

      submitForm()
      {
        this.apiService.createUtilisateur(this.dataUtilisateurs).subscribe((response)=>{
          console.log(response);
          this.closeModal();
          this.dataUtilisateurs=new Utilisateurs();
        });
      }

 closeModal()
 {
    this.router.navigate(['administrateur']);
    console.log("Redirect list ok");
    //this.closeModel.dismiss();
 }

 getProfils()
  {
    this.apiServiceProfil.getAllProfil().subscribe(response=>{
      console.log("Profil");
      this.profilData=response;
    });
  }

}
