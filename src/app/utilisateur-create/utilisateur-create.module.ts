import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UtilisateurCreatePageRoutingModule } from './utilisateur-create-routing.module';

import { UtilisateurCreatePage } from './utilisateur-create.page';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    UtilisateurCreatePageRoutingModule
  ],
  declarations: [UtilisateurCreatePage]
})
export class UtilisateurCreatePageModule {}
