export class Pieces 
{
    designationPiece : string;
    referencePiece: string;
    prixUnitairePiece: number;
    quantitePiece: number;
    descriptionPiece: string;
}
