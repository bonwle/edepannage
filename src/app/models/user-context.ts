import { Utilisateurs } from './utilisateurs';
import { ProfilService } from '../services/profil.service';

export class UserContext {
    public static userConnect: Utilisateurs;
    public static userRolesList: any[];
    public static userConnectId: string;
    private static apiServiceProfil: ProfilService;

    public static logInUser(user:Utilisateurs)
    {
        this.userConnect=user;
        this.userConnectId=user._id;
    }



    public static logOutUser()
    {
        this.userConnect=null;
        this.userConnectId=null;
    }
}
