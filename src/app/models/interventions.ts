export class Interventions {
        OrdreIntervention: string;
       
        DateDebutIntervention :Date;
        DateFinIntervention :Date;
        LieuIntervention: string;
        FraisDeplacement: number;
        ObjetIntervention:string;
        DescriptionIntervention:string;
        typeIntervention: string;
        mainDoeuvre: number;
        statut : string;
        Client: string;
        Adresses : string;
        AgentAffectes:any=[];
        EquipeAffectee: any=[];
        AutreAgents : any=[];
        ZoneAffectee : any;
        Gestionnaire:string;
        _id:any;
}
