export class ModelDashboard {
    labels:Array<any>;
    values:Array<any>;
    labelDay:Array<any>;
    labelWeek:Array<any>;
    labelMonth:Array<any>;
    labelYear:Array<any>;
    donneeDay:Array<any>;
    donneeWeek:Array<any>;
    donneeMonth:Array<any>;
    donneeYear:Array<any>;
}
