export class Pdf {
    logo: any;
    nomFacture: string;
    dateFacture: Date;
    numeroFacture: string;
    prixHorTaxe : number;
    tva: number;
    prixTotal :number;

    
}

export class Diagnostique {
    DescriptionProbleme: String;
    DesignationPieces: any;
    Quantite: Number;
    PhotoConstat :string;
    IdIntervention: string;
    ordreIntervention: string;
}

export class Devis 
{
    Quantite : number;
    IdIntervention:string;
    IdPieces : string;
}

export class Pieces 
{
    designationPiece : string;
    referencePiece: string;
    prixUnitairePiece: number;
    quantitePiece: number;
    descriptionPiece: string;
}

export class Rapport 
{
    Photo : string;
    DescriptionTravail : string;
    DateDebut : Date;
    DateFin: Date;
    SignatureClient : Object;
    SignatureIntervenant : Object;
    IdIntervention:string;
}



