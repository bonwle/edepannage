import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZoneListPage } from './zone-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

const routes: Routes = [
  {
    path: '',
    component: ZoneListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class ZoneListPageRoutingModule {}
