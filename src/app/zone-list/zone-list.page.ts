import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { EquipeDetailPage } from '../equipe-detail/equipe-detail.page';
import { ZoneService } from '../services/zone.service';
import { ZoneCreatePage } from '../zone-create/zone-create.page';
import { ZoneEditPage } from '../zone-edit/zone-edit.page';
import { ZoneDetailPage } from '../zone-detail/zone-detail.page';

@Component({
  selector: 'app-zone-list',
  templateUrl: './zone-list.page.html',
  styleUrls: ['./zone-list.page.scss'],
})
export class ZoneListPage implements OnInit {
 
  zonesData: any;

  constructor(
    public apiService: ZoneService,
    private modalCntrl: ModalController,
    private alertCtlr: AlertController
  ) 
  { this.zonesData=[];}

  ngOnInit() {
    this.getAllZones();
  }

  ionViewWillEnter()
  {
    this.getAllZones();
  }

  getAllZones()
  {
    this.apiService.getZones().subscribe(response=>{
      console.log(response);
      this.zonesData=response;
    })
  }

  delete(item)
  {
    this.confirmDeleted(item);
  }

  async confirmDeleted(item)
   {
     const alert= await this.alertCtlr.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deleteZone(item._id).subscribe(response=>{this.getAllZones()})
            }
          }
            ] 
           
       }
     );
     await alert.present()
   }


  async openDetailModal(data) {
    const modal = await this.modalCntrl.create({
     component: ZoneDetailPage,
     animated:true,
     componentProps:{ZoneDetailData:data}
    });

    return await modal.present();
   }

   async openEditModal(data) {
    const modal = await this.modalCntrl.create({
     component: ZoneEditPage,
     animated:true,
     backdropDismiss:false,
     componentProps:{editZoneData:data}
    });

    return await modal.present();
   }

   async openAddModal() {
    const modal = await this.modalCntrl.create({
     component: ZoneCreatePage,
     animated:true,
     backdropDismiss:false,
    });

    return await modal.present();
   }
}
