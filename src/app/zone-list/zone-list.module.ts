import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoneListPageRoutingModule } from './zone-list-routing.module';

import { ZoneListPage } from './zone-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FlxUiDatatableModule } from 'flx-ui-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxDatatableModule,
    ZoneListPageRoutingModule,
    FlxUiDatatableModule
  ],
  declarations: [ZoneListPage]
})
export class ZoneListPageModule {}
