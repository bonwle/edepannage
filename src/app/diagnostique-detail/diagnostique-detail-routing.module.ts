import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiagnostiqueDetailPage } from './diagnostique-detail.page';

const routes: Routes = [
  {
    path: '',
    component: DiagnostiqueDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiagnostqueDetailPageRoutingModule {}
