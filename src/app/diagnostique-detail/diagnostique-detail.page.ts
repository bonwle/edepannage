import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Diagnostique } from '../models/diagnostique';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { ActivatedRoute } from '@angular/router';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
@Component({
  selector: 'app-diagnostique-detail',
  templateUrl: './diagnostique-detail.page.html',
  styleUrls: ['./diagnostique-detail.page.scss'],
})
export class DiagnostiqueDetailPage implements OnInit {

  dataDiagnostique: Diagnostique;
  DiagnosticsIntervention:any;
  id: number;
  rapportImage: string;
  demarrage:any;
 

  constructor( 
    private modalCtrl: ModalController,
    public diagnosticservice: DiagnostiqueService,
    public activatedRoute: ActivatedRoute,
    private photoviewer: PhotoViewer
    ) { 
      this.DiagnosticsIntervention =[];
      this.demarrage =[];
     
    }

  ngOnInit() {

    this.id=this.activatedRoute.snapshot.params["id"];
    this.getDiagnostic();   
  }


  formatDate(date1){
    var dt = [];
     dt = date1.split('-');
    var year = dt[0];
    var month = dt[1];
    var day = dt[2];
    var dateFormated = day + "/" + month + "/" + year;
    return dateFormated;
  }
  
  getDiagnostic()
  {
    this.diagnosticservice.getDiagnostique(this.id).subscribe(response=>{
      this.DiagnosticsIntervention=response;
      this.demarrage = this.formatDate(this.DiagnosticsIntervention.dateDemarrage.split('T')[0]);
      console.log(this.demarrage);
    });
  }


  showPhoto()
 {
  this.photoviewer.show(this.rapportImage, "Image Rapport");
 }
 
}
