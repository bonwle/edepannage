import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DiagnostiqueDetailPage } from './diagnostique-detail.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';


const routes: Routes = [
  {
    path: '',
    component: DiagnostiqueDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,OwlDateTimeModule, OwlNativeDateTimeModule, OwlFormFieldModule,
    OwlInputModule, OwlSelectModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DiagnostiqueDetailPage]
})
export class DiagnostiqueDetailPageModule {}
