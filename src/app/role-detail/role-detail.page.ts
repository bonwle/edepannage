import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { RoleService } from '../services/role.service';
@Component({
  selector: 'app-role-detail',
  templateUrl: './role-detail.page.html',
  styleUrls: ['./role-detail.page.scss'],
})
export class RoleDetailPage implements OnInit {
  id: number;
  dataDetailRole: any;


  constructor
  (
    private activatedRoute: ActivatedRoute,
    private apiRoleService: RoleService
    ) 
    { 
      this.dataDetailRole=[];
    }

  ngOnInit() 
  {
    this.id=this.activatedRoute.snapshot.params["id"];
      this.apiRoleService.getRole(this.id).subscribe(response=>{
      this.dataDetailRole=response;
    })
  }
}
