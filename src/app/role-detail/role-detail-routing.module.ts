import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoleDetailPage } from './role-detail.page';

const routes: Routes = [
  {
    path: '',
    component: RoleDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleDetailPageRoutingModule {}
