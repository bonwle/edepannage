import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleDetailPageRoutingModule } from './role-detail-routing.module';

import { RoleDetailPage } from './role-detail.page';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule, OwlInputModule,
    RoleDetailPageRoutingModule
  ],
  declarations: [RoleDetailPage]
})
export class RoleDetailPageModule {}
