import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EquipeCreatePageRoutingModule } from './equipe-create-routing.module';

import { EquipeCreatePage } from './equipe-create.page';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';
import { OwlNativeDateTimeModule, OwlDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    IonicModule,
    EquipeCreatePageRoutingModule
  ],
  declarations: [ EquipeCreatePage]
})
export class EquipeCreatePageModule {}
