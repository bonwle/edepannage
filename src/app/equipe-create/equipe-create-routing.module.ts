import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EquipeCreatePage } from './equipe-create.page';

const routes: Routes = [
  {
     path: '',
     component: EquipeCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquipeCreatePageRoutingModule {}
