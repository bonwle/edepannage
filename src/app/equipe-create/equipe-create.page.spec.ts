import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EquipeCreatePage } from './equipe-create.page';

describe('EquipeCreatePage', () => {
  let component: EquipeCreatePage;
  let fixture: ComponentFixture<EquipeCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipeCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EquipeCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
