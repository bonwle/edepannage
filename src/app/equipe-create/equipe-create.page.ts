import { Component, OnInit } from '@angular/core';
import { EquipeService } from '../services/equipe.service';
import { Router } from '@angular/router';
import { Equipes } from '../models/equipes';
import { Utilisateurs } from '../models/utilisateurs';
import { UtilisateursService } from '../services/utilisateurs.service';

@Component ({
  selector: 'app-equipe-create',
  templateUrl: './equipe-create.page.html',
  styleUrls: ['./equipe-create.page.scss'],

})

export class EquipeCreatePage implements OnInit {

  dataEquipe: Equipes;
  utilisateurs: any;

  constructor(
    private serviceEquipe: EquipeService,
    private route: Router,
    private apiServiceUser: UtilisateursService,
  ) 
  { 
    
    this.dataEquipe= new Equipes();
    this.utilisateurs=[];
  }

  ngOnInit() {
    this.getUsers();
  }

  submitForm()
  {
    console.log(this.dataEquipe);
    this.serviceEquipe.createEquipe(this.dataEquipe).subscribe((response)=>{
      console.log(response);
      this.route.navigate(['administrateur']);
    });
  }
  
  getUsers()
  {
    this.apiServiceUser.getUtilisateurs().subscribe(response=>{
      console.log(response);
      this.utilisateurs=response;
    })
  }
}
