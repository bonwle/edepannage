import { Component, OnInit } from '@angular/core';
import { Pieces } from '../models/pieces';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, ToastController, NavParams } from '@ionic/angular';
import { PieceService } from '../services/piece.service';

@Component({
  selector: 'app-piece-edit',
  templateUrl: './piece-edit.page.html',
  styleUrls: ['./piece-edit.page.scss'],
})
export class PieceEditPage implements OnInit {
  id: number;
  dataPieceToEdit: Pieces;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiPieceService: PieceService,
    private toastCtrl: ToastController,
  ) 
  { 
    this.dataPieceToEdit=new Pieces();
  }

  ngOnInit() {

    this.id=this.activatedRoute.snapshot.params["id"];
    this.apiPieceService.getPiece(this.id).subscribe(response=>{
    this.dataPieceToEdit=response;
    })
  }


  updatePieceData()
  {
    this.apiPieceService.updatePiece(this.id, this.dataPieceToEdit).subscribe(response=>{
    this.displayToastEdit();
    this.router.navigate(['piece-list']);
  })
  }

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"middle",
      }
    );
    toast.present();
  }

}
