import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PieceEditPageRoutingModule } from './piece-edit-routing.module';

import { PieceEditPage } from './piece-edit.page';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PieceEditPageRoutingModule,
    OwlFormFieldModule ,
    OwlInputModule,
  ],
  declarations: [PieceEditPage]
})
export class PieceEditPageModule {}
