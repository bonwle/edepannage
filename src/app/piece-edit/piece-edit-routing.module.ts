import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PieceEditPage } from './piece-edit.page';

const routes: Routes = [
  {
    path: '',
    component: PieceEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PieceEditPageRoutingModule {}
