import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PieceEditPage } from './piece-edit.page';

describe('PieceEditPage', () => {
  let component: PieceEditPage;
  let fixture: ComponentFixture<PieceEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PieceEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PieceEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
