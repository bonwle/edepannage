
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, AlertController, PopoverController } from '@ionic/angular';


import { AuthentificationPage } from '../authentification/authentification.page';
import { UtilisateursService } from '../services/utilisateurs.service';
import { Utilisateurs } from '../models/utilisateurs';
import { ProfilService } from '../services/profil.service';
import { UserContext } from '../models/user-context';
import { RoleService } from '../services/role.service';


import { Chart } from 'chart.js';
import { InterventionService } from '../services/intervention.service';
import { setInterval } from 'timers';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  @ViewChild('general', { static: false }) general;
  @ViewChild('Planifiee', { static: false }) Planifiee;
  @ViewChild('enCours', { static: false }) enCours;
  @ViewChild('Realisee', { static: false }) Realisee;
  @ViewChild('nonAboutie', { static: false }) nonAboutie;

  generalChart: any;
  encoursChart: any;
  planifieeChart: any;
  realiseeChart: any;
  nonaboutieChart: any;
  nbrePlanifiee: number = 0;
  nbreEncours: number = 0;
  nbreRealise: number = 0;
  nbreNonabouti: number = 0;
  data: any;
  dataInterventions: any;
  startDate: Date;
  endDate: Date;

  userAuthData: Utilisateurs;
  userConnectData: any;
  userConnectName: string = "";
  authOK: boolean;
  authMessage: string;

  dateData: any;
  dataGroupe: any;
  public shouldShow = true;
  dataEncours: any;
  countEncours: any;
  dataPlanifie: any;
  countPlanifie: any;
  dataSuspendue: any;
  countSuspendus: any;
  dataTerminee: any;
  countTermine: any;
  roleId: any;
  grapheStartDate: Date;
  grapheEndDate: Date;
  grapheDataX: any;
  tempData: any;
  allStatusData: any;
  nbreEncoursData: any;
  valeurPlanifie: number = 0;
  valeurEncours: number = 0;
  valeurSuspendue: number = 0;
  valeurTermine: number = 0;

  constructor
    (
      private router: Router,
      private modalCntrl: ModalController,
      private authAlertCtrl: AlertController,
      private apiAuthUserService: UtilisateursService,
      private apiServiceProfil: ProfilService,
      private apiRole: RoleService,
      private apiInterventionService: InterventionService,
      private popupUserConnect: PopoverController,

  ) {
    this.userAuthData = new Utilisateurs;
    this.userConnectData = [];
    this.authOK = true;
    this.authMessage = "";


    this.dateData = [];
    this.data = [];
    this.dataInterventions = [];
    this.dataGroupe = [];
    this.dataEncours = [];
    this.countEncours = [];
    this.dataPlanifie = [];
    this.countPlanifie = [];
    this.dataSuspendue = [];
    this.countSuspendus = [];
    this.dataTerminee = [];
    this.countTermine = [];
    this.grapheDataX = [];
    this.tempData = [];
    this.allStatusData = [];
    this.nbreEncoursData = [];
    this.valeurPlanifie = 0;
    this.valeurEncours = 0;
    this.valeurSuspendue = 0;
    this.valeurTermine = 0;

    var dat = new Date();
    var week = dat.getDay();
    var jrs = week == 0 ? 6 : week - 1;
    this.grapheStartDate = new Date(dat.setDate(dat.getDate() - jrs));
    this.grapheEndDate = new Date(dat.setDate(dat.getDate() + 6));

  }

  ngOnInit() {
    this.authPrompt();
    this.getGrapheData();

  }

  generatelChart(value1, ordonees1, ordonees2, ordonees3, ordonees5) {

    let dt = this.general.nativeElement;
    dt.height = 200;
    this.generalChart = new Chart(dt, {
      type: 'line',
      data: {
        //labels: [value1,value2,value3,value4],
        labels: value1,

        datasets: [
          {
            type: "line",
            fill: 'true',
            label: 'Planifiée(s)',
            // data: [1.5, 14, 3, 4.9, 4.9, 5.5, 7, 12, 20, 6],
            // data:[ordonees1,ordonees2,ordonees3,ordonees5],
            data: ordonees1,
            backgroundColor: 'transparent',
            borderColor: 'rgb(99,101,104)',
            borderWidth: 1,
          }
          ,
          {
            type: 'line',
            label: 'En cours',
            //data: [20, 10, 4, 7, 11,5,6],
            data: ordonees2,

            // backgroundColor: 'rgb(242, 38, 19)',
            backgroundColor: "transparent",
            borderColor: 'rgb(14,135,240)',
            borderWidth: 2,
            spanGaps: true
          },
          {
            type: 'line',
            label: 'Terminée(s)',
            //data: [10,19,14,5,1],
            data: ordonees3,

            backgroundColor: "transparent",
            borderColor: 'rgb(46, 208, 41)',
            borderWidth: 3,
            lineTension: 0.1,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            // pointBorderColor: 'rgba(75,192,192,1)',
            // pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            // pointHoverRadius: 5,
            // pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            // pointHoverBorderColor: 'rgba(220,220,220,1)',
            // pointHoverBorderWidth: 2,
            pointRadius: 3,
            pointHitRadius: 10,
            spanGaps: true,
          },
          {
            type: 'line',
            label: 'Suspendue(s)',
            //data: [10,2,6,1,2],
            data: ordonees5,

            backgroundColor: "transparent",
            borderColor: 'rgb(237,38,12)',
            borderWidth: 4,
            spanGaps: false
          }
        ]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'GRAPHE GENERAL'
        },
        legend: {
          display: false,
          // labels: {
          //     fontColor: 'rgb(255, 255, 255)'
          // }
        },
        scales: {

          yAxes: [{
            ticks: {
              beginAtZero: true,
              stepSize: 1
            },
            stacked: false,
            gridLines: {
              display: true,
              color: "rgba(0,200,100)"
            }
          }],

          xAxes: [{
            //     type: 'time',
            ticks: {
              //     min: "2019-12-10",
              //     max: "2020-01-18",
              //     displayFormats: {
              //       day: "MMM YY DD"
              //   },

              beginAtZero: true,
              maxTicksLimit: 40
            },
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'DATES D\'INTERVENTIONS'
            },
            gridLines: {
              display: false

            }
          }],
        }
      }
    });

    setInterval(() => this.rafraichirData(), 5000);

  }

  rafraichirData() {
    this.generalChart.update();
    this.generalChart.render();

  }


  ///-------planifiee
  generatePlanifiee(abcisses1, ordonees1) {
    let dt = this.Planifiee.nativeElement
    dt.height = 200;
    this.planifieeChart = new Chart(dt, {
      type: 'bar',
      data: {
        // labels: [ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
        labels: abcisses1,

        datasets: [
          {
            type: "line",
            fill: 'true',
            label: 'Légende',
            // data: [1.5, 14, 3, 4.9, 4.9, 5.5, 7, 12, 20, 6],
            //data:[ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
            data: ordonees1,
            backgroundColor: 'transparent',
            borderColor: 'rgb(99,101,104)',
            borderWidth: 1.5
          }
        ]
      },
      options: {
        legend: {
          display: false,
          // labels: {
          //     fontColor: 'rgb(255, 255, 255)'
          // }
        },
        scales: {
          xAxes: [{

          }],
          yAxes: [{
            ticks: {
              beginAtZero: true
            },
            stacked: false
          }]
        }
      }
    });

  }

  //-----------en cours 
  generateCours(abcisses1, ordonees2) {
    let dt = this.enCours.nativeElement
    dt.height = 200;
    this.encoursChart = new Chart(dt, {
      type: 'bar',
      data: {
        // labels: [ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
        labels: abcisses1,

        datasets: [
          {
            type: "line",
            fill: 'true',
            label: 'Légende',
            // data: [1.5, 14, 3, 4.9, 4.9, 5.5, 7, 12, 20, 6],
            //data:[ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
            data: ordonees2,
            backgroundColor: "transparent",
            borderColor: 'rgb(14,135,240)',
            borderWidth: 1.5
          }
        ]
      },
      options: {
        legend: {
          display: false,
          // labels: {
          //     fontColor: 'rgb(255, 255, 255)'
          // }
        },

        scales: {
          xAxes: [{

          }],
          yAxes: [{
            ticks: {
              stepSize: 1
            },
            stacked: false
          }]
        }
      }
    });

  }
  //----------- realisee  
  generatelRealisee(abcisses1, ordonees3) {
    let dt = this.Realisee.nativeElement
    dt.height = 200;
    this.realiseeChart = new Chart(dt, {
      type: 'bar',
      data: {
        // labels: [ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
        labels: abcisses1,

        datasets: [
          {
            type: "line",
            fill: 'true',
            label: 'Légende',
            // data: [1.5, 14, 3, 4.9, 4.9, 5.5, 7, 12, 20, 6],
            //data:[ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
            data: ordonees3,
            backgroundColor: "transparent",
            borderColor: 'rgb(46, 208, 41)',
            borderWidth: 1.5
          }

        ]
      },
      options: {
        legend: {
          display: false,
          // labels: {
          //     fontColor: 'rgb(255, 255, 255)'
          // }
        },

        scales: {
          xAxes: [{

          }],
          yAxes: [{
            ticks: {
              stepSize: 1
            },
            stacked: false
          }]
        }
      }
    });

  }

  //------------- non aboutie 
  generatelNonAboutie(abcisses1, ordonees4) {
    let dt = this.nonAboutie.nativeElement
    dt.height = 200;
    this.nonaboutieChart = new Chart(dt, {
      type: 'bar',
      data: {
        // labels: [ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
        labels: abcisses1,

        datasets: [
          {
            type: "line",
            fill: 'true',
            label: 'Légende',
            // data: [1.5, 14, 3, 4.9, 4.9, 5.5, 7, 12, 20, 6],
            //data:[ordonees1,ordonees2,ordonees3,ordonees4,ordonees5],
            data: ordonees4,
            backgroundColor: "transparent",
            borderColor: 'rgb(237,38,12)',
            borderWidth: 1.5,

          }

        ]
      },
      options: {
        legend: {
          display: false,
          // labels: {
          //     fontColor: 'rgb(255, 255, 255)'
          // }
        },
        scales: {
          xAxes: [{

          }],
          yAxes: [{
            ticks: {
              stepSize: 1
            },
            stacked: false

          }]
        }
      }
    });

  }


  formatDate(date1) {
    var dt = [];
    dt = date1.split('-');
    var year = dt[0];
    var month = dt[1];
    var day = dt[2];
    var dateFormated = day + "-" + month + "-" + year;
    return dateFormated;
  }

  getGrapheData() {
    this.apiInterventionService.getGroupByDate().subscribe(response => {
      this.dataGroupe = response;
      this.tempData = response;
      this.getSelectPeriodData();
    });
  }

  getSelectPeriodData() {

    // this.generalChart(null,null,null,null,null);
    // this.generalChart.update();
    if ((this.grapheEndDate.toISOString().split('T')[0]) < (this.grapheStartDate.toISOString().split('T')[0])) {
      var tempDate = this.grapheStartDate;
      this.grapheStartDate = this.grapheEndDate;
      this.grapheEndDate = tempDate;
    }
    this.allStatusData = this.tempData.filter((item) =>
      item._id.DateDebutIntervention >= this.grapheStartDate.toISOString().split('T')[0] && item._id.DateDebutIntervention <= this.grapheEndDate.toISOString().split('T')[0]);
    this.buildingGrapheData();
  }


  initGrapheData() {
    this.countEncours = [];
    this.countPlanifie = [];
    this.countSuspendus = [];
    this.countTermine = [];
    this.dataEncours = [];
    this.dataPlanifie = [];
    this.dataTerminee = [];
    this.dataSuspendue = [];
    this.grapheDataX = [];
    this.valeurTermine = 0;
    this.valeurSuspendue = 0;
    this.valeurPlanifie = 0;
    this.valeurEncours = 0;
  }

  buildingGrapheData() {
    this.initGrapheData();
    var endDateSelected = true;
    var dateStart = this.grapheStartDate;
    var dateEnd = this.grapheEndDate;
    this.grapheDataX.push(this.grapheStartDate.toISOString().split('T')[0]);
    do {
      if (dateStart.toISOString().split('T')[0] < dateEnd.toISOString().split('T')[0]) {
        dateStart.setDate(dateStart.getDate() + 1);
        this.grapheDataX.push(dateStart.toISOString().split('T')[0]);
      }
      else
        endDateSelected = false;

    } while (endDateSelected);


    this.allStatusData.forEach(data => {

      if (data._id.statut == "Planifiée") {
        this.dataPlanifie.push(data._id.DateDebutIntervention);
        this.countPlanifie.push(data.count);
        this.valeurPlanifie += data.count;
      }

      if (data._id.statut == "En cours") {
        this.dataEncours.push(data._id.DateDebutIntervention);
        this.countEncours.push(data.count);
        this.valeurEncours += data.count;
      }

      if (data._id.statut == "Suspendue") {
        this.dataSuspendue.push(data._id.DateDebutIntervention);
        this.countSuspendus.push(data.count);
        this.valeurSuspendue += data.count;

      }

      if (data._id.statut == "Terminée") {
        this.dataTerminee.push(data._id.DateDebutIntervention);
        this.countTermine.push(data.count);
        this.valeurTermine += data.count;
      }
    });

    var tabNbrePlanifie = [], tabNbreSuspendue = [], tabNbreTermine = [], tabNbreSuspendue = [], tabNbreEncours = [], valEncours = 0, valPlanifie = 0, valTermine = 0, valSuspendus = 0;
    for (let j = 0; j < this.grapheDataX.length; j++) {

      if (this.dataEncours[valEncours] === this.grapheDataX[j]) {

        tabNbreEncours.push(this.countEncours[valEncours]);
        valEncours++;

      }
      else {
        if (this.dataEncours[valEncours] > this.grapheDataX[j]) {
          tabNbreEncours.push(0);
        }

      }

      if (this.dataTerminee[valTermine] === this.grapheDataX[j]) {

        tabNbreTermine.push(this.countTermine[valTermine]);
        valTermine++;

      }
      else {
        if (this.dataTerminee[valTermine] > this.grapheDataX[j]) {
          tabNbreTermine.push(0);
          // k++;
        }

      }

      if (this.dataSuspendue[valSuspendus] === this.grapheDataX[j]) {

        tabNbreSuspendue.push(this.countSuspendus[valSuspendus]);
        valSuspendus++;

      }
      else {
        if (this.dataSuspendue[valSuspendus] > this.grapheDataX[j]) {
          tabNbreSuspendue.push(0);
        }

      }


      if (this.dataPlanifie[valPlanifie] === this.grapheDataX[j]) {

        tabNbrePlanifie.push(this.countPlanifie[valPlanifie]);
        valPlanifie++;

      }
      else {
        if (this.dataPlanifie[valPlanifie] > this.grapheDataX[j]) {
          tabNbrePlanifie.push(0);
        }

      }

    }

    var dateform = [];
    var dateGraphePlanifie = [];
    var dateGrapheTermine = [];
    var dateGrapheEncours = [];
    var dateGrapheSuspendus = [];

    this.grapheDataX.forEach(element => {
      dateform.push(this.formatDate(element));

    });

    this.dataEncours.forEach(x => {
      dateGrapheEncours.push(this.formatDate(x));

    });

    this.dataPlanifie.forEach(x => {
      dateGraphePlanifie.push(this.formatDate(x));

    });

    this.dataTerminee.forEach(x => {
      dateGrapheTermine.push(this.formatDate(x));

    });

    this.dataSuspendue.forEach(x => {
      dateGrapheSuspendus.push(this.formatDate(x));

    });

    this.generatelChart(dateform, tabNbrePlanifie, tabNbreEncours, tabNbreTermine, tabNbreSuspendue);
    this.generatePlanifiee(dateGraphePlanifie, this.countPlanifie);
    this.generateCours(dateGrapheEncours, this.countEncours);
    this.generatelRealisee(dateGrapheTermine, this.countTermine);
    this.generatelNonAboutie(dateGrapheSuspendus, this.countSuspendus);
  }

  OpenFen(item, val) {
    if (val == 0) return;
    this.router.navigate(['intervention-list/' + item]);
    // console.log("Redirect to list interventions");
  }

  async openAuthModal() {
    const modal = await this.modalCntrl.create({
      component: AuthentificationPage,
      animated: true,
      backdropDismiss: false
    });
    return await modal.present();
  }




  async authPrompt() {
    if (UserContext.userConnect != undefined) {
      this.userConnectName = UserContext.userConnect[0].NomUser;
      return;
    }
    const alert = await this.authAlertCtrl.create(
      {
        header: 'Authentification',
        backdropDismiss: false,
        inputs: [
          {
            label: 'Login: ',
            name: 'userAuthName',
            type: 'text',
            placeholder: 'Nom utilisateur'
          },
          {
            label: 'Password: ',
            name: 'userAuthPassword',
            type: 'password',
            placeholder: 'Mot de passe',
          }
        ],
        buttons: [
          {
            text: 'se connecter',

            handler: val => {
              //this.confirmUser(this.userAuthData.NomUser,this.userAuthData.MotDePasse);
              // credential confirmation 
              if (this.confirmUser(val.userAuthName, val.userAuthPassword)) {
                return true;
              }
              else {
                val.message = ("Mot de passe ou login incorrect.".fontcolor("danger"));
                return false;
              }
            }
          }
        ],
        message: this.authMessage
      }
    );
    await alert.present();
  }





  confirmUser(nameUserAuth, passwordUserAuth): any {
    this.apiAuthUserService.getUtilisateurConnecte(nameUserAuth, passwordUserAuth).subscribe((response) => {
      if (response['length'] == 0) {
        return false;
      }
      else {
        //recuperer premier elt array reponse
        this.userConnectData = response[0];
        //recuperer tous les elts reponse
        this.userAuthData = response;
        //recuperer user et id 
        UserContext.logInUser(response);
        //recuperer roles de user connecté 
        this.getuserConnectRole();
        //fermer popover
        this.authClose();
        //recuperer nom de user connecté 
        this.userConnectName = UserContext.userConnect[0].NomUser;
        return true;
      }
    });
  }

  authClose() {
    this.router.navigate(['home']);
    this.authAlertCtrl.dismiss();
  }




  getuserConnectRole() {
    if (UserContext.userConnect[0].IdProfil == null)
      return;
    this.apiServiceProfil.getProfil(UserContext.userConnect[0].IdProfil).subscribe((response) => {
      UserContext.userRolesList = response.rolesIds;
      console.log(UserContext.userRolesList);
    });
  }



  async presentPopover(ev: any) {
    const popover = await this.popupUserConnect.create({
      component: AuthentificationPage,
      event: ev,
      showBackdrop: true,
      translucent: true,
    });
    return await popover.present();
  }

}

