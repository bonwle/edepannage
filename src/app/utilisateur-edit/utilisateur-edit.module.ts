import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UtilisateurEditPageRoutingModule } from './utilisateur-edit-routing.module';

import { UtilisateurEditPage } from './utilisateur-edit.page';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    UtilisateurEditPageRoutingModule
  ],
  declarations: [UtilisateurEditPage]
})
export class UtilisateurEditPageModule {}
