import { Component, OnInit } from '@angular/core';
import { Utilisateurs } from '../models/utilisateurs';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilisateursService } from '../services/utilisateurs.service';
import { ToastController } from '@ionic/angular';
import { ProfilService } from '../services/profil.service';
@Component({
  selector: 'app-utilisateur-edit',
  templateUrl: './utilisateur-edit.page.html',
  styleUrls: ['./utilisateur-edit.page.scss'],
})
export class UtilisateurEditPage implements OnInit {

  id: number;
  editData: Utilisateurs;
  UserEditprofilData: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiService: UtilisateursService,
    private toastCtrl: ToastController,
    private apiServiceProfil: ProfilService
  ) 
  { this.editData=new Utilisateurs();
    this.UserEditprofilData=[];
  }
 
  ngOnInit() {
    this.id=this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.getUtilisateur(this.id).subscribe(response=>{
      console.log(response);
      this.editData=response;
      this.getProfils();
    })
  }

  update()
  {
    console.log(this.editData);
    this.apiService.updateUtilisateur(this.id, this.editData).subscribe(response=>{
    this.router.navigate(['administrateur']);
    this.displayToastEdit();

  })
  }

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"middle",
      }
    );
    toast.present();
  }
  getProfils()
  {
    this.apiServiceProfil.getAllProfil().subscribe(response=>{
      console.log("Profil");
      this.UserEditprofilData=response;
    });
  }
}
