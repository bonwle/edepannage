import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UtilisateurEditPage } from './utilisateur-edit.page';

const routes: Routes = [
  {
    path: '',
    component: UtilisateurEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UtilisateurEditPageRoutingModule {}
