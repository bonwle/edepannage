import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Chart } from 'chart.js';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Component({

  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  dataInterventions: any;
  @ViewChild('pieCanvas', {static:false}) pieCanvas: ElementRef;
  @ViewChild('barCanvas', {static:false}) barCanvas: ElementRef;
  @ViewChild('doughnutCanvas', {static:false}) doughnutCanvas: ElementRef;
  @ViewChild('lineCanvas',{static:false}) lineCanvas: ElementRef;


  //@ViewChild('hrzLineChart') hrzLineChart;
  @ViewChild('hrzLineChart4', {static:false}) hrzLineChart4;
  @ViewChild('hrzLineChart3', {static:false}) hrzLineChart3;

  private barChart: Chart;
  private doughnutChart: Chart;
  private lineChart: Chart;
  private pieChart: Chart;
  
  /*--------------------------*/
 
  lines: any;
  hrzLines: any;
  hrzLines4: any;
  hrzLines3: any;
  colorArray: any;


     apiData: any;
      ionViewDidEnter() {
    this.createAreaChart();
    this.createSimpleLineChart();
    this.createGroupLineChart();
    
   // this.createHrzLineChart3();
  }

  constructor(
    private alertCtrl: AlertController,
    private httpVar: HttpClient
  ) {
    this.dataInterventions = [];
    this.apiData =[];
  }
  ngOnInit() {
   // this.getListInterventions();
   // this.createHrzLineChart3(labels,values);
   // this. fetchData();

    this.pieChart = new Chart(this.pieCanvas.nativeElement, {
      type: "pie",
      data: {
        labels: ["en vue", "en cours", "fini"],
        datasets: [
          {
            label: "# x ",
            data: [100,10,200],
            backgroundColor: [
              "rgba(100,1,145,10)",
              "rgba(11,50,14,30)",
              "rgba(50,10,145,200)"
            ],
            hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
          }
        ]
      }
    });

    /*---------*/
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "bar",
      data: {
        labels: ["janvier", "fevrier", "mars", "avril","mai","juin","jullet","aout",
        "septembre","octobre","novembre","decembre"],
        datasets: [
          {
            label: "# taux d'intervention mensuelles",
            data: [5, 10, 20, 14, 5, 9 , 4,11, 30, 1,4,6 ],
            backgroundColor: [
              "rgba(255, 99, 132, 0.2)",
              "rgba(54, 162, 235, 0.2)",
              "rgba(255, 206, 86, 0.2)",
              "rgba(75, 192, 192, 0.2)",
              "rgba(153, 102, 255, 0.2)",
              "rgba(255, 159, 64, 0.2)"
            ],
            borderColor: [
              "rgba(255,99,132,1)",
              "rgba(54, 162, 235, 1)",
              "rgba(255, 206, 86, 1)",
              "rgba(75, 192, 192, 1)",
              "rgba(153, 102, 255, 1)",
              "rgba(255, 159, 64, 1)"
            ],
            borderWidth: 1
          }
        ]
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      }
    });
/*----------------*/
// tslint:disable-next-line: align
this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
  // tslint:disable-next-line: quotemark
  type: "doughnut",
  data: {
    labels: ["avec succes", "annulé", "terminé", "en cours", "à faire"],
    datasets: [
      {
        label: "# of Votes",
        data: [50,10,60,19,13],
        backgroundColor: [
          // tslint:disable-next-line: quotemark
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
          "rgba(75, 192, 192, 0.2)",
          "rgba(153, 102, 255, 0.2)",
          "rgba(255, 159, 64, 0.2)"
        ],
        hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56", "#FF6384", "#36A2EB", "#FFCE56"]
      }
    ]
  }
});

// tslint:disable-next-line: align
this.lineChart = new Chart(this.lineCanvas.nativeElement, {
  type: "line",
  data: {
    labels: ["intervention1", "intervention2", "intervention2", "intervention2", "intervention2", "intervention2", "intervention2"],
    datasets: [
      {
        label: "duree par intervention",
        fill: true,
        lineTension: 0.1,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        pointBorderColor: "rgba(75,192,192,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [2, 5, 1, 7, 8, 10, 3],
        spanGaps: false
      }
    ]
  }
   });
    
  }


/*--------------definition de function ---------------*/
createAreaChart(){

}

createSimpleLineChart(){

}

createGroupLineChart(){

}

createHrzLineChart3(labels,values) {

 // this.getListInterventions();

  let ctx = this.hrzLineChart3.nativeElement
  ctx.height = 400;
  this.hrzLines3 = new Chart(ctx, {
    type: 'line',
    data: {
      
     // labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7','S8'],
     labels : labels,

      datasets: [{
        type : 'bar',
        label: 'nombre de connexion par annee',
       // data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
       data : values,
       
        backgroundColor: 'rgb(242, 38, 19)',
        borderColor: 'rgb(242, 38, 19)',
        borderWidth: 1,
        spanGaps: false
      },
      {
        fill:'false',
        label: 'nombre de like par annee ',
         data: [1.5, 2.8, 3, 4.9, 4.9, 5.5, 7, 12],
        //  data:values,
        backgroundColor: 'rgb(38, 194, 129)',
        borderColor: 'rgb(38, 194, 129)',
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          stacked: true
        }]
      }
    }
  });

  /*-----test */
  let ct = this.hrzLineChart4.nativeElement
  ct.height = 400;
  this.hrzLines4 = new Chart(ct, {
    type: 'line',
    data: {

     // labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7','S8'],
     labels : labels,

      datasets: [{
        label: 'nombre de connexion par annee',
        data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
        fill:'false',
       //data : values,
       backgroundColor: 'rgb(38, 100, 129)',
       borderColor: 'rgb(38, 200, 129)',
       // backgroundColor: 'rgb(24, 3, 19)',
       // borderColor: 'rgb(242, 38, 19)',
        borderWidth: 1
      },
      {
        label: 'nombre de like par annee ',
         data: [3, 2.8, 4, 8, 6.9, 7.5, 7,5],
         fill: false,
         //data:values,
        backgroundColor: 'rgb(38, 100, 129)',
        borderColor: 'rgb(38, 200, 129)',
        borderWidth: 1
      },
      {
        label: 'nombre de cool',
        // data: [1, 7.1, 7.9, 6, 4.3, 5.1,7, 2.1],
        data: [0, 2.8, 9, 6.9, 2.9, 1.5, 10, 11],
         fill: false,
        //  data:values,
        backgroundColor: 'rgb(38, 100, 129)',
        borderColor: 'rgb(38, 8, 100)',
        borderWidth: 1
      }
    
    ]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          stacked: true
        }]
      }
    }
  });


  }

  ionViewWillEnter()
  {
   // this.getListInterventions();
    console.log("Initialize view ok");
  }


  delete(item)
  {
    //this.confirmDeleted(item);
  }


}
