

import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { RoleDetailPage } from '../role-detail/role-detail.page';
import { RoleService } from '../services/role.service';
import { RoleCreatePage } from '../role-create/role-create.page';
import { RoleEditPage } from '../role-edit/role-edit.page';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.page.html',
  styleUrls: ['./role-list.page.scss'],
})

export class RoleListPage implements OnInit {
  tableStyle = 'material';
  public columns: any;
  public rows: any;
  filteredData = [];
  roleData: any;

  constructor(
    public apiService: RoleService,
    private modalCntrl: ModalController,
    private alertCtlr: AlertController
  ) 
  { this.roleData = [];}

  ngOnInit() {
    this.getAllRole();
  }

  ionViewWillEnter()
  {
    this.getAllRole();
  }
  getAllRole()
  {
    this.apiService.getListRole().subscribe(response=>{
      this.roleData=response;
    })
  }

  delete(item)
  {
    this.confirmDeleted(item);
  }

  async confirmDeleted(item)
   {
     const alert= await this.alertCtlr.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deleteRole(item._id).subscribe(response=>{this.getAllRole()})
            }
          }
            ] 
           
       }
     );
     await alert.present()
   }


  async openDetailModal(data) {
    const modal = await this.modalCntrl.create({
     component: RoleDetailPage,
     animated:true,
     componentProps:{detailData:data}
    });

    return await modal.present();
   }

   async openEditModal(data) {
    const modal = await this.modalCntrl.create({
     component: RoleEditPage,
     animated:true,
     backdropDismiss:false,
     componentProps:{editData:data}
    });

    return await modal.present();
   }

   async openAddModal() {
    const modal = await this.modalCntrl.create({
     component: RoleCreatePage,
     animated:true,
     backdropDismiss:false,
    });

    return await modal.present();
   }

   
}

