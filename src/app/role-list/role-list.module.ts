import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RoleListPageRoutingModule } from './role-list-routing.module';

import { RoleListPage } from './role-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoleListPageRoutingModule, NgxDatatableModule
  ],
  declarations: [RoleListPage]
})
export class RoleListPageModule {}
