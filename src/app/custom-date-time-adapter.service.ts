import { Injectable } from '@angular/core';
import { DateTimeAdapter } from 'ng-pick-datetime';

export const CUSTOM_DATE_TIME_FORMATS = {
    parseInput: 'DD/MM/YYYY',
    fullPickerInput: 'DD/MM/YYYY hh:mm a',
    datePickerInput: 'DD/MM/YYYY',
    timePickerInput: 'hh:mm a',
    monthYearLabel: 'MMM-YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM-YYYY'
 
};

@Injectable({
  providedIn: 'root'
})

export class CustomDateTimeAdapterService  {

  constructor() { }
}
