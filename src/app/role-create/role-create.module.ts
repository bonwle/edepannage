import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoleCreatePageRoutingModule } from './role-create-routing.module';

import { RoleCreatePage } from './role-create.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule, OwlInputModule,
    RoleCreatePageRoutingModule, NgxDatatableModule
  ],
  declarations: [RoleCreatePage]
})
export class RoleCreatePageModule {}
