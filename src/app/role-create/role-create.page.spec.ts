import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoleCreatePage } from './role-create.page';

describe('RoleCreatePage', () => {
  let component: RoleCreatePage;
  let fixture: ComponentFixture<RoleCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoleCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
