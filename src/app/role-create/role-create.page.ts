import { Component, OnInit } from '@angular/core';
import { RoleService } from '../services/role.service';
import { Router } from '@angular/router';
import { Role } from '../models/role';
@Component({
  selector: 'app-role-create',
  templateUrl: './role-create.page.html',
  styleUrls: ['./role-create.page.scss'],
})
export class RoleCreatePage implements OnInit {
     dataRole: Role;

  constructor(
    public apiService: RoleService,
    public router: Router,
  ) 
  {
    this.dataRole = new Role();
  }

  ngOnInit() {
  }


  submitForm()
  {
    this.apiService.createRole(this.dataRole).subscribe((response)=>{
      this.router.navigate(['administrateur']);
    })
  }
}
