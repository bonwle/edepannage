import { Component, OnInit } from '@angular/core';
import { EquipeService } from '../services/equipe.service';
import { ModalController, AlertController } from '@ionic/angular';

import { EquipeDetailPage } from '../equipe-detail/equipe-detail.page';
import { EquipeCreatePage } from '../equipe-create/equipe-create.page';
import {EquipeEditPage} from '../equipe-edit/equipe-edit.page';

@Component({
  selector: 'app-equipe-list',
  templateUrl: './equipe-list.page.html',
  styleUrls: ['./equipe-list.page.scss'],
})
export class EquipeListPage implements OnInit{

  tableStyle = 'material';
  public columns: any;
  public rows: any;
  filteredData = [];
  dataEquipe: any;

  constructor(
    public apiService: EquipeService,
    private modalCntrl: ModalController,
    private alertCtrl: AlertController
  )
  { 
    this.dataEquipe = [];
  }

  ngOnInit() {
    this.getAllEquipes();
  }

  ionViewWillEnter()
  {
    this.getAllEquipes();
    console.log("Initialize view ok");
  }

  getAllEquipes()
  {
    this.apiService.getEquipes().subscribe(response=>{
      console.log(response);
      this.dataEquipe=response;
    })
  }

  delete(item)
  {
    this.confirmDeleted(item);
  }

  
  async openDetailModal(data) {
    const modal = await this.modalCntrl.create({
     component: EquipeDetailPage,
     animated:true,
     componentProps:{equipeDetail:data}
    });
    return await modal.present();
   }

   async openAddModal() {
    const modal = await this.modalCntrl.create({
     component: EquipeCreatePage ,
     animated:true,
     backdropDismiss:false
    });
    return await modal.present();
   }

   async openEditModal(data) 
   {
     console.log(data);
    const modal = await this.modalCntrl.create({
     component: EquipeEditPage,
     componentProps:{EditData:data},
     animated:true,
     backdropDismiss:true
    });
    return await modal.present();
   }

   async confirmDeleted(item)
   {
     const alert= await this.alertCtrl.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deleteEquipe(item._id).subscribe(response=>{this.getAllEquipes()});
            }
          }
            ] 
           
       }
     );
     await alert.present();
   }
}
