import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EquipeListPage } from './equipe-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EquipeListPageRoutingModule } from './equipe-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    NgxDatatableModule,
    EquipeListPageRoutingModule
     ],
  declarations: [EquipeListPage]
})
export class EquipeListPageModule {}
