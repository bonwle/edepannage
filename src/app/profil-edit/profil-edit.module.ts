import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilEditPageRoutingModule } from './profil-edit-routing.module';

import { ProfilEditPage } from './profil-edit.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfilEditPageRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
  ],
  declarations: [ProfilEditPage]
})
export class ProfilEditPageModule {}
