import { Component, OnInit } from '@angular/core';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Profil } from '../models/profil';
import { ProfilService } from '../services/profil.service';
import { RoleService } from '../services/role.service';
import { Role } from '../models/role';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-profil-edit',
  templateUrl: './profil-edit.page.html',
  styleUrls: ['./profil-edit.page.scss'],
})
export class ProfilEditPage implements OnInit {

  dataEditProfil: Profil;
  roleDataEdit: any;
  id: number;

  constructor
  (
    private activatedRoute: ActivatedRoute,
    private apiProfilService: ProfilService,
    private roleApiService: RoleService,
    private toastCtrl: ToastController,
    private router: Router,
  ) 
  {
    this.dataEditProfil=new Profil();
    this.roleDataEdit=[]; 
  }

  ngOnInit()
   {
      this.id=this.activatedRoute.snapshot.params["id"];
      this.apiProfilService.getProfil(this.id).subscribe(response=>{
      this.dataEditProfil=response;
      this.getRoles();
    })
  }

  getRoles()
  {
    this.roleApiService.getListRole().subscribe(response=>{
      this.roleDataEdit=response;
    })
  }


  updateProfil()
  {
    this.apiProfilService.updatePiece(this.id, this.dataEditProfil).subscribe(response=>{
    this.displayToastEdit();
    this.router.navigate(['administrateur']);
  })
  }

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"middle",
      }
    );
    toast.present();
  }
}
