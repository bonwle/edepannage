import { Component, OnInit } from '@angular/core';
import { InterventionService } from '../services/intervention.service';
import { AlertController,ToastController } from '@ionic/angular';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { UserContext } from '../models/user-context';
import { Router } from '@angular/router';
@Component({
  selector: 'app-intervention-list',
  templateUrl: './intervention-list.page.html',
  styleUrls: ['./intervention-list.page.scss'],
})
export class InterventionListPage implements OnInit {

  resulat:any;
  event : string ='test-event';
  channel : any ='edepannagetest';
  
  dataInterventions: any;
  data:any;
  columnData: string[];
  dataDiagnostic: any;
  dataFilter: any;
  resultat : any;
  liste:any;
  startDate: Date;
  endDate: Date;
  isRoleAddIntervention:boolean=true;
  isRoleEditIntervention:boolean=true;
  isRoleDetailIntervention: boolean=true;
  isRoleDeleteIntervention: boolean=true;
  isAllRolesIntervention:boolean=false;
  tabloStatus:any;

  constructor(
    public apiService: InterventionService,
    private alertCtrl: AlertController,
    private toasCtrl : ToastController,
    public diagnosticService: DiagnostiqueService,
    public dateTimeAdapter: DateTimeAdapter<any>,
    private router: Router,
  )
  { 
    this.dataInterventions = [];
    this.tabloStatus=['/intervention-list/Planifiee', '/intervention-list/Terminee', '/intervention-list/EnCours', '/intervention-list/Suspendue'];
    this.data=[];
    this.columnData=[];
    this.dataDiagnostic =[];
    this.dataFilter =[];
    this.resultat =[];
    this.liste=[];    
    var date= new Date();
    var weekDay=date.getDay();
    var day=weekDay==0?6:weekDay-1;
    this.startDate=new Date(date.setDate(date.getDate()-day));
    this.endDate=new Date(date.setDate(date.getDate()+6));
    }

  ngOnInit() 
  {
    this.apiService.refreshok.subscribe(()=>{

    //  this.channel = this.apiService.init();
    //   this.channel.bind(this.event,(data)=>{
    //     this.resulat = data.message;
    //     console.log(data.message);
    //     this.notif(data.message);
    //   });
      this.getListInterventions();
    });

    this.getListInterventions();
   this.getRolesConnectUser();
  }


 
  async notif(resultat)
  {
    const toast=await this.toasCtrl.create(
      {
        message:resultat,
        color:"success",
        duration:5000,
        position:"top"
      }
    );
    toast.present();
  }
  

  getRolesConnectUser()
  {
    if(UserContext.userRolesList==undefined)
    {
      return;
    }
    var _roles=UserContext.userRolesList;
    this.isRoleAddIntervention=(_roles.indexOf("AI01")==-1);
    this.isRoleEditIntervention=(_roles.indexOf("MI01")==-1);
    this.isRoleDetailIntervention=(_roles.indexOf("VDI01")==-1);
    this.isRoleDeleteIntervention=(_roles.indexOf("SI01")==-1);
    if(this.isRoleEditIntervention==true && this.isRoleDeleteIntervention==true && this.isRoleDetailIntervention==true)
    {
      this.isAllRolesIntervention=false;
    }
    else
    {
      this.isAllRolesIntervention=true;
    }

  }

  getListInterventions()
  {
    this.apiService.getInterventions().subscribe(response=>{
      let tab=[];
      this.dataInterventions=response;
      if(this.tabloStatus.indexOf(this.router.url)!=-1)
      {
        tab=this.dataInterventions.filter((item)=>'/intervention-list/'+item.statut==this.router.url);
        this.dataInterventions=tab;
        return;
      }
      this.data=response;
      this.dataFilter =response;
      this.columnData=Object.keys(this.data[0]);
      this.getPeriodData();
    })
  }


  delete(item)
  {
    this.diagnosticService.getDiagnosticIntervention(item._id).subscribe(response=>{

      if(response['length']>0)
      {
        this.confirmDeleted(item,"Impossible de supprimer une intervention liée à un diagnostic.",0);
        return;
      }
      else
        this.confirmDeleted(item,"Confirmez-vous la suppression de cet élément?",1);
    
   });
    
  }

  
  async confirmDeleted(item,msg, nbre)
  {
    
    const alert= await this.alertCtrl.create(
      {
        header:"Suppression",
        message:msg,
        animated:true,
        backdropDismiss:false,
        translucent:true,
        buttons:[
         {
           text:"Fermer",
           role:"cancel",
           cssClass: "secondary" 
         },
         {
           text: "Ok",
           handler:()=>{
             if(nbre==1)
             this.apiService.deleteIntervention(item._id).subscribe(response=>{this.getListInterventions()});
           }
         }
           ] 
          
      }
    );
    await alert.present()
  }

  


   getListInterventionsWithoutPeriod()
  {
    this.apiService.getInterventions().subscribe(response=>{
      this.dataInterventions=response;
      this.data=response;
      this.dataFilter =response;
      if(this.data[0]!=undefined)
      this.columnData=Object.keys(this.data[0]);
    })
  }

refresh()
{
  this.getListInterventionsWithoutPeriod();
 // this.dataInterventions = JSON.parse(JSON.stringify(this.data));
 }


onFilterSelectChange(selected, item)
{
  if(selected)
  {
    if(this.liste.indexOf(item)==-1)
    {
      this.liste.push(item);
    }
  }
  else
  {
    if(this.liste.indexOf(item)!==-1)
    {
      this.liste.splice(this.liste.indexOf(item),1);
    }
  }
  if(this.liste.length>0)
  {
    this.dataInterventions = this.dataFilter.filter((filtre)=>{
      let status = filtre.statut;
     if(this.liste.indexOf(status)!== -1) 
     {
      return  status;
     }
   });
  }
  else
  {
    this.dataInterventions=this.dataFilter;
  }
}

   
// calcul total frais de deplacement 
// totalFrais(){
//   let frais = this.data.reduce((previous, current)=>{
//     let prevResult = Number.isInteger(previous)? previous : previous.FraisDeplacement;
//     return prevResult + current.FraisDeplacement;
//   });
 
// }

//filter à l'aide des boutons 
//  filterData(){
//  this.dataInterventions = this.data.filter((filtre)=>{
//  return filtre.FraisDeplacement > 500 && filtre.FraisDeplacement < 2000 ;
//  });
//  }

getPeriodData()
{
  if((!this.endDate && !this.startDate)) return this.dataInterventions= this.data;

  if(this.startDate && this.endDate)
  {
    if(this.endDate<this.startDate)
    {
      var tempDate=this.startDate;
      this.startDate=this.endDate;
      this.endDate=tempDate;
    }
    this.dataInterventions = this.data.filter((item) =>
    item.DateDebutIntervention>=this.startDate.toISOString() && item.DateFinIntervention <= this.endDate.toISOString() );

  }
  else
  {
    if(this.startDate && this.endDate==null)
    {
      this.dataInterventions = this.data.filter((item) =>
      item.DateDebutIntervention==this.startDate.toISOString() && item.DateFinIntervention == this.startDate.toISOString());
    }
    else
    {
      this.dataInterventions = this.data.filter((item) =>
      item.DateDebutIntervention==this.endDate.toISOString() || item.DateFinIntervention == this.endDate.toISOString());
    }
  }

  this.dataFilter=this.dataInterventions;

}

searchData(event)
{
  this.getPeriodData();
    let filter=event.target.value.toLowerCase();
    if(filter.trim()==="")
    {
    this.dataInterventions=this.data;
  }
    else
    {
    this.dataInterventions=this.data.filter(item=>{
      for(let i=0; i<this.columnData.length; i++)
      {
        var colVal=item[this.columnData[i]];

        if(!filter|| (!!colVal && colVal.toString().toLowerCase().indexOf(filter.trim())!=-1))
        {
          return true;
        }
      }
    });
    }
   this.dataFilter=this.dataInterventions;   
}
}
