import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InterventionListPage } from './intervention-list.page';

const routes: Routes = [
  {
    path: '',
    component: InterventionListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterventionListPageRoutingModule {}
