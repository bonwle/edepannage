import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InterventionListPageRoutingModule } from './intervention-list-routing.module';

import { InterventionListPage } from './intervention-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    OwlFormFieldModule, 
    OwlInputModule, 
    OwlSelectModule,
    NgxDatatableModule,
    InterventionListPageRoutingModule
  ],
  declarations: [InterventionListPage]
})
export class InterventionListPageModule {}
