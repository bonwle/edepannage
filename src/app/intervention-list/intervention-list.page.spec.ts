import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InterventionListPage } from './intervention-list.page';

describe('InterventionListPage', () => {
  let component: InterventionListPage;
  let fixture: ComponentFixture<InterventionListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterventionListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InterventionListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
