import { Component, OnInit } from '@angular/core';
import { UtilisateursService } from '../services/utilisateurs.service';
import { ModalController, AlertController } from '@ionic/angular';
import { UtilisateurEditPage } from '../utilisateur-edit/utilisateur-edit.page';
import { UtilisateurDetailPage } from '../utilisateur-detail/utilisateur-detail.page';
import { UtilisateurCreatePage } from '../utilisateur-create/utilisateur-create.page';

@Component({
  selector: 'app-utilisateur-list',
  templateUrl: './utilisateur-list.page.html',
  styleUrls: ['./utilisateur-list.page.scss'],
})
export class UtilisateurListPage implements OnInit {

  utilisateursData: any;

  constructor(
    public apiService: UtilisateursService,
    private modalCntrl: ModalController,
    private alertCtlr: AlertController
  ) 
  { this.utilisateursData=[];}

  ngOnInit()
   {
    this.getAllUtilisateurs();
  }

  ionViewWillEnter()
  {
   this.getAllUtilisateurs();
  }

  getAllUtilisateurs()
  {
    this.apiService.getUtilisateurs().subscribe(response=>{
      console.log(response);
      this.utilisateursData=response;
    })
  }

  delete(item)
  {
    this.confirmDeleted(item);
  }

  async confirmDeleted(item)
   {
     const alert= await this.alertCtlr.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deleteUtilisateur(item._id).subscribe(response=>{this.getAllUtilisateurs()})
            }
          }
            ] 
           
       }
     );
     await alert.present()
   }


  async openDetailModal(data) {
    const modal = await this.modalCntrl.create({
     component: UtilisateurDetailPage,
     animated:true,
     componentProps:{utilisateurDetail:data}
    });

    return await modal.present();
   }

   async openEditModal(data) {
    const modal = await this.modalCntrl.create({
     component: UtilisateurEditPage,
     animated:true,
     backdropDismiss:false,
     componentProps:{editZoneData:data}
    });

    return await modal.present();
   }

   async openAddModal() {
    const modal = await this.modalCntrl.create({
     component: UtilisateurCreatePage,
     animated:true,
     backdropDismiss:false,
    });

    return await modal.present();
   }

}
