import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UtilisateurListPage } from './utilisateur-list.page';

const routes: Routes = [
  {
    path: '',
    component: UtilisateurListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UtilisateurListPageRoutingModule {}
