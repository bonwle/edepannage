import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UtilisateurListPageRoutingModule } from './utilisateur-list-routing.module';

import { UtilisateurListPage } from './utilisateur-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxDatatableModule,
    UtilisateurListPageRoutingModule
  ],
  declarations: [UtilisateurListPage]
})
export class UtilisateurListPageModule {}
