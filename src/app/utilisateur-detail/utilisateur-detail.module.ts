import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UtilisateurDetailPageRoutingModule } from './utilisateur-detail-routing.module';

import { UtilisateurDetailPage } from './utilisateur-detail.page';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    UtilisateurDetailPageRoutingModule
  ],
  declarations: [UtilisateurDetailPage]
})
export class UtilisateurDetailPageModule {}
