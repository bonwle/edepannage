import { Component, OnInit } from '@angular/core';
import { Utilisateurs } from '../models/utilisateurs';
import { ActivatedRoute} from '@angular/router';
import { UtilisateursService } from '../services/utilisateurs.service';
import { Profil } from '../models/profil';
import { ProfilService } from '../services/profil.service';

@Component({
  selector: 'app-utilisateur-detail',
  templateUrl: './utilisateur-detail.page.html',
  styleUrls: ['./utilisateur-detail.page.scss'],
})
export class UtilisateurDetailPage implements OnInit {

  id: number;
  detaildata: any;
  UserprofilData: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: UtilisateursService,
    private apiProfilService: ProfilService
  ) 
  { this.detaildata=[];
    this.UserprofilData=[];
  }
 
  ngOnInit() 
  {
    this.id=this.activatedRoute.snapshot.params["id"];
    this.apiService.getUtilisateur(this.id).subscribe(response=>{
      console.log(response);
      this.detaildata=response;
      this.getProfils();
    })
  }

 getProfils()
  {
    this.apiProfilService.getAllProfil().subscribe(response=>{
      console.log("Profil");
      this.UserprofilData=response;
    });
  }

}
