import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UtilisateurDetailPage } from './utilisateur-detail.page';

const routes: Routes = [
  {
    path: '',
    component: UtilisateurDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UtilisateurDetailPageRoutingModule {}
