import { Component, OnInit } from '@angular/core';
import { Diagnostique } from '../models/diagnostique';
import { ActivatedRoute, Router } from '@angular/router';
import { DiagnostiqueService } from '../services/diagnostique.service';
import { ToastController, ModalController, NavParams } from '@ionic/angular';

@Component ({
  selector: 'app-diagnostique-edit',
  templateUrl: './diagnostique-edit.page.html',
  styleUrls: ['./diagnostique-edit.page.scss'],
})
export class DiagnostiqueEditPage implements OnInit {
  id: number;
  dataDiagnostique: Diagnostique;

  constructor(
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public DiagnosticService: DiagnostiqueService,
    public toastCtrl: ToastController,
    public ctlrModal: ModalController,
    public navParamsEditData: NavParams

  ) { this.dataDiagnostique = new Diagnostique();
  }

  ngOnInit() {
    this.id=this.navParamsEditData.get('DiagnostiqueEdit')['_id'];
    this.DiagnosticService.getDiagnostique(this.id).subscribe(response=>{
      console.log(response);
      this.dataDiagnostique=response;
    });
  }

  //update function 
  update()
  {
    console.log("ok update diagnostic");
    this.DiagnosticService.updateDiagnostique(this.id, this.dataDiagnostique).subscribe(response=>{
   // this.router.navigate(['diagnostiqueList']);
    this.displayToastEdit();
    this.closeModal();

  })
  }

  
  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"bottom",
        closeButtonText:"X",
        animated:true,
        showCloseButton:true,
        
      }
    );
    toast.present();
  }

  closeModal()
  {
    this.router.navigate(['diagnostiqueList']);
    console.log("Redirect list ok");
    this.ctlrModal.dismiss();
  }

}
