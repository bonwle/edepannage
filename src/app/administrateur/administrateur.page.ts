import { Component, OnInit } from '@angular/core';
import { UtilisateursService } from '../services/utilisateurs.service';
import { EquipeService } from '../services/equipe.service';
import { ZoneService } from '../services/zone.service';
import { ProfilService } from '../services/profil.service';
import { RoleService } from '../services/role.service';
import { AlertController, PopoverController } from '@ionic/angular';
import { UserContext } from '../models/user-context';
import { AuthentificationPage } from '../authentification/authentification.page';

@Component({
  selector: 'app-administrateur',
  templateUrl: './administrateur.page.html',
  styleUrls: ['./administrateur.page.scss'],
})
export class AdministrateurPage implements OnInit {

  administrateurPages: string;
  utilisateursData: any;
  dataEquipe: any;
  zonesData: any;
  ProfilData: any;
  roleData: any;
  dataToSearch:any;
  columnData: string[];
  equipeDataToSearch: any;
  equipeColumnData: string[];
  zoneDataToSearch: any;
  zoneColumnData:string[];
  profilDataToSearch:any;
  profilColumnData:string[];
  roleDataTosearch: any;
  roleColumnData:string[];
  userRoles: any;
  userConnectName:string="";

  isRoleAddUser: boolean;
  isRoleEditUser:boolean;
  isRoleDetailUser:boolean;
  isRoleDeleteUser:boolean;
  isAllRolesUser:boolean;

  isRoleAddEquipe: boolean;
  isRoleEditEquipe: boolean;
  isRoleDetailEquipe: boolean;
  isRoleDeleteEquipe: boolean;
  isAllRolesEquipe: boolean;

  isRoleAddZone: boolean;
  isRoleEditZone: boolean;
  isRoleDetailZone: boolean;
  isRoleDeleteZone: boolean;
  isAllRolesZone:boolean;

  isRoleAddProfil:boolean;
  isRoleEditProfil:boolean;
  isRoleDetailProfil:boolean;
  isRoleDeleteProfil:boolean;
  isAllRolesProfil:boolean;
  

  constructor(
    private apiServiceUser: UtilisateursService,
    private apiServiceEquipe: EquipeService,
    private apiServiceZone: ZoneService,
    private apiServiceProfil: ProfilService,
    private apiServiceRole: RoleService,
    private alertCtlr: AlertController,
    private popupUserConnect: PopoverController,
  ) 
  {

    // tslint:disable-next-line: no-unused-expression
    this.dataEquipe,this.dataToSearch,this.columnData, this.ProfilData, 
    this.equipeColumnData, this.roleColumnData, this.zoneColumnData, this.profilColumnData,
    this.equipeDataToSearch, this.zoneDataToSearch, this.profilDataToSearch, this.roleDataTosearch,
    this.zonesData, this.utilisateursData=[];
    this.userRoles=UserContext.userRolesList;
  }

  ngOnInit() 
  {
    if(UserContext.userConnect!=undefined) this.userConnectName=UserContext.userConnect[0].NomUser;
    this.administrateurPages="usersPage" ;
    this.getAllUtilisateurs();
    this.getAllEquipes();
    this.getAllZones();
    this.getProfils();
    this.getAllRole();
    this.getRolesUserConnect();
   }

  ionViewWillEnter()
  {
   this.getAllUtilisateurs();
   this.getAllEquipes();
   this.getAllZones();
   this.getProfils();
   this.getAllRole();
  }

  async presentPopover(ev:any)
  {
    const popover= await this.popupUserConnect.create({
      component:AuthentificationPage,
      event:ev,
      showBackdrop:true,
      translucent: true,
    });
    return await popover.present();
  }


  getRolesUserConnect()
  {
    if(UserContext.userRolesList==undefined) return;
    var _roles=UserContext.userRolesList;
    this.isRoleAddUser=(_roles.indexOf("AU01")==-1);
    this.isRoleEditUser=(_roles.indexOf("MU01")==-1);
    this.isRoleDetailUser=(_roles.indexOf("VDU01")==-1);
    this.isRoleDeleteUser=(_roles.indexOf("SU01")==-1);
    if(this.isRoleEditUser==true && this.isRoleDetailUser==true && this.isRoleDeleteUser==true)
    {
      this.isAllRolesUser=false;
    }
    else
    {
      this.isAllRolesUser=true;
    }

    this.isRoleAddEquipe=(_roles.indexOf("AE01")==-1);
    this.isRoleEditEquipe=(_roles.indexOf("ME01")==-1);
    this.isRoleDetailEquipe=(_roles.indexOf("VDE01")==-1);
    this.isRoleDeleteEquipe=(_roles.indexOf("SE01")==-1);
    if(this.isRoleEditEquipe==true && this.isRoleDetailEquipe==true && this.isRoleDeleteEquipe==true)
    {
      this.isAllRolesEquipe=false;
    }
    else
    {
      this.isAllRolesEquipe=true;
    }

    this.isRoleAddZone=(_roles.indexOf("AZ01")==-1);
    this.isRoleEditZone=(_roles.indexOf("MZ01")==-1);
    this.isRoleDetailZone=(_roles.indexOf("VDZ01")==-1);
    this.isRoleDeleteZone=(_roles.indexOf("SZ01")==-1);
    if(this.isRoleEditZone==true && this.isRoleDetailZone==true && this.isRoleDeleteZone==true)
    {
      this.isAllRolesZone=false;
    }
    else
    {
      this.isAllRolesZone=true;
    }

    this.isRoleAddProfil=(_roles.indexOf("AP01")==-1);
    this.isRoleEditProfil=(_roles.indexOf("MP01")==-1);
    this.isRoleDetailProfil=(_roles.indexOf("VDP01")==-1);
    this.isRoleDeleteProfil=(_roles.indexOf("SP01")==-1);
    if(this.isRoleEditProfil==true && this.isRoleDetailProfil==true && this.isRoleDeleteProfil==true)
    {
      this.isAllRolesProfil=false;
    }
    else
    {
      this.isAllRolesProfil=true;
    }

  }

  getAllUtilisateurs()
  {
    this.apiServiceUser.getUtilisateurs().subscribe(response=>{
      console.log(response);
      this.utilisateursData=response;
      this.dataToSearch=response;
      this.columnData=Object.keys(this.dataToSearch[0]);
    })
  }

  getAllZones()
  {
    this.apiServiceZone.getZones().subscribe(response=>{
      console.log(response);
      this.zonesData=response;
      this.zoneDataToSearch=response;
      this.zoneColumnData=Object.keys(this.zoneDataToSearch[0]);
    })
  }

  getAllEquipes()
  {
    this.apiServiceEquipe.getEquipes().subscribe(response=>{
      console.log(response);
      console.log("Equipe loading ok");
      this.dataEquipe=response;
      this.equipeDataToSearch=response;
      this.equipeColumnData=Object.keys(this.equipeDataToSearch[0]);
    })
  }

  getProfils()
  {
    this.apiServiceProfil.getAllProfil().subscribe(response=>{
      console.log(response);
      this.ProfilData=response;
      this.profilDataToSearch =response;
      this.profilColumnData=Object.keys(this.profilDataToSearch[0]);
    });
  }

  getAllRole()
  {
    this.apiServiceRole.getListRole().subscribe(response=>{
      console.log(response);
      this.roleData=response;
      this.roleDataTosearch=response;
      this.roleColumnData=Object.keys(this.roleDataTosearch[0]);
    })
  }


  //------------------->Search bar options
  searchUserData(event)
  {
    console.log("Search event ok");
    const filter=event.target.value.toString().toLowerCase();
    if(filter.trim()==="")
    {
    this.utilisateursData=this.dataToSearch;
   }
    else
    {
     this.utilisateursData=this.dataToSearch.filter(item=>{
       for(let i=0; i<this.columnData.length; i++)
       {
         var colVal=item[this.columnData[i]];

         if(!filter|| (!!colVal && colVal.toString().toLowerCase().indexOf(filter.trim())!=-1))
         {
           return true;
         }
       }
     });
    }
  }

  searchEquipeData(event)
  {
    console.log("Search event ok");
    let filter=event.target.value.toLowerCase();
    if(filter.trim()==="")
    {
    this.dataEquipe =this.equipeDataToSearch ;
   }
    else
    {
     this.dataEquipe=this.equipeDataToSearch.filter(item=>{
       for(let i=0; i<this.equipeColumnData.length; i++)
       {
         var colVal=item[this.equipeColumnData[i]];

         if(!filter|| (!!colVal && colVal.toString().toLowerCase().indexOf(filter.trim())!=-1))
         {
           return true;
         }
       }
     });
    }
  }

  searchZoneData(event)
  {
    console.log("Search event ok");
    let filter=event.target.value.toLowerCase();
    if(filter.trim()==="")
    {
    this.zonesData=this.zoneDataToSearch;
   }
    else
    {
     this.zonesData=this.zoneDataToSearch.filter(item=>{
       for(let i=0; i<this.zoneColumnData.length; i++)
       {
         var colVal=item[this.zoneColumnData[i]];

         if(!filter|| (!!colVal && colVal.toString().toLowerCase().indexOf(filter.trim())!=-1))
         {
           return true;
         }
       }
     });
    }
  }

  searchProfilData(event)
  {
    console.log("Search event ok");
    let filter=event.target.value.toLowerCase();
    if(filter.trim()==="")
    {
    this.ProfilData=this.profilDataToSearch;
   }
    else
    {
     this.ProfilData=this.profilDataToSearch.filter(item=>{
       for(let i=0; i<this.profilColumnData.length; i++)
       {
         var colVal=item[this.profilColumnData[i]];

         if(!filter|| (!!colVal && colVal.toString().toLowerCase().indexOf(filter.trim())!=-1))
         {
           return true;
         }
       }
     });
    }
  }

  searchRoleData(event)
  {
    console.log("Search event ok");
    let filter=event.target.value.toLowerCase();
    if(filter.trim()==="")
    {
    this.roleData=this.roleDataTosearch;
   }
    else
    {
     this.roleData=this.roleDataTosearch.filter(item=>{
       for(let i=0; i<this.roleColumnData.length; i++)
       {
         var colVal=item[this.roleColumnData[i]];

         if(!filter|| (!!colVal && colVal.toString().toLowerCase().indexOf(filter.trim())!=-1))
         {
           return true;
         }
       }
     });
    }
  }

  //----------------------> delete options
  delete(item1, item2)
  {
    this.confirmDeleted(item1,item2);
  }

  async confirmDeleted(item, item2)
   {
     const alert= await this.alertCtlr.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              switch (item2) {
                case 1:
                  {
                    this.apiServiceUser.deleteUtilisateur(item._id).subscribe(()=>{
                    this.getAllUtilisateurs();
                    console.log("Suppression User avec Succès!");
                  });
                  break;
                }
                case 2:
                  {
                    this.apiServiceEquipe.deleteEquipe(item._id).subscribe(()=>{
                    this.getAllEquipes();
                    console.log("Suppression equipe avec Succès!");
                  });
                  break;
                }
                case 3:
                  {
                    this.apiServiceZone.deleteZone(item._id).subscribe(()=>{
                    this.getAllZones();
                    console.log("Suppression zone avec Succès!");
                  });
                  break;
                }
                case 4:
                  {
                    this.apiServiceProfil.deleteProfil(item._id).subscribe(()=>{
                    this.getProfils();
                    console.log("Suppression profil avec Succès!");
                  });
                  break;
                }
                case 5:
                  {
                    this.apiServiceRole.deleteRole(item._id).subscribe(()=>{
                    this.getAllRole();
                    console.log("Suppression role avec Succès!");
                  });
                  break;
                }
                default:
                  break;
              }
              
          }}
            ] 
           
       }
     );
     await alert.present()
   }

}
