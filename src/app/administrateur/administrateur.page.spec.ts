import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdministrateurPage } from './administrateur.page';

describe('AdministrateurPage', () => {
  let component: AdministrateurPage;
  let fixture: ComponentFixture<AdministrateurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrateurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdministrateurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
