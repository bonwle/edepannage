import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevisCreatePageRoutingModule } from './devis-create-routing.module';

import { DevisCreatePage } from './devis-create.page';
import { OwlFormFieldModule, OwlInputModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DevisCreatePageRoutingModule,
    OwlFormFieldModule, OwlInputModule
  ],
  declarations: [DevisCreatePage]
})
export class DevisCreatePageModule {}
