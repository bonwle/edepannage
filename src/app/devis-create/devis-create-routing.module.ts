import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevisCreatePage } from './devis-create.page';

const routes: Routes = [
  {
    path: '',
    component: DevisCreatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevisCreatePageRoutingModule {}
