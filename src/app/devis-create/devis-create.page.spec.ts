import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevisCreatePage } from './devis-create.page';

describe('DevisCreatePage', () => {
  let component: DevisCreatePage;
  let fixture: ComponentFixture<DevisCreatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisCreatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevisCreatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
