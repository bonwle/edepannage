import { Component, OnInit } from '@angular/core';
import { Equipes } from '../models/equipes';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipeService } from '../services/equipe.service';
import { ToastController, ModalController, NavParams } from '@ionic/angular';
import { UtilisateursService } from '../services/utilisateurs.service';

@Component({
  selector: 'app-equipe-edit',
  templateUrl: './equipe-edit.page.html',
  styleUrls: ['./equipe-edit.page.scss'],
})
export class EquipeEditPage implements OnInit {
  
  id: number;
  data: Equipes;
  usersData: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiService: EquipeService,
    private apiServiceUser: UtilisateursService,
    private toastCtrl: ToastController,
  ) 
  { this.data=new Equipes();
  this.usersData=[];}

  ngOnInit() {

    this.id=this.activatedRoute.snapshot.params["id"];
    //get item details using id
    this.apiService.getEquipe(this.id).subscribe(response=>{
    console.log(response);
    this.data=response;});
    this.getUsers();
  }
  update()
  {
    this.apiService.updateEquipe(this.id, this.data).subscribe(response=>{
    this.displayToastEdit();
    this.router.navigate(['administrateur']);
  });
  }

  async displayToastEdit()
  {
    const toast=await this.toastCtrl.create(
      {
        message:"Modification effectuée avec succès!",
        header:"Succes Modif",
        color:"success",
        duration:2500,
        position:"middle",
      }
    );
    toast.present();
  }
  getUsers()
  {
    this.apiServiceUser.getUtilisateurs().subscribe(response=>{
      console.log(response);
      this.usersData=response;
    })
  }

}
