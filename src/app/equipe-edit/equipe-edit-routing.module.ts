import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EquipeEditPage } from './equipe-edit.page';

const routes: Routes = [
  {
    path: '',
    component: EquipeEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquipeEditPageRoutingModule {}
