import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { EquipeEditPageRoutingModule } from './equipe-edit-routing.module';
import { EquipeEditPage } from './equipe-edit.page';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlInputModule, OwlFormFieldModule, OwlSelectModule } from 'owl-ng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    OwlFormFieldModule ,
    OwlInputModule,
    OwlSelectModule,
    EquipeEditPageRoutingModule
  ],
  declarations: [EquipeEditPage]
})
export class EquipeEditPageModule {}
