import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EquipeEditPage } from './equipe-edit.page';

describe('EquipeEditPage', () => {
  let component: EquipeEditPage;
  let fixture: ComponentFixture<EquipeEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipeEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EquipeEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
