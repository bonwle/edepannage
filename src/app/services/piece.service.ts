import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { Pieces } from '../models/pieces';
import { retry, catchError } from 'rxjs/operators';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})
export class PieceService {
  base_apth=ApplicationContext.serveurUrl;

  constructor(private http: HttpClient) { }
  
    //http Options
    httpOptions={headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })}
  
    //Handle API errors
    handlerror(error:HttpErrorResponse)
    {
      if(error.error instanceof ErrorEvent)
      {
        console.error('An error occurred:', error.error.message);
      }
      else
      {
        console.error(
          'Backend returned code ${error.status},'+'body was:${error.error}'
        );
      }
  
      return throwError('something bad happened; plaese try again later.');
    };
  
  //--------------> Pieces CRUD operations <-------------------//
  
    createPiece(item): Observable<Pieces>
    {
      return this.http
      .post<Pieces>(this.base_apth+'/piece',JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
  
    getPiece(id):Observable<Pieces>
    {
      return this.http
      .get<Pieces>(this.base_apth+'/piece/'+id)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
  
    getAllPieces():Observable<Pieces>
    {
      console.log("API Get All Pieces");
      return this.http
      .get<Pieces>(this.base_apth+'/pieces')
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
    
    updatePiece(id, item): Observable<Pieces>
    {
      return this.http
      .put<Pieces>(this.base_apth+'/piece/'+id, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
  
    deletePiece(id)
    {
      return this.http
      .delete<Pieces>(this.base_apth+'/piece/'+id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
}
