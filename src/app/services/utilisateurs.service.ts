import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Utilisateurs } from '../models/utilisateurs';
import { retry, catchError } from 'rxjs/operators';
import { stringify } from '@angular/compiler/src/util';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})
export class UtilisateursService {

  //API path
  base_apth=ApplicationContext.serveurUrl;

constructor(private http: HttpClient) { }

  //http Options
  httpOptions={headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })}

  //Handle API errors
  handlerror(error:HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(
        'Backend returned code ${error.status},'+'body was:${error.error}'
      );
    }

    return throwError('something bad happened; plaese try again later.');
  };

  //----------Utilisateurs CRUD Operations-------------------//

  createUtilisateur(item): Observable<Utilisateurs>
  {
      return this.http
    .post<Utilisateurs>(this.base_apth+'/utilisateurs',JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getUtilisateur(id):Observable<Utilisateurs>
  {
    return this.http
    .get<Utilisateurs>(this.base_apth+'/utilisateurs/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getUtilisateurs():Observable<Utilisateurs>
  {
    console.log("API Get All Users");
    return this.http
    .get<Utilisateurs>(this.base_apth+'/utilisateurs')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }


  updateUtilisateur(id, item): Observable<Utilisateurs>
  {
    return this.http
    .put<Utilisateurs>(this.base_apth+'/utilisateurs/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  deleteUtilisateur(id)
  {
    return this.http
    .delete<Utilisateurs>(this.base_apth+'/utilisateurs/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getUtilisateurConnecte(nomUtilisateur, passwordUtilisateur?):Observable<Utilisateurs>
  {
   return this.http
   .get<Utilisateurs>(this.base_apth+'/utilisateurs/'+nomUtilisateur+'/'+passwordUtilisateur)
   .pipe(
     retry(2),
     catchError(this.handlerror)
    )
  }
}
