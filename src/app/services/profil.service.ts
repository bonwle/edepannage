
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import { Profil } from '../models/profil';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})


export class ProfilService 
{

base_apth=ApplicationContext.serveurUrl;

constructor(private http: HttpClient) { }

  //http Options
  httpOptions={headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })}

  //Handle API errors
  handlerror(error:HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(
        'Backend returned code ${error.status},'+'body was:${error.error}'
      );
    }

    return throwError('something bad happened; plaese try again later.');
  };

//--------------> Profil CRUD operations <-------------------//

  createProfil(item): Observable<Profil>
  {
    return this.http
    .post<Profil>(this.base_apth+'/profil',JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getProfil(id):Observable<Profil>
  {
    return this.http
    .get<Profil>(this.base_apth+'/profil/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

 getAllProfil():Observable<Profil>
  {
    console.log("API Get All Profil");
    return this.http
    .get<Profil>(this.base_apth+'/profils')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }
  
  updatePiece(id, item): Observable<Profil>
  {
    return this.http
    .put<Profil>(this.base_apth+'/profil/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  deleteProfil(id)
  {
    return this.http
    .delete<Profil>(this.base_apth+'/profil/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

}
