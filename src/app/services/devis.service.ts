import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { Devis } from '../models/devis';
import { retry, catchError } from 'rxjs/operators';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})
export class DevisService {
  base_apth=ApplicationContext.serveurUrl;

  constructor(private http: HttpClient) { }
  
    //http Options
    httpOptions={headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })}
  
    //Handle API errors
    handlerror(error:HttpErrorResponse)
    {
      if(error.error instanceof ErrorEvent)
      {
        console.error('An error occurred:', error.error.message);
      }
      else
      {
        console.error(
          'Backend returned code ${error.status},'+'body was:${error.error}'
        );
      }
  
      return throwError('something bad happened; plaese try again later.');
    };
  
  //--------------> DEVIS CRUD operations <-------------------//
  
    createDevis(item): Observable<Devis>
    {
      return this.http
      .post<Devis>(this.base_apth+'/devis',JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
  
    getDevis(id):Observable<Devis>
    {
      return this.http
      .get<Devis>(this.base_apth+'/devis/'+id)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
  
    getAllDevis():Observable<Devis>
    {
      console.log("API Get All Devis");
      return this.http
      .get<Devis>(this.base_apth+'/devis')
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }

    getAllDevisByIntervention(id):Observable<Devis>
    {
      console.log("API Get All Devis");
      return this.http
      .get<Devis>(this.base_apth+'/devisbyintervention/'+id)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      );
    }

    updateDevis(id, item): Observable<Devis>
    {
      return this.http
      .put<Devis>(this.base_apth+'/devis/'+id, JSON.stringify(item), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
  
    deleteDevis(id)
    {
      return this.http
      .delete<Devis>(this.base_apth+'/devis/'+id, this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handlerror)
      )
    }
}
