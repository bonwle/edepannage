import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { Zones } from '../models/zones';
import { retry,catchError } from 'rxjs/operators';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})
export class ZoneService {
//API path
base_apth=ApplicationContext.serveurUrl;

constructor(private http: HttpClient) { }

  //http Options
  httpOptions={headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })}

  //Handle API errors
  handlerror(error:HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(
        'Backend returned code ${error.status},'+'body was:${error.error}'
      );
    }

    return throwError('something bad happened; plaese try again later.');
  };

  //----------ZONE CRUD Operations-------------------//

  createZone(item): Observable<Zones>
  {
      return this.http
    .post<Zones>(this.base_apth+'/zones',JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getZone(id):Observable<Zones>
  {
    return this.http
    .get<Zones>(this.base_apth+'/zones/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }


  getZoneEquipe(id):Observable<Zones>
  {
    return this.http //zones/:EqupipeAffectee/zone
    .get<Zones>(this.base_apth+'/equipeszone/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }


  getZones():Observable<Zones>
  {
    console.log("API Get All");
    return this.http
    .get<Zones>(this.base_apth+'/zones')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  updateZone(id, item): Observable<Zones>
  {
    return this.http
    .put<Zones>(this.base_apth+'/zones/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  deleteZone(id)
  {
    return this.http
    .delete<Zones>(this.base_apth+'/zones/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }
}
  
