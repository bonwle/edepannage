import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Equipes } from '../models/equipes';
import { ApplicationContext } from '../models/application-context';


@Injectable({
  providedIn: 'root'
})
export class EquipeService {
//API path
base_apth=ApplicationContext.serveurUrl;

constructor(private http: HttpClient) { }

  //http Options
  httpOptions={headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })}

  //Handle API errors
  handlerror(error:HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(
        'Backend returned code ${error.status},'+'body was:${error.error}'
      );
    }

    return throwError('something bad happened; plaese try again later.');
  };

//--------------> EQUIPE CRUD operations <-------------------//

  createEquipe(item): Observable<Equipes>
  {
      return this.http
    .post<Equipes>(this.base_apth+'/equipes',JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getEquipe(id):Observable<Equipes>
  {
    return this.http
    .get<Equipes>(this.base_apth+'/equipes/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  getEquipeAgent(id):Observable<Equipes>
  {
    return this.http 
    .get<Equipes>(this.base_apth+'/agentequipes/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  getEquipes():Observable<Equipes>
  {
    console.log("API Get All");
    return this.http
    .get<Equipes>(this.base_apth+'/equipes')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  updateEquipe(id, item): Observable<Equipes>
  {
    return this.http
    .put<Equipes>(this.base_apth+'/equipes/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  deleteEquipe(id)
  {
    return this.http
    .delete<Equipes>(this.base_apth+'/equipes/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  // agentnotequipes

  getAgentNotInEquipe(id):Observable<Equipes>
  {
    return this.http 
    .get<Equipes>(this.base_apth+'/agentnotequipes/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

}