
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import { Role } from '../models/role';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})


export class RoleService 
{
  base_apth=ApplicationContext.serveurUrl;

  constructor(private http: HttpClient) { }

  httpOptions = {headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

  handlerror(error: HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(
        'Backend returned code ${error.status},'+'body was:${error.error}'
      );
    }

    return throwError('something bad happened; plaese try again later.');
  };

  /*----------DIAGNOSTIQUE CRUD Operations-------------------*/

  createRole(data): Observable<Role>
  {
      return this.http
    .post<Role>(this.base_apth+'/Roles',JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getRole(id):Observable<Role>
  {
    return this.http
    .get<Role>(this.base_apth+'/Roles/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getListRole(): Observable<Role>
  {
    console.log('lister tous les roles');
    return this.http
    .get<Role>(this.base_apth+'/Roles')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  updateRole(id, data): Observable<Role>
  {
    return this.http
    .put<Role>(this.base_apth+'/Roles/'+id, JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  deleteRole(id)
  {
    return this.http
    .delete<Role>(this.base_apth+'/Roles/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }


}
