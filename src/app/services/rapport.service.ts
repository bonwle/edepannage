import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { Rapport } from '../models/rapport';
import { retry, catchError } from 'rxjs/operators';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})
export class RapportService {

  //API path
  base_apth=ApplicationContext.serveurUrl;

constructor(private http: HttpClient) { }

  //http Options
  httpOptions={headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })};

  httpOptions1={headers: new HttpHeaders({
    'Content-Type': 'image/jpeg'
  })};

  //Handle API errors
  handlerror(error:HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(
        'Backend returned code ${error.status},'+'body was:${error.error}'
      );
    }

    return throwError('something bad happened; plaese try again later.');
  };

//--------------> Rapport CRUD operations <-------------------//

createWithRapport(item): Observable<Rapport>
  {
      return this.http
    .post<Rapport>(this.base_apth+'/rapportimg',JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  createRapport(item): Observable<Rapport>
  {
      return this.http
    .post<Rapport>(this.base_apth+'/Rapport',JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }


  getRapport(id):Observable<Rapport>
  {
    return this.http
    .get<Rapport>(this.base_apth+'/Rapport/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  
getAllRapportIntervention(id):Observable<Rapport>
{
  console.log("API Get All Rapport");
  return this.http
  .get<Rapport>(this.base_apth+'/RapportByintervention/'+id)
  .pipe(
    retry(2),
    catchError(this.handlerror)
  );
}


  getAllRapport():Observable<Rapport>
  {
    console.log("API Get All");
    return this.http
    .get<Rapport>(this.base_apth+'/Rapport')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }


  getRapportImage(image):Observable<any>
  {
    console.log("API Get image");
    return this.http
    .get<Rapport>(this.base_apth+'/imgRaport/'+image, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }


  
  
  updateRapport(id, item): Observable<Rapport>
  {
    return this.http
    .put<Rapport>(this.base_apth+'/Rapport/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  deleteRapport(id)
  {
    return this.http
    .delete<Rapport>(this.base_apth+'/Rapport/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  // getImageByName(nomImage){
  //   return this.http
  //   .get<Rapport>(this.base_apth+'/imgRaport/'+nomImage, this.httpOptions1)
  //   .pipe(
  //     retry(2),
  //     catchError(this.handlerror)
  //   );
  // }
  
}
