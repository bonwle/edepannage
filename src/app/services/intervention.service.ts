import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Interventions } from '../models/interventions';
import { ApplicationContext } from '../models/application-context';
import { throwError, Observable,Subject } from 'rxjs';
import { retry, catchError,tap, map} from 'rxjs/operators';
declare const Pusher: any;

@Injectable({
  providedIn: 'root'
})
export class InterventionService {
//channel;
 //API path
 base_apth=ApplicationContext.serveurUrl;
 
constructor(private http: HttpClient) { 
  // var pusher = new Pusher('feae418dab7469aee93e', {
  //   cluster: 'eu',
  //   forceTLS: true
  // });
  
  //   this.channel = pusher.subscribe('edepannagetest');

}


public init(){
  // return this.channel;
  }
  //http Options
private refresh = new Subject<void>();
 
get refreshok(){
return this.refresh;
}
  httpOptions={headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })}

  //Handle API errors
  handlerror(error:HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      // console.error(
      //   'Backend returned code ${error.status},'+'body was:${error.error}'
      // );
    }

    return throwError('something bad happened; plaese try again later.');
  };

  //----------Intervention CRUD Operations-------------------//

  createIntervention(item): Observable<Interventions>
  {
      return this.http
    .post<Interventions>(this.base_apth+'/interventions',JSON.stringify(item), this.httpOptions)
    .pipe(
          tap(() =>{
            this.refresh.next();
            retry(2),
            catchError(this.handlerror)
          }) 
    );
  }

  getIntervention(id):Observable<Interventions>
  {
    return this.http
    .get<Interventions>(this.base_apth+'/interventions/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }   

  getEquipeUser(id):Observable<Interventions>
  {
    //console.log("get equipe nom");
    return this.http
    .get<Interventions>(this.base_apth+'/getEquipeUsers/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getInterventions():Observable<Interventions>
  {
    console.log("API Get All");
    return this.http
    .get<Interventions>(this.base_apth+'/interventions')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  updateIntervention(id, item): Observable<Interventions>
  {
    return this.http
    .put<Interventions>(this.base_apth+'/interventions/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      tap(() =>{
        this.refresh.next();
        retry(2),
        catchError(this.handlerror)
      }) 
    );
  }


  updateMainDoeuvre(id, item): Observable<Interventions>
  {
    return this.http
    .patch<Interventions>(this.base_apth+'/interventionsmaindoeuvre/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  updateStatut(id, item): Observable<Interventions>
  {
    return this.http
    .patch<Interventions>(this.base_apth+'/interventionstatut/'+id, JSON.stringify(item), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }


  deleteIntervention(id)
  {
    return this.http
    .delete<Interventions>(this.base_apth+'/interventions/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }



  getGroupByDate():Observable<Interventions>
{
  console.log("group by date ");
  return this.http
  .get<Interventions>(this.base_apth+'/groupByStatut')
  .pipe(
    retry(2),
    catchError(this.handlerror)
  );
}

}


