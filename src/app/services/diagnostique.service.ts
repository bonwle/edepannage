import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import { Diagnostique } from '../models/diagnostique';
import { ApplicationContext } from '../models/application-context';

@Injectable({
  providedIn: 'root'
})

export class DiagnostiqueService
// tslint:disable-next-line: one-line
{
  base_apth=ApplicationContext.serveurUrl
  lien = 'http://api.ipapi.com';
  lien1 ='https://ipapi.co/json';

  constructor(private http: HttpClient) { }

  httpOptions = {headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

  handlerror(error: HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else
    {
      console.error(
        'Backend returned code ${error.status},'+'body was:${error.error}'
      );
    }

    return throwError('something bad happened; plaese try again later.');
  };

  /*----------DIAGNOSTIQUE CRUD Operations-------------------*/

  createDiagnostique(data): Observable<Diagnostique>
  {
      return this.http
    .post<Diagnostique>(this.base_apth+'/Diagnostics',JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getDiagnostique(id):Observable<Diagnostique>
  {
    return this.http
    .get<Diagnostique>(this.base_apth+'/Diagnostics/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }
//----------------------------recuperation des diagnostiques pour une intervention -------------------------------
  getDiagnosticIntervention(id):Observable<Diagnostique>
  {
    return this.http
    .get<Diagnostique>(this.base_apth+'/intervention/diagnostics/'+id)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }


  getListDiagnostiques(): Observable<Diagnostique>
  {
    console.log('lister tous les diagnostiques');
    return this.http
    .get<Diagnostique>(this.base_apth+'/Diagnostics')
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  updateDiagnostique(id, data): Observable<Diagnostique>
  {
    return this.http
    .put<Diagnostique>(this.base_apth+'/Diagnostics/'+id, JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  deleteDiagnostique(id)
  {
    return this.http
    .delete<Diagnostique>(this.base_apth+'/Diagnostics/'+id, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    )
  }

  getGeolocalisation(ip):Observable<any>
  {
    return this.http
    .get<any>(this.lien+'/'+ip+'?access_key=5f049d9896611279831d80d1481f5420',this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  getGeolocalisation1():Observable<any>
  {
    return this.http
    .get<any>(this.lien1, this.httpOptions)
    .pipe(
      retry(2),
      catchError(this.handlerror)
    );
  }

  
  // getRapportImage(image):Observable<any>
  // {
  //   console.log("API Get image");
  //   return this.http
  //   .get<any>(this.basePath+'/imgRaport/'+image, this.httpOptions)
  //   .pipe(
  //     retry(2),
  //     catchError(this.handlerror)
  //   );
  // }


}
