import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PieceListPage } from './piece-list.page';

describe('PieceListPage', () => {
  let component: PieceListPage;
  let fixture: ComponentFixture<PieceListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PieceListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PieceListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
