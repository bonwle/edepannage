import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PieceListPageRoutingModule } from './piece-list-routing.module';

import { PieceListPage } from './piece-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxDatatableModule,
    PieceListPageRoutingModule
  ],
  declarations: [PieceListPage]
})
export class PieceListPageModule {}
