import { Component, OnInit } from '@angular/core';
import { PieceService } from '../services/piece.service';
import { AlertController } from '@ionic/angular';
import { UserContext } from '../models/user-context';

@Component({
  selector: 'app-piece-list',
  templateUrl: './piece-list.page.html',
  styleUrls: ['./piece-list.page.scss'],
})
export class PieceListPage implements OnInit {
  piecesData: any;
  pieceDataToSearch: any;
  pieceColumnData: string[];
  roleAddPiece: boolean=true;
  roleEditPiece: boolean=true;
  roleDeletePiece: boolean=true;
  roleDetailPiece: boolean=true;
  allRolesPiece:boolean=true;

  constructor(
    public apiService: PieceService,
    private alertCtlr: AlertController
  ) 
  { 
    this.piecesData=[];
    this.pieceDataToSearch=[];
    this.pieceColumnData=[];
  }

  ngOnInit() {
    this.getPieces();
    this.getConnectuSerRoles();
  }

  ionViewWillEnter()
  {
    this.getPieces();
  }
  getConnectuSerRoles()
  {
    if(UserContext.userRolesList==undefined) return;
    var _roles=UserContext.userRolesList;
    this.roleEditPiece=(_roles.indexOf("MPI01")==-1);
    this.roleAddPiece=(_roles.indexOf("API01")==-1);
    this.roleDeletePiece=(_roles.indexOf("SPI01")==-1);
    this.roleDetailPiece=(_roles.indexOf("VDPI01")==-1);
    if(this.roleDetailPiece==true && this.roleEditPiece==true && this.roleDeletePiece==true)
    {
      this.allRolesPiece=false;
    }
    else
    {
      this.allRolesPiece=true;
    }

  }

  getPieces()
  {
    this.apiService.getAllPieces().subscribe((response)=>{
      this.piecesData=response;
      this.pieceDataToSearch=response;
      this.pieceColumnData=Object.keys(this.pieceDataToSearch[0])
    })
  }

  delete(item)
  {
    this.confirmDeleted(item);
  }

  async confirmDeleted(item)
   {
     const alert= await this.alertCtlr.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deletePiece(item._id).subscribe(response=>{this.getPieces()})
            }
          }
            ] 
           
       }
     );
     await alert.present()
   }

   searchPieceData(event)
  {
    let filter=event.target.value.toLowerCase();
    if(filter.trim()==="")
    {
    this.piecesData=this.pieceDataToSearch;
   }
    else
    {
     this.piecesData=this.pieceDataToSearch.filter(item=>{
       for(let i=0; i<this.pieceColumnData.length; i++)
       {
         var colVal=item[this.pieceColumnData[i]];

         if(!filter|| (!!colVal && colVal.toString().toLowerCase().indexOf(filter.trim())!=-1))
         {
           return true;
         }
       }
     });
    }
  }
}
