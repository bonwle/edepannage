import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DevisListPageRoutingModule } from './devis-list-routing.module';

import { DevisListPage } from './devis-list.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxDatatableModule,
    DevisListPageRoutingModule
  ],
  declarations: [DevisListPage]
})
export class DevisListPageModule {}
