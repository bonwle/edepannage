import { Component, OnInit } from '@angular/core';
import { DevisService } from '../services/devis.service';
import { Devis } from '../models/devis';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-devis-list',
  templateUrl: './devis-list.page.html',
  styleUrls: ['./devis-list.page.scss'],
})
export class DevisListPage implements OnInit {

  dataDevis: any;
  devisData:Devis;
  
  tableStyle = 'dark';
  public columns: any;
  public rows: any;
  filteredData = [];

  constructor(
    public apiService:DevisService,
    private alertCtrl:AlertController)
  {
    this.dataDevis = [];
    this.devisData=new Devis()
   }

  ngOnInit() {
    this.getDevis();
  }

  delete(item)
  {
    this.confirmDeleted(item);
  }

  getDevis()
  {
    // tslint:disable-next-line: curly
    if(this.devisData['IdIntervention'])
    this.apiService.getAllDevisByIntervention(this.devisData['IdIntervention']).subscribe(response=>{
      console.log(response);
      this.dataDevis=response;
    })
  }

  submitForm()
  // tslint:disable-next-line: one-line
  {
    // tslint:disable-next-line: curly
   // if(this.devisData.Designation)
    this.apiService.createDevis(this.devisData).subscribe((response)=>{
      this.getDevis();
      console.log(response);
    });
  }
  
  async confirmDeleted(item)
   {
     const alert= await this.alertCtrl.create(
       {
         header:"Suppression",
         message:"Confirmez-vous la suppression de cet élément?",
         animated:true,
         backdropDismiss:false,
         translucent:true,
         buttons:[
          {
            text:"Non",
            role:"cancel",
            cssClass: "secondary" 
          },
          {
            text: "Oui",
            handler:()=>{
              this.apiService.deleteDevis(item._id).subscribe(response=>{this.getDevis()});
            }
          }
            ] 
           
       }
     );
     await alert.present()
   }

}
