import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DevisListPage } from './devis-list.page';

describe('DevisListPage', () => {
  let component: DevisListPage;
  let fixture: ComponentFixture<DevisListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevisListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DevisListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
