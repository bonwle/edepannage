import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevisListPage } from './devis-list.page';

const routes: Routes = [
  {
    path: '',
    component: DevisListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevisListPageRoutingModule {}
